<?php
/**
* Buchzeit APS-Verwaltung
*
* @author Manuel Postlbauer
* @version 0.9
*/

/**
* index_member
*
* Nach dem Login wird der Benutzer auf diese Seite weitergeleitet.
* hier wird er mit Infos versorgt was er im Mitgliedsbereich bewerkstelligen kann.
*
*/
session_start();
$displayloginregister=0;
//Ist der Benutzer eingeloggt zeige Benutzermen�
if(isset($_SESSION['LOGGEDIN'])){
	$displayloginregister=1;
}
//Ist der Benutzer nicht eingeloggt, wird auf die Loginpage weitergeleitet
if(!isset($_SESSION['LOGGEDIN'])){
	header("location: index.php");
	exit();
}
//Header des Seitendesigns
include("header.php");
?>
<h1>Home</h1>
<p>Sie sind nun eingeloggt. Mit einem Klick auf <b>PASSWORT ÄNDERN</b> können Sie Ihr Benutzerkennwort ändern. Wenn Sie auf <b>DATEN EDITIEREN</b> klicken können Sie die Daten bezüglich Ihrer Schulbibliothek verändern.</p>
<?php
include("footer.php");
?>