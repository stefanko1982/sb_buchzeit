<?php
/**
* Buchzeit APS-Verwaltung
*
* @author Manuel Postlbauer
* @version 0.9
*/

/**
* step 9 (Teil von formular.php)
*/
session_start();
session_destroy();
include("header.php");
?>
<h1>Daten editieren</h1>
<p>Wir bedanken uns für Ihre Mitarbeit, Ihre Daten sind jetzt in unserer Datenbank gespeichert.</p>
<p><a href="index.php">Hier</a> gelangen Sie zurück zur Startseite</p>
<?php
include("footer.php");
?>