<?php
session_start();
if(!isset($_SESSION['mail_new_password'])){
	header("location: index.php");
	exit();
}
$nomail="";
include("localconf.php");

function sendMail($nomail,$fk_bibliothek_id){
    include("localconf.php");
	if($nomail){
		$message = "Kennwortanfrage\n\n";
		$message .="Ein Benutzer der Schule".$_SESSION['mail_schulname']." mit der Schulkennzahl:".$_SESSION['mail_schulkennzahl']." hat ein neues Passwort angefordert.\n";
		$message .="Da für diese Schule keine gültige E-Mail Adresse im System ist, wurde Ihnen das neu generierte Passwort zugesendet.\n";
		$message .="Der Benutzer müsste sich in Kürze bei Ihnen melden.\n\n";
		$message .="Das neue Kennwort ist:".$_SESSION['mail_new_password']."";
		// In case any of our lines are larger than 70 characters, we should use wordwrap()
		$message = wordwrap($message, 70);
		// Send
		global $emailaddress;
		$emailsubject = "SB-Verwaltung_Kennwortanfrage für ".$_SESSION['mail_schulkennzahl']."";
		mail($emailaddress, mb_encode_mimeheader($emailsubject), $message);
	}
	else{
		$message = "Antwort auf Kennwortanfrage \n\n";
		$message .="Ihr neues Kennwort ist: ".$_SESSION['mail_new_password']."\n\n";
		$message .="Sie können nachdem sie sich mit Ihrem neuen Kennwort eingeloggt haben es\n";
		$message .="unter Profil erneut ändern.\n\n";
		$message .="Mit freundlichen Grüßen\n";
		$message .="Ihr Buchzeitteam";
		// In case any of our lines are larger than 70 characters, we should use wordwrap()
		$message = wordwrap($message, 70);
		// Send
		global $emailaddress;
		$emailsubject = "SB-Verwaltung_Kennwortanfrage für ".$_SESSION['mail_schulkennzahl'];
		mail(getUserMail($fk_bibliothek_id), mb_encode_mimeheader($emailsubject), $message);
	}
}
function getUserMail($fk_bibliothek_id){
    include("localconf.php");
	$qry="SELECT email FROM bibliothek_kontaktperson WHERE fk_bibliothek_id='".$fk_bibliothek_id."' AND stellvertreter ='0'";
	$result=$mysqli->query($qry);
	if($result) {
			if($row = $result->fetch_assoc()){
				//Gibt es eine Eigene Bibliothek, dann ist auch eine Email Adresse vorhanden.
				return($row['email']);
			}
		}
	else {
			die("Query failed");
	}
}

$qry="SELECT * FROM schuladressen WHERE schulkennzahl='".$_SESSION['mail_schulkennzahl']."' AND postleitzahl='".$_SESSION['mail_postleitzahl']."'";
$result=$mysqli->query($qry);
		if($result) {
				if($row = $result->fetch_assoc()){
				//Gibt es eine Eigene Bibliothek, dann ist auch eine Email Adresse vorhanden.
				$fk_bibliothek_id=$row['fk_bibliothek_id'];
				if($fk_bibliothek_id!=0){
					sendMail(0,$fk_bibliothek_id);
					unset($_SESSION['mail_new_password']);
					unset($_SESSION['mail_schulkennzahl']);
					unset($_SESSION['mail_postleitzahl']);
					unset($_SESSION['mail_schulname']);
				}
				else{
					sendMail(1,$fk_bibliothek_id);
					unset($_SESSION['mail_new_password']);
					unset($_SESSION['mail_schulkennzahl']);
					unset($_SESSION['mail_postleitzahl']);
					unset($_SESSION['mail_schulname']);
					$nomail=1;
				}
			}
		}
		else {
			die("Query failed");
		}

include("header.php");
?>
<h1>Neues Kennwort</h1>
<?php
if($nomail){
?>
<p>
In unserer Datenbank ist leider keine Emailadresse von Ihnen vorhanden, unter der wir Ihnen ein neues Kennwort zustellen könnten.<br/>
Um ein neues Kennwort zu beantragen senden Sie bitte ein E-Mail mit Ihrer <b>Schulkennzahl</b> und <b>Postleitzahl</b> an:
<a href="mailto:h.pitzer@buchzeit.at?subject=Passwortanfrage">h.pitzer@buchzeit.at</a><br/>
Sie werden dann in den nächsten 24 Stunden ein neues Kennwort per Mail erhalten.<br/><br/>
Mit freundlichen Grüßen<br/>
Ihr Buchzeitteam<br/><br/>
<a href="index.php">Hier</a> gelangen Sie zurück zur Startseite
</p>
<?php
}
else{
?>
<p>
Es wurde ein neues Passwort generiert. Sie müssten dieses in Kürze per E-mail erhalten<br/><br/>
Mit freundlichen Grüßen<br/>
Ihr Buchzeitteam<br/><br/>
<a href="index.php">Hier</a> gelangen Sie zurück zur Startseite
</p>
<?php
}
include("footer.php");
?>
