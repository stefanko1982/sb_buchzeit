<?php
/**
* Buchzeit APS-Verwaltung
*
* @author Manuel Postlbauer
* @version 0.9
*/

/**
* step 4 (Teil von formular.php)
*/
if(!isset($_SESSION['LOGGEDIN'])){
	header("location: index.php");
	exit();
}
if($_SESSION['stepcounter']==4){
	$_SESSION['roomsize']=$_POST['roomsize'];
	$_SESSION['zentral']=$_POST['zentral'];
	$_SESSION['edvsaele']=$_POST['edvsaele'];
	$_SESSION['schwerzuerr']=$_POST['schwerzuerr'];
	$_SESSION['arbeitsplatzanzahl']=$_POST['arbeitsplatzanzahl'];
	$_SESSION['internetarbeits']=$_POST['internetarbeits'];
	$_SESSION['tvgeraet']=$_POST['tvgeraet'];
	$_SESSION['beamer']=$_POST['beamer'];
	$_SESSION['printer']=$_POST['printer'];
	$_SESSION['scanner']=$_POST['scanner'];
	$_SESSION['hoerbuchstation']=$_POST['hoerbuchstation'];
	$_SESSION['kopierger']=$_POST['kopierger'];
	if($_POST['programm']=="littera"){
		$_SESSION['islittera']=1;
		unset($_SESSION['isexlibris']);
		unset($_SESSION['isbond']);
		unset($_SESSION['isbiblioweb']);
		unset($_SESSION['isanderes']);
	}
	if($_POST['programm']=="exlibris"){
		$_SESSION['isexlibris']=1;
		unset($_SESSION['islittera']);
		unset($_SESSION['isbond']);
		unset($_SESSION['isbiblioweb']);
		unset($_SESSION['isanderes']);
	}
	if($_POST['programm']=="bond"){
		$_SESSION['isbond']=1;
		unset($_SESSION['isexlibris']);
		unset($_SESSION['islittera']);
		unset($_SESSION['isbiblioweb']);
		unset($_SESSION['isanderes']);
	}
	if($_POST['programm']=="biblioweb"){
		$_SESSION['isbiblioweb']=1;
		unset($_SESSION['isexlibris']);
		unset($_SESSION['isbond']);
		unset($_SESSION['islittera']);
		unset($_SESSION['isanderes']);
	}
	if($_POST['programm']=="anderes"){
		$_SESSION['isanderes']=1;
		unset($_SESSION['isexlibris']);
		unset($_SESSION['isbond']);
		unset($_SESSION['isbiblioweb']);
		unset($_SESSION['islittera']);
	}
	$_SESSION['litteraver']=$_POST['litteraver'];
	$_SESSION['exlibrisver']=$_POST['exlibrisver'];
	$_SESSION['bondver']=$_POST['bondver'];
	$_SESSION['bibliowebver']=$_POST['bibliowebver'];
	$_SESSION['anderesname']=$_POST['anderesname'];
	$_SESSION['anderesver']=$_POST['anderesver'];
	
	$_SESSION['stepcounter']+=1;
}
else{
	$_SESSION['stepcounter']-=1;
}
include("header.php");
?>
<form action="formular.php?step=5" method="post" onsubmit="return checkFields(4)">
	<h1>Daten editieren</h1>
	<img src="images/step4.png" alt=""/>
	<br/>
	<br/>
	<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="481px">
		<tr>
			<th colspan="2">Medienbestand - Anzahl</th>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Printmedien - Belletristik, Sach- und Fachbuchbestand, Comics:</td>
			<td><input type="text" name="belletristik" id="belletristik" value="<?php if(isset($_SESSION['belletristik'])) echo $_SESSION['belletristik']; else echo"0"; ?>" /></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Printmedien - Zeitschriften (Abo):</td>
			<td><input type="text" name="zeitschriften" id="zeitschriften" value="<?php if(isset($_SESSION['zeitschriften'])) echo $_SESSION['zeitschriften']; else echo"0"; ?>" /></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Printmedien - Tageszeitungen (Abo):</td>
			<td><input type="text" name="tageszeitungen" id="tageszeitungen" value="<?php if(isset($_SESSION['tageszeitungen'])) echo $_SESSION['tageszeitungen']; else echo"0"; ?>" /></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Digitale  Medien (CDs, DVDs):</td>
			<td><input type="text" name="cddvd" id="cddvd" value="<?php if(isset($_SESSION['cddvd'])) echo $_SESSION['cddvd']; else echo"0"; ?>" /></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Downloadangebote:</td>
			<td><input type="text" name="downloadang" id="downloadang" value="<?php if(isset($_SESSION['downloadang'])) echo $_SESSION['downloadang']; else echo"0"; ?>" /></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Herkömmliche AV-Medien (Kassetten, Videos):</td>
			<td><input type="text" name="videos" id="videos" value="<?php if(isset($_SESSION['videos'])) echo $_SESSION['videos']; else echo"0"; ?>" /></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Spiele:</td>
			<td><input type="text" name="spiele" id="spiele" value="<?php if(isset($_SESSION['spiele'])) echo $_SESSION['spiele']; else echo"0"; ?>" /></td>
		</tr>
	</table>
	
	<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="481px">
		<tr>
			<th colspan="2">Mehrfachexemplare</th>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Anzahl der Titel in Klassenstärke (mindestens 25 Exemplare):</td>
			<td><input type="text" name="mehrfachtitel" id="mehrfachtitel" value="<?php if(isset($_SESSION['mehrfachtitel'])) echo $_SESSION['mehrfachtitel']; else echo"0"; ?>" /></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>5 bis 10 Exemplare pro Titel - Anzahl der Titel:</td>
			<td><input type="text" name="exemplareprotitel" id="exemplareprotitel" value="<?php if(isset($_SESSION['exemplareprotitel'])) echo $_SESSION['exemplareprotitel']; else echo"0"; ?>" /></td>
		</tr>
	</table>
	<input type="button" value="" onclick="document.location.href='formular.php?step=3';" class="zurueckbutton"/>
	<input type="submit" value="" class="weiterbutton"/>
</form>
<?php
include("footer.php");
?>