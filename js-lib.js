function checkFields(step) {
	var alertmessage="";
	error=0;
	if(step==1){
		with(document){
			if(getElementById("school_id").value == "") error++;
			if(isNaN(getElementById("school_id").value)){
				alertmessage+="Die Schulkennzahl muss eine Zahl sein!";
				error++
			}
			if(getElementById("school_name").value == "") error++;
			if(getElementById("address_plz").value == "") error++;
			if(isNaN(getElementById("address_plz").value)){
				alertmessage+="\nDie Postleitzahl muss eine Zahl sein!";
				error++
			}
			if(getElementById("address_school_loc").value == "") error++;
			if(getElementById("address_street").value == "") error++;
			if((getElementById("schooltype0").checked == false) && (getElementById("schooltype1").checked == false) && (getElementById("schooltype2").checked == false) && (getElementById("schooltype3").checked == false) && (getElementById("schooltype4").checked == false)) {
				error++;
			}
			if((getElementById("gemeinde").value == "")&&(getElementById("schulerhalter").value == "")) error++;
		 }
	}
	if(step==2){
		with(document){
			if(isNaN(getElementById("classroomcount").value)){
				alertmessage+="Die Klassenanzahl muss eine Zahl sein!";
				error++
			}
		}
	}
	if(step==3){
		with(document){
			if((getElementById("roomsize").value == "")||(getElementById("roomsize").value == "0")) error++;
			if(isNaN(getElementById("roomsize").value)){
				alertmessage+="Die Raumgröße muss eine Zahl sein!";
				error++
			}
			if(isNaN(getElementById("arbeitsplatzanzahl").value)){
				alertmessage+="\nDie Anzahl der Arbeitsplätze muss eine Zahl sein!";
				error++
			}
			if(isNaN(getElementById("internetarbeits").value)){
				alertmessage+="\nDie Anzahl der Internet-Arbeitsplätze muss eine Zahl sein!";
				error++
			}
		}
	}
	if(step==4){
		with(document){
			if(isNaN(getElementById("belletristik").value)){
				alertmessage="Die Anzahl der Printmedien - Belletristik muss eine Zahl sein!";
				error++
			}
			if(isNaN(getElementById("zeitschriften").value)){
				alertmessage+="\nDie Anzahl der Printmedien - Zeitschriften muss eine Zahl sein!";
				error++
			}
			if(isNaN(getElementById("tageszeitungen").value)){
				alertmessage+="\nDie Anzahl der Printmedien - Tageszeitungen muss eine Zahl sein!";
				error++
			}
			if(isNaN(getElementById("cddvd").value)){
				alertmessage+="\nDie Anzahl der digitalen Medien muss eine Zahl sein!";
				error++
			}
			if(isNaN(getElementById("downloadang").value)){
				alertmessage+="\nDie Anzahl der Downloadangebote muss eine Zahl sein!";
				error++
			}
			if(isNaN(getElementById("videos").value)){
				alertmessage+="\nDie Anzahl der AV-Medien muss eine Zahl sein!";
				error++
			}
			if(isNaN(getElementById("spiele").value)){
				alertmessage+="\nDie Anzahl der Spiele muss eine Zahl sein!";
				error++
			}
			if(isNaN(getElementById("mehrfachtitel").value)){
				alertmessage+="\nDie Anzahl der Titel in Klassenstärke muss eine Zahl sein!";
				error++
			}
			if(isNaN(getElementById("exemplareprotitel").value)){
				alertmessage+="\nDie Anzahl der Exemplare pro Titel muss eine Zahl sein!";
				error++
			}
		}
	}
	if(step==5){
		with(document){
			if(isNaN(getElementById("oeffnungstunden").value)){
				alertmessage+="Die Anzahl der Stunden pro Woche bei den Öffnungszeiten muss eine Zahl sein!";
				error++
			}
			if(getElementById("mail").value == "") error++;
			if(getElementById("phone").value == "") error++;
			if(getElementById("ankaufsbudget").value == "0") error++;
			if(isNaN(getElementById("ankaufsbudget").value)){
				alertmessage+="\nDas jährliche Ankaufsbudget für Medien muss eine Zahl sein!";
				error++
			}
			if(isNaN(getElementById("bibliothekaranzahl").value)){
				alertmessage+="\nDie Anzahl der ausgebildeten Schulbibliothekar/innen muss eine Zahl sein!";
				error++
			}
			if(isNaN(getElementById("abgwochenstunden").value)){
				alertmessage+="\nDie Abgeltung: Unterschreitung - Anzahl der Wochenstunden muss eine Zahl sein!";
				error++
			}
		}
	}
	if(step==6){
		with(document){
			if(getElementById("contact_name").value == "") error++;
			if(getElementById("contact_mail").value == "") error++;
			if(getElementById("contact_phone").value == "") error++;
		}
	}
	if(error > 0){
		if(alertmessage!=0)
			alert(alertmessage);
		else
			alert("Bitte füllen Sie alle erforderlichen Felder korrekt aus!");
		return false;
	}
	return true;
}
function bibliotheksverwaltungsprogramm(id)
{
	if(id==1)
	{
		document.getElementById('verw1').style.display="inline";
		document.getElementById('verw1_1').style.display="inline";
		document.getElementById('verw2').style.display="none";
		document.getElementById('verw2_1').style.display="none";
		document.getElementById('verw3').style.display="none";
		document.getElementById('verw3_1').style.display="none";
		document.getElementById('verw4').style.display="none";
		document.getElementById('verw4_1').style.display="none";
		document.getElementById('verw5').style.display="none";
		document.getElementById('verw5_1').style.display="none";
		document.getElementById('exlibrisver').value="";
		document.getElementById('bondver').value="";
		document.getElementById('bibliowebver').value="";
		document.getElementById('anderesname').value="";
		document.getElementById('anderesver').value="";
	}
	if(id==2)
	{
		document.getElementById('verw1').style.display="none";
		document.getElementById('verw1_1').style.display="none";
		document.getElementById('verw2').style.display="inline";
		document.getElementById('verw2_1').style.display="inline";
		document.getElementById('verw3').style.display="none";
		document.getElementById('verw3_1').style.display="none";
		document.getElementById('verw4').style.display="none";
		document.getElementById('verw4_1').style.display="none";
		document.getElementById('verw5').style.display="none";
		document.getElementById('verw5_1').style.display="none";
		document.getElementById('litteraver').value="";
		document.getElementById('bondver').value="";
		document.getElementById('bibliowebver').value="";
		document.getElementById('anderesname').value="";
		document.getElementById('anderesver').value="";
	}
	if(id==3)
	{
		document.getElementById('verw1').style.display="none";
		document.getElementById('verw1_1').style.display="none";
		document.getElementById('verw2').style.display="none";
		document.getElementById('verw2_1').style.display="none";
		document.getElementById('verw3').style.display="inline";
		document.getElementById('verw3_1').style.display="inline";
		document.getElementById('verw4').style.display="none";
		document.getElementById('verw4_1').style.display="none";
		document.getElementById('verw5').style.display="none";
		document.getElementById('verw5_1').style.display="none";
		document.getElementById('litteraver').value="";
		document.getElementById('exlibrisver').value="";
		document.getElementById('bibliowebver').value="";
		document.getElementById('anderesname').value="";
		document.getElementById('anderesver').value="";
	}
	if(id==4)
	{
		document.getElementById('verw1').style.display="none";
		document.getElementById('verw1_1').style.display="none";
		document.getElementById('verw2').style.display="none";
		document.getElementById('verw2_1').style.display="none";
		document.getElementById('verw3').style.display="none";
		document.getElementById('verw3_1').style.display="none";
		document.getElementById('verw4').style.display="inline";
		document.getElementById('verw4_1').style.display="inline";
		document.getElementById('verw5').style.display="none";
		document.getElementById('verw5_1').style.display="none";
		document.getElementById('litteraver').value="";
		document.getElementById('exlibrisver').value="";
		document.getElementById('bondver').value="";
		document.getElementById('anderesname').value="";
		document.getElementById('anderesver').value="";
	}
	if(id==5)
	{
		document.getElementById('verw1').style.display="none";
		document.getElementById('verw1_1').style.display="none";
		document.getElementById('verw2').style.display="none";
		document.getElementById('verw2_1').style.display="none";
		document.getElementById('verw3').style.display="none";
		document.getElementById('verw3_1').style.display="none";
		document.getElementById('verw4').style.display="none";
		document.getElementById('verw4_1').style.display="none";
		document.getElementById('verw5').style.display="inline";
		document.getElementById('verw5_1').style.display="inline";
		document.getElementById('litteraver').value="";
		document.getElementById('exlibrisver').value="";
		document.getElementById('bondver').value="";
		document.getElementById('bibliowebver').value="";
	}
}

function andereoeffentlichkeitsarbeit(element)
{
    if(element.checked==true)
    {
        document.getElementById('andere_oeffentlichkeitsarbeit_div').style.display="inline";
		
    }
        else
        {
           document.getElementById('andere_oeffentlichkeitsarbeit_div').style.display="none";
		
        }            
   
}
function calculatesum(element)
{
    
    if(isNaN(document.getElementById('anz_lesungen').value)==false && isNaN(document.getElementById('anz_wokshops').value)==false && isNaN(document.getElementById('anz_bibliotheks_schulungen').value)==false & isNaN(document.getElementById('anz_sonstige_va').value)==false )
    {
        sum=parseInt(document.getElementById('anz_lesungen').value)+parseInt(document.getElementById('anz_wokshops').value)+parseInt(document.getElementById('anz_bibliotheks_schulungen').value)+parseInt(document.getElementById('anz_sonstige_va').value);
    
    }
    document.getElementById('sum_va').value=sum;
    //document.getElementById('anz_wokshops').value
    //document.getElementById('anz_bibliotheks_schulungen').value;
    // document.getElementById('anz_sonstige_va').value;
}
   