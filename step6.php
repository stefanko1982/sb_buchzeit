<?php
/**
* Buchzeit APS-Verwaltung
*
* @author Manuel Postlbauer
* @version 0.9
*/

/**
* step 6 (Teil von formular.php)
*/
if(!isset($_SESSION['LOGGEDIN'])){
	header("location: index.php");
	exit();
}
if($_SESSION['stepcounter']==6){

	$_SESSION['oeffnungstunden']=$_POST['oeffnungstunden'];
	$_SESSION['webopac']=$_POST['webopac'];
	$_SESSION['homepage']=$_POST['homepage'];
	$_SESSION['mail']=$_POST['mail'];
	$_SESSION['phone']=$_POST['phone'];
	$_SESSION['bibliothekaranzahl']=$_POST['bibliothekaranzahl'];
	$_SESSION['abgwochenstunden']=$_POST['abgwochenstunden'];
	$_SESSION['mitarbeitschueler']=$_POST['mitarbeitschueler'];
	$_SESSION['mitarbeiteltern']=$_POST['mitarbeiteltern'];
	$_SESSION['ankaufsbudget']=$_POST['ankaufsbudget'];

	$_SESSION['stepcounter']+=1;
}
else{
	$_SESSION['stepcounter']-=1;
}
include("header.php");
?>
<form action="formular.php?step=7" method="post" onsubmit="return checkFields(6)">
	<h1>Daten editieren</h1>
	<img src="images/step6.png" alt=""/>
	<br/>
	<br/>
	<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="481px">
		<tr>
			<th colspan="2">Funktion und Nutzung</th>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Funktion der Schulbibliothek bei der Leseförderung:</td>
			<td><input type="checkbox" name="usage_reading" value="1" <?php if(!empty($_SESSION['usage_reading'])) echo "checked"; ?> /></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Funktion der Schulbibliothek bei individualisiertem Lernen:</td>
			<td><input type="checkbox" name="usage_learning" value="1" <?php if(!empty($_SESSION['usage_learning'])) echo "checked"; ?> /></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Funktion der Schulbibliothek beim Erwerb von Medienkompetenz:</td>
			<td><input type="checkbox" name="usage_media" value="1" <?php if(!empty($_SESSION['usage_media'])) echo "checked"; ?> /></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td colspan="2">Andere Funktionen, nämlich:</td>
		</tr>
		<tr class="listschooltablealtbg">
			<td colspan="2"><textarea name="usage_other" cols="55" rows="3"><?php if(isset($_SESSION['usage_other'])) echo $_SESSION['usage_other'];?></textarea></td>
		</tr>
	</table>
	
	<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="481px">
		<tr>
			<th colspan="2">Unterricht in der Schulbibliothek (alle Klassen/alle Lehrkräfte)</th>
		</tr>
		<tr class="listschooltablealtbg">
			<td>täglich</td>
			<td><input type="checkbox" name="dailyclass" value="1" <?php if(!empty($_SESSION['dailyclass'])) echo "checked"; ?> /></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>mehrmals pro Woche</td>
			<td><input type="checkbox" name="weeklyclass" value="1" <?php if(!empty($_SESSION['weeklyclass'])) echo "checked"; ?> /></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>mehrmals im Monat</td>
			<td><input type="checkbox" name="monthlyclass" value="1" <?php if(!empty($_SESSION['monthlyclass'])) echo "checked"; ?> /></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>sporadisch</td>
			<td><input type="checkbox" name="sporadicclass" value="1" <?php if(!empty($_SESSION['sporadicclass'])) echo "checked"; ?> /></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>gar nicht</td>
			<td><input type="checkbox" name="noclass" value="1" <?php if(!empty($_SESSION['noclass'])) echo "checked"; ?> /></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Nutzung der Schulbibliothek für Veranstaltungen außerhalb der Unterrichtszeit:</td>
			<td><input type="checkbox" name="outsideclass" value="1" <?php if(!empty($_SESSION['outsideclass'])) echo "checked"; ?>/></td>
		</tr>
	</table>
	
	<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="481px">
		<tr>
			<th colspan="2">Anregungen</th>
		</tr>
		<tr class="listschooltablealtbg">
			<td colspan="2">Was funktioniert gut in unserer Schulbibliothek und sollte daher beibehalten werden?</td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td colspan="2"><textarea name="comments_pos" cols="55" rows="6"><?php if(isset($_SESSION['comments_pos'])) echo $_SESSION['comments_pos'];?></textarea></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td colspan="2">Was funktioniert in unserer Schulbibliothek nicht und sollte daher unbedingt geändert werden?</td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td colspan="2"><textarea name="comments_neg" cols="55" rows="6"><?php if(isset($_SESSION['comments_neg'])) echo $_SESSION['comments_neg'];?></textarea></td>
		</tr>
	</table>
	
	<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="481px">
		<tr>
			<th colspan="2">Kontaktperson (ausgebildete Schulbibliothekar/in)</th>
		</tr>
		<tr class="listschooltablealtbg">
			<td width="200px">Name:&nbsp;<span class="starcolor">*</span></td>
			<td><input type="text" name="contact_name" id="contact_name" size="45" value="<?php if(isset($_SESSION['contact_name'])) echo $_SESSION['contact_name'];?>" /></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>E-Mail-Adresse:&nbsp;<span class="starcolor">*</span></td>
			<td><input type="text" name="contact_mail" id="contact_mail" size="45" value="<?php if(isset($_SESSION['contact_mail'])) echo $_SESSION['contact_mail'];?>" /></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Telefon:&nbsp;<span class="starcolor">*</span></td>
			<td><input type="text" name="contact_phone" id="contact_phone" size="45" value="<?php if(isset($_SESSION['contact_phone'])) echo $_SESSION['contact_phone'];?>"/></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>FAX:&nbsp;</td>
			<td><input type="text" name="contact_fax" id="contact_fax" size="45" value="<?php if(isset($_SESSION['contact_fax'])) echo $_SESSION['contact_fax'];?>"/></td>
		</tr>
	</table>
	
	<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="481px">
		<tr>
			<th colspan="2">2.Kontaktperson (ausgebildete Schulbibliothekar/in)</th>
		</tr>
		<tr class="listschooltablealtbg">
			<td width="200px">Name:</td>
			<td><input type="text" name="contact_name2" id="contact_name2" size="45" value="<?php if(isset($_SESSION['contact_name2'])) echo $_SESSION['contact_name2'];?>" /></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>E-Mail-Adresse:</td>
			<td><input type="text" name="contact_mail2" id="contact_mail2" size="45" value="<?php if(isset($_SESSION['contact_mail2'])) echo $_SESSION['contact_mail2'];?>" /></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Telefon:</td>
			<td><input type="text" name="contact_phone2" id="contact_phone2" size="45" value="<?php if(isset($_SESSION['contact_phone2'])) echo $_SESSION['contact_phone2'];?>" /></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>FAX:</td>
			<td><input type="text" name="contact_fax2" id="contact_fax2" size="45" value="<?php if(isset($_SESSION['contact_fax2'])) echo $_SESSION['contact_fax2'];?>" /></td>
		</tr>
	</table>
	<input type="button" value="" onclick="document.location.href='formular.php?step=5';" class="zurueckbutton"/>
	<input type="submit" value="" class="weiterbutton"/>
</form>
<?php
include("footer.php");
?>