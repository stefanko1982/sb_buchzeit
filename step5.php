<?php
/**
* Buchzeit APS-Verwaltung
*
* @author Manuel Postlbauer
* @version 0.9
*/

/**
* step 5 (Teil von formular.php)
*/
if(!isset($_SESSION['LOGGEDIN'])){
	header("location: index.php");
	exit();
}
if($_SESSION['stepcounter']==5){
	$_SESSION['belletristik']=$_POST['belletristik'];
	$_SESSION['zeitschriften']=$_POST['zeitschriften'];
	$_SESSION['tageszeitungen']=$_POST['tageszeitungen'];
	$_SESSION['cddvd']=$_POST['cddvd'];
	$_SESSION['downloadang']=$_POST['downloadang'];
	$_SESSION['videos']=$_POST['videos'];
	$_SESSION['spiele']=$_POST['spiele'];
	$_SESSION['mehrfachtitel']=$_POST['mehrfachtitel'];
	$_SESSION['exemplareprotitel']=$_POST['exemplareprotitel'];
	
	$_SESSION['stepcounter']+=1;
}
else{
	$_SESSION['stepcounter']-=1;
}
include("header.php");
?>
<form action="formular.php?step=6" method="post" onsubmit="return checkFields(5)">
	<h1>Daten editieren</h1>
	<img src="images/step5.png" alt=""/>
	<br/>
	<br/>
	<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="481px">
		<tr>
			<th colspan="2">Öffnungszeiten und Erreichbarkeit</th>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Öffnungszeiten in Anzahl der Stunden pro Woche:</td>
			<td><input type="text" name="oeffnungstunden" id="oeffnungstunden" size="45" value="<?php if(isset($_SESSION['oeffnungstunden'])) echo $_SESSION['oeffnungstunden']; else echo"0"; ?>" /></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>WebOPAC? - URL:</td>
			<td><input type="text" name="webopac" id="webopac" size="45" value="<?php if(isset($_SESSION['webopac'])) echo $_SESSION['webopac'];?>" /></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Homepage? - URL:</td>
			<td><input type="text" name="homepage" id="homepage" size="45" value="<?php if(isset($_SESSION['homepage'])) echo $_SESSION['homepage'];?>" /></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>E-Mail-Adresse:&nbsp;<span class="starcolor">*</span></td>
			<td><input type="text" name="mail" id="mail" size="45" value="<?php if(isset($_SESSION['mail'])) echo $_SESSION['mail'];?>" /></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Telefon:&nbsp;<span class="starcolor">*</span></td>
			<td><input type="text" name="phone" id="phone" size="45" value="<?php if(isset($_SESSION['phone'])) echo $_SESSION['phone'];?>" /></td>
		</tr>
	</table>
	
	<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="481px">
		<tr>
			<th colspan="2">Betreuung</th>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Ausgebildete Schulbibliothekar/innen (Anzahl):</td>
			<td><input type="text" name="bibliothekaranzahl" id="bibliothekaranzahl" value="<?php if(isset($_SESSION['bibliothekaranzahl'])) echo $_SESSION['bibliothekaranzahl']; else echo"0"; ?>"/></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Abgeltung: Unterschreitung - Anzahl der Wochenstunden:</td>
			<td><input type="text" name="abgwochenstunden" id="abgwochenstunden" value="<?php if(isset($_SESSION['abgwochenstunden'])) echo $_SESSION['abgwochenstunden']; else echo"0"; ?>"/></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Mitarbeit von Schüler/innen:</td>
			<td><input type="checkbox" name="mitarbeitschueler" value="1" <?php if(!empty($_SESSION['mitarbeitschueler'])) echo "checked"; ?> /></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Mitarbeit von Eltern:</td>
			<td><input type="checkbox" name="mitarbeiteltern" value="1" <?php if(!empty($_SESSION['mitarbeiteltern'])) echo "checked"; ?> /></td>
		</tr>
	</table>
	
	<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="481px">
		<tr>
			<th colspan="2">Budget</th>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Jährliches Ankaufsbudget für Medien / durchschnittlich:&nbsp;<span class="starcolor">*</span></td>
			<td><input type="text" name="ankaufsbudget" value="<?php if(isset($_SESSION['ankaufsbudget'])) echo $_SESSION['ankaufsbudget']; else echo"0"; ?>" id="ankaufsbudget"/></td>
		</tr>
	</table>
	<input type="button" value="" onclick="document.location.href='formular.php?step=4';" class="zurueckbutton"/>
	<input type="submit" value="" class="weiterbutton"/>
</form>
<?php
include("footer.php");
?>