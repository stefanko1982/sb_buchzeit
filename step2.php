<?php
/**
* Buchzeit APS-Verwaltung
*
* @author Manuel Postlbauer
* @version 0.9
*/

/**
* step 2 (Teil von formular.php)
*/
if(!isset($_SESSION['LOGGEDIN'])){
	header("location: index.php");
	exit();
}
//Damit Daten nicht verloren gehen beim drücken des Zurückbuttons Stepcounter hinkt immer ein Step nach beim Zurückgehen
//damit nicht die leeren Post-Variablen in die Session Variablen gespeichert werden.
if($_SESSION['stepcounter']==2){

$_SESSION['school_id']=$_POST['school_id'];
$_SESSION['school_name']=$_POST['school_name'];
$_SESSION['address_plz']=$_POST['address_plz'];
$_SESSION['address_school_loc']=$_POST['address_school_loc'];
$_SESSION['address_street']=$_POST['address_street'];
$_SESSION['schooltype']=$_POST['schooltype'];
$_SESSION['gemeinde']=$_POST['gemeinde'];
$_SESSION['schulerhalter']=$_POST['schulerhalter'];

$_SESSION['stepcounter']+=1;
}
else{
	$_SESSION['stepcounter']-=1;
}
include("header.php");
?>
<form action="formular.php?step=3" method="post" onsubmit="return checkFields(2)">
	<h1>Daten editieren</h1>
	<img src="images/step2.png" alt=""/>
	<br/>
	<br/>
	<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="481px">
		<tr>
			<th colspan="2">Kategorisierung</th>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Anzahl der Klassen: </td>
			<td><input type="text" name="classroomcount" id="classroomcount" value="<?php if(isset($_SESSION['classroomcount'])) echo $_SESSION['classroomcount']; else echo"0" ?>"/></td>
		</tr>
		<?php
		//Damit man nicht auf einmal sagen kann man hat keine Bibliothek mehr
		?>
		<tr class="listschooltablealtbg2" <?php if($_SESSION['GLOBAL_BIBLIOTHEK_ID']!=0) echo"style='display:none;'";?>>
			<td>Schulbibliothek:</td>
			<td><input type="checkbox" name="librarykind_0" value="1" id="librarykind_0" <?php if(!empty($_SESSION['librarykind_0'])) echo "checked"; ?>/></td>
		</tr>
		<tr class="listschooltablealtbg" <?php if($_SESSION['GLOBAL_BIBLIOTHEK_ID']!=0) echo"style='display:none;'";?>>
			<td>Kombination 2 oder mehrere Schulen:</td>
			<td><input type="checkbox" name="librarykind_1" value="1" id="librarykind_1" <?php if(!empty($_SESSION['librarykind_1'])) echo "checked"; ?>/></td>
		</tr>
		<tr class="listschooltablealtbg2" <?php if($_SESSION['GLOBAL_BIBLIOTHEK_ID']!=0) echo"style='display:none;'";?>>
			<td>Kombination mit öffentlicher Bibliothek:</td>
			<td><input type="checkbox" name="librarykind_2" value="1" id="librarykind_2" <?php if(!empty($_SESSION['librarykind_2'])) echo "checked"; ?>/></td>
		</tr>
                <tr class="listschooltablealtbg2" <?php if($_SESSION['GLOBAL_BIBLIOTHEK_ID']!=0) echo"style='display:none;'";?>>
			<td>Andere Varianten:</td>
			<td><input type="checkbox" name="librarykind_3" value="1" id="librarykind_3" <?php if(!empty($_SESSION['librarykind_3'])) echo "checked"; ?>/></td>
		</tr>
                
	</table>
		<?php
		//Damit man nicht auf einmal sagen kann man hat keine Bibliothek mehr
		if($_SESSION['GLOBAL_BIBLIOTHEK_ID']==0){
		?>
			<div class="important-text">Wenn die Schule über keine formell eingerichtete Schulbibliothek (eigener Raum, ausgebildete/r Schulbibliothekar/in, entsprechender Medienbestand) verfügt, bitte nur eine Leermeldung abgeben!</div>
		<?php
		}
		?>
	<br />
	<input type="button" value="" onclick="document.location.href='formular.php?step=1';" class="zurueckbutton"/>
	<input type="submit" value="" class="weiterbutton"/>
</form>
<?php
include("footer.php");
?>