<?php
session_start();
if(isset($_POST['Submit']))
{
	include("localconf.php");

	$error="";
	$schulkennzahl = $mysqli->real_escape_string($_POST['kennzahlfield']);
	$plz= $mysqli->real_escape_string($_POST['plzfield']);
	$schulname="";

	if(($schulkennzahl == '')||($plz == '')){
		$error= 'Schulkennzahl oder PLZ fehlt.';
	}

	if(!$error){
		$qry="SELECT * FROM schuladressen WHERE schulkennzahl='".$schulkennzahl."' AND postleitzahl='".$plz."'";                
		$result=$mysqli->query($qry);

		if($result) {
                    
			if(mysqli_num_rows($result) == 1) {
                            $resultarray = $result->fetch_assoc();
				$schulname=$resultarray['schultitel'];
				//PHP Zufallspasswortgenerierung
				$password;
				$pool = "qwertzupasdfghkyxcvbnm";
				$pool .= "23456789";
				$pool .= "WERTZUPLKJHGFDSAYXCVBNM";
				srand ((double)microtime()*1000000);
				for($index = 0; $index < 5; $index++)
				{
					$password .= substr($pool,(rand()%(strlen ($pool))), 1);
				}
				//Neues Passwort in die DB MD5 verschlüsselt eintragen
				$qry="UPDATE schuladressen SET password='".md5($password)."' WHERE schulkennzahl='".$schulkennzahl."' AND postleitzahl='".$plz."'";
				$result=$mysqli->query($qry);
				if($result) {
					//$_SESSION['ERROR'] = 'Ihr neues Passwort wurde Ihnen zugesendet';
					$_SESSION['mail_new_password']=$password;
					$_SESSION['mail_schulkennzahl']=$schulkennzahl;
					$_SESSION['mail_postleitzahl']=$plz;
					$_SESSION['mail_schulname']=$schulname;
					header("location: password_recover_mail.php");
					exit();
				}
				else {
					die("Query failed");
				}
			
			}
			else {
				$error = 'Ungültige Schulkennzahl und/oder PLZ.';
			}
		}
		else {
			die("Query failed");
		}
	}
}
include("header.php");
?>
<h1>Neues Kennwort</h1>
<p>Um ein neues Kennwort per E-Mail zu erhalten geben sie Ihre <b>Schulkennzahl</b> und Ihre <b>Postleitzahl</b> ein.</p>
<form id="password_recovery" name="password_recovery" method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>">
<table class="listschooltable" width="400" cellpadding="5" cellspacing="1">
	<tr>
		<th colspan="2">Neues Kennwort</th>
	</tr>
	<tr class="listschooltablealtbg">
		<td>Schulkennzahl:</td>
		<td><input type="text" name="kennzahlfield" id="kennzahlfield"/></td>
	</tr>
		<tr class="listschooltablealtbg2">
		<td>Postleitzahl:</td>
		<td><input type="text" name="plzfield" id="plzfield"/></td>
	</tr>
   <tr class="listschooltablealtbg">
      <td>&nbsp;</td>
      <td><input type="submit" name="Submit" value="" class="weiterbutton"/></td>
    </tr>
</table>
<div class="important-text">
	<?php
        if(!empty($error))
		echo $error;
	?>
</div>
</form>
<?php
include("footer.php");
?>