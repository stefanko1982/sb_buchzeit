<?php
/**
* Buchzeit APS-Verwaltung
*
* @author Manuel Postlbauer
* @version 0.9
*/

/**
* step 8 (Teil von formular.php)
* Hier wir der Inahlt des Formulars in die Datenbank gespeichert.
*/

//Ist man nicht eingeloggt
//dann wird auf index.php weitergeleitet
session_start();
if(!isset($_SESSION['LOGGEDIN'])){
	header("location: index.php");
	exit();
}
//Wurde noch nicht aus der Datenbank gelesen, dann wird man auf 
//formular.php?step=1 weitergeleitet.
if($_SESSION['ISREADFROMDB']!=1){
	header("location: formular.php?step=1");
	exit();
}
//Weil die neuen Eintr�ge gespeichert werden müssen beim erneuten Formularbearbeiten
//die neuen Werte aus der Datenbank gelesen werden. Deshalb wird die Variable gel�scht
$_SESSION['ISREADFROMDB']=0;
//Datenbankverbindung
include("localconf.php");
//Schreibt die Formulardaten von Step 1 und Step 2 in die Datenbank
//array_search sucht sich zum Namen des Elements im Array den Index raus, damit wieder eine Zahl inder Datenbank steht


function sendRegistrationMail(){
	$message = $_SESSION['school_name']." mit der Schulkennzahl:".$_SESSION['school_id']." hat Daten geändert";
	// In case any of our lines are larger than 70 characters, we should use wordwrap()
	$message = wordwrap($message, 70);
	// Send
	global $emailaddress;
	$emailsubject = "SB-Verwaltung Datenänderung für ".$_SESSION['school_id'];
	mail($emailaddress, $emailsubject, $message);
}
			
$qry="UPDATE schuladressen SET schulkennzahl='".$mysqli->real_escape_string($_SESSION['school_id'])."',
                               schultitel='".$mysqli->real_escape_string($_SESSION['school_name'])."', 
							   postleitzahl='".$mysqli->real_escape_string($_SESSION['address_plz'])."', 
							   ort='".$mysqli->real_escape_string($_SESSION['address_school_loc'])."', 
							   strasse_hausnummer='".$mysqli->real_escape_string($_SESSION['address_street'])."', 
							   schulerhalter_gemeinde='".$mysqli->real_escape_string($_SESSION['gemeinde'])."', 
							   schulerhalter_privat='".$mysqli->real_escape_string($_SESSION['schulerhalter'])."', 
							   schulart='".$mysqli->real_escape_string($_SESSION['schooltype'])."', 
							   klassenanzahl='".$mysqli->real_escape_string($_SESSION['classroomcount'])."', 
							   eigene_bibliothek ='".$mysqli->real_escape_string($_SESSION['librarykind_0'])."', 
							   gemeinsame_bibliothek ='".$mysqli->real_escape_string($_SESSION['librarykind_1'])."', 
							   oeff_gemeinsame_bibliothek ='".$mysqli->real_escape_string($_SESSION['librarykind_2'])."', 
        andere_bibliothek ='".$mysqli->real_escape_string($_SESSION['librarykind_3'])."',
            bundesland= '". $mysqli->real_escape_string($_SESSION['address_bundesland'])."' 
        WHERE id='".$mysqli->real_escape_string($_SESSION['USERID'])."'";

$result=$mysqli->query($qry);

if($result) {
	//echo "Ihre Daten wurden erfolgreich in der Datenbank gespeichert!";
}
else {
	echo $mysqli->error();
	die("Query failed");
}
//Ist eine Schulbibliothek vorhanden, dann werden die Informationen geupdatet, sonst wir eine neue Bibliothek mit neuen Informationen angelegt
//Die Sessionvariable $_SESSION['GLOBAL_BIBLIOTHEK_ID'] ist vom Auslesevorgang von formular.php bekannt.
if(isset($_SESSION['GLOBAL_BIBLIOTHEK_ID'])){	
	
	//Ist ein Raum vorhanden, wenn ja update sonst insert statement
	if(isset($_SESSION['GLOBAL_RAUM_EINRICHTUNG_ID'])){
		$qry="UPDATE raum_einrichtung SET raumgroesse='".$mysqli->real_escape_string($_SESSION['roomsize'])."',
										  lage_zentral='".$mysqli->real_escape_string($_SESSION['zentral'])."', 
										  lage_nahe_edv='".$mysqli->real_escape_string($_SESSION['edvsaele'])."',
										  lage_schwer_zu_erreichen='".$mysqli->real_escape_string($_SESSION['schwerzuerr'])."',
										  anzahl_arbeitsp='".$mysqli->real_escape_string($_SESSION['arbeitsplatzanzahl'])."',
										  anzahl_arbeitsp_i='".$mysqli->real_escape_string($_SESSION['internetarbeits'])."',
										  tvgeraet='".$mysqli->real_escape_string($_SESSION['tvgeraet'])."',
										  beamer='".$mysqli->real_escape_string($_SESSION['beamer'])."',
										  drucker='".$mysqli->real_escape_string($_SESSION['printer'])."',
										  scanner='".$mysqli->real_escape_string($_SESSION['scanner'])."',
										  hoerbuchstation='".$mysqli->real_escape_string($_SESSION['hoerbuchstation'])."',
										  kopiergeraet='".$mysqli->real_escape_string($_SESSION['kopierger'])."'
										  WHERE id='".$mysqli->real_escape_string($_SESSION['GLOBAL_RAUM_EINRICHTUNG_ID'])."'";
		$result=$mysqli->query($qry);
		if($result) {
			//echo "Ihre Daten wurden erfolgreich in der Datenbank gespeichert!";
		}
		else {
				echo $mysqli->error();
				die("Query failed");
		}
	}
	else{
		$qry="INSERT INTO raum_einrichtung SET raumgroesse='".$mysqli->real_escape_string($_SESSION['roomsize'])."',
											   lage_zentral='".$mysqli->real_escape_string($_SESSION['zentral'])."', 
											   lage_nahe_edv='".$mysqli->real_escape_string($_SESSION['edvsaele'])."', 
											   lage_schwer_zu_erreichen='".$mysqli->real_escape_string($_SESSION['schwerzuerr'])."',
											   anzahl_arbeitsp='".$mysqli->real_escape_string($_SESSION['arbeitsplatzanzahl'])."',
											   anzahl_arbeitsp_i='".$mysqli->real_escape_string($_SESSION['internetarbeits'])."',
											   tvgeraet='".$mysqli->real_escape_string($_SESSION['tvgeraet'])."',
											   beamer='".$mysqli->real_escape_string($_SESSION['beamer'])."',
											   drucker='".$mysqli->real_escape_string($_SESSION['printer'])."',
											   scanner='".$mysqli->real_escape_string($_SESSION['scanner'])."',
											   hoerbuchstation='".$mysqli->real_escape_string($_SESSION['hoerbuchstation'])."',
											   kopiergeraet='".$mysqli->real_escape_string($_SESSION['kopierger'])."'";
		$result=$mysqli->query($qry);
		if($result) {
			//echo "Ihre Daten wurden erfolgreich in der Datenbank gespeichert!";
			//Holt sich die ID des gerade Eingetragenen Raums. Wird ben�tigt als fk_raum_einrichtung_id f�r die Biblitohekstabelle
			$raumeinrichtungid = $mysqli->insert_id;
			
			//Schreibt die id als fk_raum_einrichtung_id in die bibliothekstabelle
			$qry="UPDATE bibliothek SET fk_raum_einrichtung_id='".$mysqli->real_escape_string($raumeinrichtungid)."' WHERE id='".$mysqli->real_escape_string($_SESSION['GLOBAL_BIBLIOTHEK_ID'])."' ";
			$result=$mysqli->query($qry);
			if($result) {
				//echo "Ihre Daten wurden erfolgreich in der Datenbank gespeichert!";
			}
			else {
					echo $mysqli->error();
					die("Query failed");
			}
		}
		else {
			die("Query failed");
		}
	}
	
	
	
	//Ist ein Medienbestand vorhanden, wenn ja update sonst insert statement
	if(isset($_SESSION['GLOBAL_MEDIENBESTAND_ID'])){
		$qry="UPDATE bibliothek_medienbest SET printmedien_belletristik='".$mysqli->real_escape_string($_SESSION['belletristik'])."',
												  printmedien_zeitschriften='".$mysqli->real_escape_string($_SESSION['zeitschriften'])."', 
												  printmedien_zeitungen='".$mysqli->real_escape_string($_SESSION['tageszeitungen'])."', 
												  digitale_medien='".$mysqli->real_escape_string($_SESSION['cddvd'])."', 
												  downloadangebote='".$mysqli->real_escape_string($_SESSION['downloadang'])."', 
												  av_medien='".$mysqli->real_escape_string($_SESSION['videos'])."', 
												  spiele='".$mysqli->real_escape_string($_SESSION['spiele'])."', 
												  anz_titel_klassenst='".$mysqli->real_escape_string($_SESSION['mehrfachtitel'])."', 
                                                                                                  art_oeffentlichkeitsarbeit='".$mysqli->real_escape_string(json_encode($_SESSION['art_oeffentlichkeitsarbeit']))."', 
                                                                                                  andere_oeffentlichkeitsarbeit='".$mysqli->real_escape_string($_SESSION['andere_oeffentlichkeitsarbeit'])."', 
                                                                                                  anz_ma_ohne_abg_ausbildung='".$mysqli->real_escape_string($_SESSION['anz_ma_ohne_abg_ausbildung'])."', 
                                                                                                  anz_ma_mit_abg_ausbildung='".$mysqli->real_escape_string($_SESSION['anz_ma_mit_abg_ausbildung'])."', 
                                                                                                  anz_sonstige_va='".$mysqli->real_escape_string($_SESSION['anz_sonstige_va'])."', 
                                                                                                  anz_wokshops='".$mysqli->real_escape_string($_SESSION['anz_wokshops'])."', 
                                                                                                  anz_bibliotheks_schulungen='".$mysqli->real_escape_string($_SESSION['anz_bibliotheks_schulungen'])."', 
                                                                                                  anz_lesungen='".$mysqli->real_escape_string($_SESSION['anz_lesungen'])."', 
                                                                                                  kom_bibliotheken='".$mysqli->real_escape_string($_SESSION['kom_bibliotheken'])."', 
                                                                                                  ext_leser='".$mysqli->real_escape_string($_SESSION['ext_leser'])."', 
                                                                                                  anz_erw_in_schulbetrieb='".$mysqli->real_escape_string($_SESSION['anz_erw_in_schulbetrieb'])."', 
                                                                                                  anz_schueler='".$mysqli->real_escape_string($_SESSION['anz_schueler'])."', 
                                                                                                  medienabgang='".$mysqli->real_escape_string($_SESSION['medienabgang'])."', 
                                                                                                  medienzugang='".$mysqli->real_escape_string($_SESSION['medienzugang'])."',                
												  anzahl_titel='".$mysqli->real_escape_string($_SESSION['exemplareprotitel'])."' WHERE id='".$mysqli->real_escape_string($_SESSION['GLOBAL_MEDIENBESTAND_ID'])."'";
		
                $result=$mysqli->query($qry);
		if($result) {
			//echo "Ihre Daten wurden erfolgreich in der Datenbank gespeichert!";
		}
		else {
				echo $mysqli->error();
				die("Query failed");
		}
	}
	else{
		$qry="INSERT INTO bibliothek_medienbest SET printmedien_belletristik='".$mysqli->real_escape_string($_SESSION['belletristik'])."',
													printmedien_zeitschriften='".$mysqli->real_escape_string($_SESSION['zeitschriften'])."',
													printmedien_zeitungen='".$mysqli->real_escape_string($_SESSION['tageszeitungen'])."',
													digitale_medien='".$mysqli->real_escape_string($_SESSION['cddvd'])."',
													downloadangebote='".$mysqli->real_escape_string($_SESSION['downloadang'])."',
													av_medien='".$mysqli->real_escape_string($_SESSION['videos'])."',
													spiele='".$mysqli->real_escape_string($_SESSION['spiele'])."',
													anz_titel_klassenst='".$mysqli->real_escape_string($_SESSION['mehrfachtitel'])."',
													anzahl_titel='".$mysqli->real_escape_string($_SESSION['exemplareprotitel'])."'";
		$result=$mysqli->query($qry);
		if($result) {
			//echo "Ihre Daten wurden erfolgreich in der Datenbank gespeichert!";
			//Holt sich die ID des gerade eingetragenen Medienbestands. Wird ben�tigt als fk_bibliothek_medienbest_id f�r die Biblitohekstabelle
			$medienbestandid= $mysqli->insert_id;
			
			//Schreibt die id als fk_bibliothek_medienbestand_id in die bibliothekstabelle
			$qry="UPDATE bibliothek SET fk_bibliothek_medienbest_id='".$mysqli->real_escape_string($medienbestandid)."' WHERE id='".$mysqli->real_escape_string($_SESSION['GLOBAL_BIBLIOTHEK_ID'])."' ";
			$result=$mysqli->query($qry);
			if($result) {
				//echo "Ihre Daten wurden erfolgreich in der Datenbank gespeichert!";
			}
			else {
					echo $mysqli->error();
					die("Query failed");
			}
		}
		else {
			echo $mysqli->error();
			die("Query failed");
		}
	}
	
	
	//Ist ein Bibliotheksverwaltungsprogramm vorhanden, wenn ja update sonst insert statement
	if($_SESSION['GLOBAL_VERW_PROG_ID']!=0){
		$name;
		$version;
		if(isset($_SESSION['islittera'])){
			$name="LITTERA";
			$version=$_SESSION['litteraver'];
		}
		if(isset($_SESSION['isexlibris'])){
			$name="EXLIBRIS";
			$version=$_SESSION['exlibrisver'];
		}
		if(isset($_SESSION['isbond'])){
			$name="BOND";
			$version=$_SESSION['bondver'];
		}
		if(isset($_SESSION['isbiblioweb'])){
			$name="BIBLIOWEB";
			$version=$_SESSION['bibliowebver'];
		}
		if(isset($_SESSION['isanderes'])){
			$name=$_SESSION['anderesname'];
			$version=$_SESSION['anderesver'];
		}
			
		$qry="UPDATE bibliothek_verw_prog SET name='".$mysqli->real_escape_string($name)."', version='".$mysqli->real_escape_string($version)."' WHERE id='".$mysqli->real_escape_string($_SESSION['GLOBAL_VERW_PROG_ID'])."' ";
		$result=$mysqli->query($qry);
		if($result) {
			//echo "Ihre Daten wurden erfolgreich in der Datenbank gespeichert!";
		}
		else {
				echo $mysqli->error();
			    die("Query failed");
		}
	}
	else{
		$name;
		$version;
		if(isset($_SESSION['islittera'])){
			$name="LITTERA";
			$version=$_SESSION['litteraver'];
		}
		if(isset($_SESSION['isexlibris'])){
			$name="EXLIBRIS";
			$version=$_SESSION['exlibrisver'];
		}
		if(isset($_SESSION['isbond'])){
			$name="BOND";
			$version=$_SESSION['bondver'];
		}
		if(isset($_SESSION['isbiblioweb'])){
			$name="BIBLIOWEB";
			$version=$_SESSION['bibliowebver'];
		}
		if(isset($_SESSION['isanderes'])){
			$name=$_SESSION['anderesname'];
			$version=$_SESSION['anderesver'];
		}
		//Wenn die Felder leer sind, weil der Benutzer nichts eingetragen hat, dann soll auch keine Leerzeile in der DB stehen
		if(($name!="")){
			$qry="INSERT INTO bibliothek_verw_prog SET name='".$mysqli->real_escape_string($name)."', version='".$mysqli->real_escape_string($version)."' ";
			$result=$mysqli->query($qry);
			if($result) {
				//echo "Ihre Daten wurden erfolgreich in der Datenbank gespeichert!";
				//Holt sich die ID des gerade eingetragenen Bibliothekssoftware. Wird ben�tigt als fk_bibliothek_verw_prog_id f�r die Biblitohekstabelle
				$verwprogid = $mysqli->insert_id;
				
				//Schreibt die id als fk_bibliothek_verw_prog_id  in die bibliothekstabelle
				$qry="UPDATE bibliothek SET fk_bibliothek_verw_prog_id='".$mysqli->real_escape_string($verwprogid)."' WHERE id='".$mysqli->real_escape_string($_SESSION['GLOBAL_BIBLIOTHEK_ID'])."' ";
				$result=$mysqli->query($qry);
				if($result) {
					//echo "Ihre Daten wurden erfolgreich in der Datenbank gespeichert!";
				}
				else {
						echo $mysqli->error();
						die("Query failed");
				}
			}
			else {
					echo $mysqli->error();
					die("Query failed");
			}
		}
	}
	
	//Ist eine Funktion_Nutzung vorhanden, wenn ja update sonst insert statement
	if(isset($_SESSION['GLOBAL_FUNKT_NUTZUNG_ID'])){
		$qry="UPDATE bibliothek_funkt_nutzung SET lesefoerderung='".$mysqli->real_escape_string($_SESSION['usage_reading'])."',
												  individuallernen='".$mysqli->real_escape_string($_SESSION['usage_learning'])."',
												  erwerbmedienkomp='".$mysqli->real_escape_string($_SESSION['usage_media'])."',
												  anderes='".$mysqli->real_escape_string($_SESSION['usage_other'])."',
												  unterrichttaeg='".$mysqli->real_escape_string($_SESSION['dailyclass'])."',
												  unterrichtwoech='".$mysqli->real_escape_string($_SESSION['weeklyclass'])."',
												  unterrichtmonat='".$mysqli->real_escape_string($_SESSION['monthlyclass'])."',
												  unterrichtsporad='".$mysqli->real_escape_string($_SESSION['sporadicclass'])."',
												  unterrichtnicht='".$mysqli->real_escape_string($_SESSION['noclass'])."',
												  nutzungveranstaltungen='".$mysqli->real_escape_string($_SESSION['outsideclass'])."' WHERE id='".$mysqli->real_escape_string($_SESSION['GLOBAL_FUNKT_NUTZUNG_ID'])."'";
		$result=$mysqli->query($qry);
		if($result) {
			//echo "Ihre Daten wurden erfolgreich in der Datenbank gespeichert!";
		}
		else {
				echo $mysqli->error();
				die("Query failed");
		}
	}
	else{
		$qry="INSERT INTO bibliothek_funkt_nutzung SET lesefoerderung='".$mysqli->real_escape_string($_SESSION['usage_reading'])."',
													   individuallernen='".$mysqli->real_escape_string($_SESSION['usage_learning'])."', 
													   erwerbmedienkomp='".$mysqli->real_escape_string($_SESSION['usage_media'])."', 
													   anderes='".$mysqli->real_escape_string($_SESSION['usage_other'])."', 
													   unterrichttaeg='".$mysqli->real_escape_string($_SESSION['dailyclass'])."', 
													   unterrichtwoech='".$mysqli->real_escape_string($_SESSION['weeklyclass'])."', 
													   unterrichtmonat='".$mysqli->real_escape_string($_SESSION['monthlyclass'])."', 
													   unterrichtsporad='".$mysqli->real_escape_string($_SESSION['sporadicclass'])."', 
													   unterrichtnicht='".$mysqli->real_escape_string($_SESSION['noclass'])."', 
													   nutzungveranstaltungen='".$mysqli->real_escape_string($_SESSION['outsideclass'])."'";
		$result=$mysqli->query($qry);
		if($result) {
			//echo "Ihre Daten wurden erfolgreich in der Datenbank gespeichert!";
			//Holt sich die ID der gerade eingetragenen Funktion_Nutzung. Wird ben�tigt als fk_funkt_nutzung_id f�r die Biblitohekstabelle
			$funktnutzid = $mysqli->insert_id;
			
			//Schreibt die id als fk_raum_einrichtung_id in die bibliothekstabelle
			$qry="UPDATE bibliothek SET fk_bibliothek_funkt_nutzung_id='".$mysqli->real_escape_string($funktnutzid)."' WHERE id='".$mysqli->real_escape_string($_SESSION['GLOBAL_BIBLIOTHEK_ID'])."' ";
			$result=$mysqli->query($qry);
			if($result) {
				//echo "Ihre Daten wurden erfolgreich in der Datenbank gespeichert!";
			}
			else {
					echo $mysqli->error();
					die("Query failed");
			}
		}
		else {
				echo $mysqli->error();
				die("Query failed");
		}
	}
	
	//Tr�gt die ganzen Daten von Seite 5 und 6 des Formulars in die Bibliothekstabelle ein
	$qry="UPDATE bibliothek SET oeffnungsstunden='".$mysqli->real_escape_string($_SESSION['oeffnungstunden'])."',
								webopacurl='".$mysqli->real_escape_string($_SESSION['webopac'])."', 
								homepageurl='".$mysqli->real_escape_string($_SESSION['homepage'])."', 
								email='".$mysqli->real_escape_string($_SESSION['mail'])."', 
								telefon='".$mysqli->real_escape_string($_SESSION['phone'])."', 
								budget='".$mysqli->real_escape_string($_SESSION['ankaufsbudget'])."', 
								anregung_gut='".$mysqli->real_escape_string($_SESSION['comments_pos'])."', 
								anregung_schlecht='".$mysqli->real_escape_string($_SESSION['comments_neg'])."', 
								anz_schulbibliothekar='".$mysqli->real_escape_string($_SESSION['bibliothekaranzahl'])."', 
								abgeltung_wochenstunden='".$mysqli->real_escape_string($_SESSION['abgwochenstunden'])."', 
								mitarbeit_schueler='".$mysqli->real_escape_string($_SESSION['mitarbeitschueler'])."', 
								mitarbeit_eltern='".$mysqli->real_escape_string($_SESSION['mitarbeiteltern'])."' WHERE id='".$mysqli->real_escape_string($_SESSION['GLOBAL_BIBLIOTHEK_ID'])."'";
	$result=$mysqli->query($qry);
	if($result) {
		//echo "Ihre Daten wurden erfolgreich in der Datenbank gespeichert!";
	}
	else {
			echo $mysqli->error();
			die("Query failed");
	}
	
	//Kontaktperson updaten oder frisch anlegen
	
	//Gibts ne zweite kontaktperson dann beide updaten sonst neue anlegen
	if($_SESSION['contact_person_anz']==2){
		$qry="UPDATE bibliothek_kontaktperson SET name='".$mysqli->real_escape_string($_SESSION['contact_name'])."',
												  email='".$mysqli->real_escape_string($_SESSION['contact_mail'])."', 
												  tel='".$mysqli->real_escape_string($_SESSION['contact_phone'])."', 
												  fax='".$mysqli->real_escape_string($_SESSION['contact_fax'])."' WHERE fk_bibliothek_id='".$mysqli->real_escape_string($_SESSION['GLOBAL_BIBLIOTHEK_ID'])."' AND stellvertreter='0' ";
		$result=$mysqli->query($qry);
		if($result) {
			//echo "Ihre Daten wurden erfolgreich in der Datenbank gespeichert!";
		}
		else {
				echo $mysqli->error();
				die("Query failed");
		}
		//L�scht der Benutzer im Formular die 2.Kontaktperson wieder raus, dann l�schen des Eintrags aus der Tabelle
		if(($_SESSION['contact_name2']=="")&&($_SESSION['contact_mail2']=="")&&($_SESSION['contact_phone2']=="")&&($_SESSION['contact_fax2']=="")){
			$qry="DELETE FROM bibliothek_kontaktperson WHERE fk_bibliothek_id='".$mysqli->real_escape_string($_SESSION['GLOBAL_BIBLIOTHEK_ID'])."' AND stellvertreter='1' ";
			$result=$mysqli->query($qry);
			if($result) {
				//echo "Ihre Daten wurden erfolgreich aus der Datenbank entfernt!";
			}
			else {
					echo $mysqli->error();
					die("Query failed");
			}
		}
		//Ansonsten wurde die 2.Kontaktperson nur ver�ndert und deshalb wird sie upgedatet.
		else{
			$qry="UPDATE bibliothek_kontaktperson SET name='".$mysqli->real_escape_string($_SESSION['contact_name2'])."',
													  email='".$mysqli->real_escape_string($_SESSION['contact_mail2'])."',
													  tel='".$mysqli->real_escape_string($_SESSION['contact_phone2'])."',
													  fax='".$mysqli->real_escape_string($_SESSION['contact_fax2'])."' WHERE fk_bibliothek_id='".$mysqli->real_escape_string($_SESSION['GLOBAL_BIBLIOTHEK_ID'])."' AND stellvertreter='1' ";
			$result=$mysqli->query($qry);
			if($result) {
				//echo "Ihre Daten wurden erfolgreich in der Datenbank gespeichert!";
			}
			else {
					echo $mysqli->error();
					die("Query failed");
			}
		}
	}
	//Gibt es nur eine Kontakperson, dann nur die eine updaten und die andere anlegen
	if($_SESSION['contact_person_anz']==1){
		$qry="UPDATE bibliothek_kontaktperson SET name='".$mysqli->real_escape_string($_SESSION['contact_name'])."', 
												  email='".$mysqli->real_escape_string($_SESSION['contact_mail'])."', 
												  tel='".$mysqli->real_escape_string($_SESSION['contact_phone'])."', 
												  fax='".$mysqli->real_escape_string($_SESSION['contact_fax'])."' WHERE fk_bibliothek_id='".$mysqli->real_escape_string($_SESSION['GLOBAL_BIBLIOTHEK_ID'])."' AND stellvertreter='0' ";
		$result=$mysqli->query($qry);
		if($result) {
			//echo "Ihre Daten wurden erfolgreich in der Datenbank gespeichert!";
		}
		else {
				echo $mysqli->error();
				die("Query failed");
		}
		//Wurde ins Formular eine zweite Kontaktperson eingetragen wird in der Datenbank ein neuer Eintrag f�r diese erstellt
		if((!empty($_SESSION['contact_name2']))||(!empty($_SESSION['contact_email2']))||(!empty($_SESSION['contact_phone2']))||(!empty($_SESSION['contact_fax2']))){
			$qry="INSERT INTO bibliothek_kontaktperson SET name='".$mysqli->real_escape_string($_SESSION['contact_name2'])."',
														   email='".$mysqli->real_escape_string($_SESSION['contact_mail2'])."', 
														   stellvertreter='1', tel='".$mysqli->real_escape_string($_SESSION['contact_phone2'])."', 
														   fax='".$mysqli->real_escape_string($_SESSION['contact_fax2'])."', 
														   fk_bibliothek_id='".$mysqli->real_escape_string($_SESSION['GLOBAL_BIBLIOTHEK_ID'])."' ";
			$result=$mysqli->query($qry);
			if($result) {
				//echo "Ihre Daten wurden erfolgreich in der Datenbank gespeichert!";
			}
			else {
					echo $mysqli->error();
					die("Query failed");
			}
		}
	}
	//Gibt es keine Kontaktperson werden beide frisch angelegt
	if($_SESSION['contact_person_anz']==0){
		$qry="INSERT INTO bibliothek_kontaktperson SET name='".$mysqli->real_escape_string($_SESSION['contact_name'])."',
													   email='".$mysqli->real_escape_string($_SESSION['contact_mail'])."', 
													   stellvertreter='0', tel='".$mysqli->real_escape_string($_SESSION['contact_phone'])."',
													   fax='".$mysqli->real_escape_string($_SESSION['contact_fax'])."',
													   fk_bibliothek_id='".$mysqli->real_escape_string($_SESSION['GLOBAL_BIBLIOTHEK_ID'])."' ";
		$result=$mysqli->query($qry);
		if($result) {
			//echo "Ihre Daten wurden erfolgreich in der Datenbank gespeichert!";
		}
		else {
				echo $mysqli->error();
				die("Query failed");
		}
		//Fragt ob Eintr�ge f�r eine zweite Kontaktperson im Formular eingeben wurden. W�rde das nicht abgefragt werden dann k�nnte in der Tabelle eine Zeile mit lauter NULL Werten eingetragen werden.
		if((!empty($_SESSION['contact_name2']))||(!empty($_SESSION['contact_email2']))||(!empty($_SESSION['contact_phone2']))||(!empty($_SESSION['contact_fax2']))){
			$qry="INSERT INTO bibliothek_kontaktperson SET name='".$mysqli->real_escape_string($_SESSION['contact_name2'])."',
														   email='".$mysqli->real_escape_string($_SESSION['contact_mail2'])."', 
														   stellvertreter='1', tel='".$mysqli->real_escape_string($_SESSION['contact_phone2'])."', 
														   fax='".$mysqli->real_escape_string($_SESSION['contact_fax2'])."', 
														   fk_bibliothek_id='".$mysqli->real_escape_string($_SESSION['GLOBAL_BIBLIOTHEK_ID'])."' ";
			$result=$mysqli->query($qry);
			if($result) {
				//echo "Ihre Daten wurden erfolgreich in der Datenbank gespeichert!";
			}
			else {
					echo $mysqli->error();
					die("Query failed");
			}
		}
	}
}
else{
	//Nur anlegen wenn es eine Bibliothek gibt
	if((isset($_SESSION['librarykind_0']))||(isset($_SESSION['librarykind_1']))||(isset($_SESSION['librarykind_2']))) {
		//wird diese neu angelegt
		$qry="INSERT INTO bibliothek SET oeffnungsstunden='".$mysqli->real_escape_string($_SESSION['oeffnungstunden'])."',
										 webopacurl='".$mysqli->real_escape_string($_SESSION['webopac'])."', 
										 homepageurl='".$mysqli->real_escape_string($_SESSION['homepage'])."', 
										 email='".$mysqli->real_escape_string($_SESSION['mail'])."', 
										 telefon='".$mysqli->real_escape_string($_SESSION['phone'])."', 
										 budget='".$mysqli->real_escape_string($_SESSION['ankaufsbudget'])."', 
										 anregung_gut='".$mysqli->real_escape_string($_SESSION['comments_pos'])."', 
										 anregung_schlecht='".$mysqli->real_escape_string($_SESSION['comments_neg'])."', 
										 anz_schulbibliothekar='".$mysqli->real_escape_string($_SESSION['bibliothekaranzahl'])."', 
										 abgeltung_wochenstunden='".$mysqli->real_escape_string($_SESSION['abgwochenstunden'])."', 
										 mitarbeit_schueler='".$mysqli->real_escape_string($_SESSION['mitarbeitschueler'])."',
										 mitarbeit_eltern='".$mysqli->real_escape_string($_SESSION['mitarbeiteltern'])."'";
		$result=$mysqli->query($qry);
		if($result) {
			//echo "Ihre Daten wurden erfolgreich in der Datenbank gespeichert!";
			$_SESSION['GLOBAL_BIBLIOTHEK_ID'] = $mysqli->insert_id;
		}
		else {
			echo $mysqli->error();
			die("Query failed");
		}
		//Speichert die ID der angelegten Bibliothek in der Schuladressentabelle
		$qry="UPDATE schuladressen SET fk_bibliothek_id='".$mysqli->real_escape_string($_SESSION['GLOBAL_BIBLIOTHEK_ID'])."' WHERE id='".$mysqli->real_escape_string($_SESSION['USERID'])."'";
		$result=$mysqli->query($qry);
		if($result) {
			//echo "Ihre Daten wurden erfolgreich in der Datenbank gespeichert!";
		}
		else {
			echo $mysqli->error();
			die("Query failed");
		}
		//Ab hier ist der gleiche Code wie oben
			//Ist ein Raum vorhanden, wenn ja update sonst insert statement
		
			$qry="INSERT INTO raum_einrichtung SET raumgroesse='".$mysqli->real_escape_string($_SESSION['roomsize'])."',
												   lage_zentral='".$mysqli->real_escape_string($_SESSION['zentral'])."', 
												   lage_nahe_edv='".$mysqli->real_escape_string($_SESSION['edvsaele'])."', 
												   lage_schwer_zu_erreichen='".$mysqli->real_escape_string($_SESSION['schwerzuerr'])."',
												   anzahl_arbeitsp='".$mysqli->real_escape_string($_SESSION['arbeitsplatzanzahl'])."',
												   anzahl_arbeitsp_i='".$mysqli->real_escape_string($_SESSION['internetarbeits'])."',
												   tvgeraet='".$mysqli->real_escape_string($_SESSION['tvgeraet'])."',
												   beamer='".$mysqli->real_escape_string($_SESSION['beamer'])."',
												   drucker='".$mysqli->real_escape_string($_SESSION['printer'])."',
												   scanner='".$mysqli->real_escape_string($_SESSION['scanner'])."',
												   hoerbuchstation='".$mysqli->real_escape_string($_SESSION['hoerbuchstation'])."',
												   kopiergeraet='".$mysqli->real_escape_string($_SESSION['kopierger'])."'";
			$result=$mysqli->query($qry);
			if($result) {
				//echo "Ihre Daten wurden erfolgreich in der Datenbank gespeichert!";
				//Holt sich die ID des gerade Eingetragenen Raums. Wird ben�tigt als fk_raum_einrichtung_id f�r die Biblitohekstabelle
				$raumeinrichtungid = $mysqli->insert_id;
				
				//Schreibt die id als fk_raum_einrichtung_id in die bibliothekstabelle
				$qry="UPDATE bibliothek SET fk_raum_einrichtung_id='".$mysqli->real_escape_string($raumeinrichtungid)."' WHERE id='".$mysqli->real_escape_string($_SESSION['GLOBAL_BIBLIOTHEK_ID'])."' ";
				$result=$mysqli->query($qry);
				if($result) {
					//echo "Ihre Daten wurden erfolgreich in der Datenbank gespeichert!";
				}
				else {
						echo $mysqli->error();
						die("Query failed");
				}
			}
			else {
				die("Query failed");
			}
		
		
		//Ist ein Medienbestand vorhanden, wenn ja update sonst insert statement
			$qry="INSERT INTO bibliothek_medienbest SET printmedien_belletristik='".$mysqli->real_escape_string($_SESSION['belletristik'])."',
														printmedien_zeitschriften='".$mysqli->real_escape_string($_SESSION['zeitschriften'])."',
														printmedien_zeitungen='".$mysqli->real_escape_string($_SESSION['tageszeitungen'])."',
														digitale_medien='".$mysqli->real_escape_string($_SESSION['cddvd'])."',
														downloadangebote='".$mysqli->real_escape_string($_SESSION['downloadang'])."',
														av_medien='".$mysqli->real_escape_string($_SESSION['videos'])."',
														spiele='".$mysqli->real_escape_string($_SESSION['spiele'])."',
														anz_titel_klassenst='".$mysqli->real_escape_string($_SESSION['mehrfachtitel'])."',
														anzahl_titel='".$mysqli->real_escape_string($_SESSION['exemplareprotitel'])."'";
			$result=$mysqli->query($qry);
			if($result) {
				//echo "Ihre Daten wurden erfolgreich in der Datenbank gespeichert!";
				//Holt sich die ID des gerade eingetragenen Medienbestands. Wird ben�tigt als fk_bibliothek_medienbest_id f�r die Biblitohekstabelle
				$medienbestandid= $mysqli->insert_id;
				
				//Schreibt die id als fk_bibliothek_medienbestand_id in die bibliothekstabelle
				$qry="UPDATE bibliothek SET fk_bibliothek_medienbest_id='".$mysqli->real_escape_string($medienbestandid)."' WHERE id='".$mysqli->real_escape_string($_SESSION['GLOBAL_BIBLIOTHEK_ID'])."' ";
				$result=$mysqli->query($qry);
				if($result) {
					//echo "Ihre Daten wurden erfolgreich in der Datenbank gespeichert!";
				}
				else {
						echo $mysqli->error();
						die("Query failed");
				}
			}
			else {
				echo $mysqli->error();
				die("Query failed");
			}
		
		
		//Ist ein Bibliotheksverwaltungsprogramm vorhanden, wenn ja update sonst insert statement
			$name;
			$version;
			if(isset($_SESSION['islittera'])){
				$name="LITTERA";
				$version=$_SESSION['litteraver'];
			}
			if(isset($_SESSION['isexlibris'])){
				$name="EXLIBRIS";
				$version=$_SESSION['exlibrisver'];
			}
			if(isset($_SESSION['isbond'])){
				$name="BOND";
				$version=$_SESSION['bondver'];
			}
			if(isset($_SESSION['isbiblioweb'])){
				$name="BIBLIOWEB";
				$version=$_SESSION['bibliowebver'];
			}
			if(isset($_SESSION['isanderes'])){
				$name=$_SESSION['anderesname'];
				$version=$_SESSION['anderesver'];
			}
			//Wenn der Benutzer �berhaupt eine Software eingetragen hat
			if(($name!="")){
				$qry="INSERT INTO bibliothek_verw_prog SET name='".$mysqli->real_escape_string($name)."', version='".$mysqli->real_escape_string($version)."' ";
				$result=$mysqli->query($qry);
				if($result) {
					//echo "Ihre Daten wurden erfolgreich in der Datenbank gespeichert!";
					//Holt sich die ID des gerade eingetragenen Bibliothekssoftware. Wird ben�tigt als fk_bibliothek_verw_prog_id f�r die Biblitohekstabelle
					$verwprogid = $mysqli->insert_id;
					
					//Schreibt die id als fk_bibliothek_verw_prog_id  in die bibliothekstabelle
					$qry="UPDATE bibliothek SET fk_bibliothek_verw_prog_id='".$mysqli->real_escape_string($verwprogid)."' WHERE id='".$mysqli->real_escape_string($_SESSION['GLOBAL_BIBLIOTHEK_ID'])."' ";
					$result=$mysqli->query($qry);
					if($result) {
						//echo "Ihre Daten wurden erfolgreich in der Datenbank gespeichert!";
					}
					else {
							echo $mysqli->error();
							die("Query failed");
					}
				}
				else {
						echo $mysqli->error();
						die("Query failed");
				}
			}
		
		
		//Ist eine Funktion_Nutzung vorhanden, wenn ja update sonst insert statement
			$qry="INSERT INTO bibliothek_funkt_nutzung SET lesefoerderung='".$mysqli->real_escape_string($_SESSION['usage_reading'])."',
														   individuallernen='".$mysqli->real_escape_string($_SESSION['usage_learning'])."', 
														   erwerbmedienkomp='".$mysqli->real_escape_string($_SESSION['usage_media'])."', 
														   anderes='".$mysqli->real_escape_string($_SESSION['usage_other'])."', 
														   unterrichttaeg='".$mysqli->real_escape_string($_SESSION['dailyclass'])."', 
														   unterrichtwoech='".$mysqli->real_escape_string($_SESSION['weeklyclass'])."', 
														   unterrichtmonat='".$mysqli->real_escape_string($_SESSION['monthlyclass'])."', 
														   unterrichtsporad='".$mysqli->real_escape_string($_SESSION['sporadicclass'])."', 
														   unterrichtnicht='".$mysqli->real_escape_string($_SESSION['noclass'])."', 
														   nutzungveranstaltungen='".$mysqli->real_escape_string($_SESSION['outsideclass'])."'";
			$result=$mysqli->query($qry);
			if($result) {
				//echo "Ihre Daten wurden erfolgreich in der Datenbank gespeichert!";
				//Holt sich die ID der gerade eingetragenen Funktion_Nutzung. Wird ben�tigt als fk_funkt_nutzung_id f�r die Biblitohekstabelle
				$funktnutzid = $mysqli->insert_id;
				
				//Schreibt die id als fk_raum_einrichtung_id in die bibliothekstabelle
				$qry="UPDATE bibliothek SET fk_bibliothek_funkt_nutzung_id='".$mysqli->real_escape_string($funktnutzid)."' WHERE id='".$mysqli->real_escape_string($_SESSION['GLOBAL_BIBLIOTHEK_ID'])."' ";
				$result=$mysqli->query($qry);
				if($result) {
					//echo "Ihre Daten wurden erfolgreich in der Datenbank gespeichert!";
				}
				else {
						echo $mysqli->error();
						die("Query failed");
				}
			}
			else {
					echo $mysqli->error();
					die("Query failed");
			}
		
		
		//Kontaktperson updaten oder frisch anlegen
		
		//Gibt es keine Kontaktperson werden beide frisch angelegt
			$qry="INSERT INTO bibliothek_kontaktperson SET name='".$mysqli->real_escape_string($_SESSION['contact_name'])."',
														   email='".$mysqli->real_escape_string($_SESSION['contact_mail'])."', 
														   stellvertreter='0', tel='".$mysqli->real_escape_string($_SESSION['contact_phone'])."',
														   fax='".$mysqli->real_escape_string($_SESSION['contact_fax'])."',
														   fk_bibliothek_id='".$mysqli->real_escape_string($_SESSION['GLOBAL_BIBLIOTHEK_ID'])."' ";
			$result=$mysqli->query($qry);
			if($result) {
				//echo "Ihre Daten wurden erfolgreich in der Datenbank gespeichert!";
			}
			else {
					echo $mysqli->error();
					die("Query failed");
			}
			//Fragt ob Eintr�ge f�r eine zweite Kontaktperson im Formular eingeben wurden. W�rde das nicht abgefragt werden dann k�nnte in der Tabelle eine Zeile mit lauter NULL Werten eingetragen werden.
			if((!empty($_SESSION['contact_name2']))||(!empty($_SESSION['contact_email2']))||(!empty($_SESSION['contact_phone2']))||(!empty($_SESSION['contact_fax2']))){
				$qry="INSERT INTO bibliothek_kontaktperson SET name='".$mysqli->real_escape_string($_SESSION['contact_name2'])."',
															   email='".$mysqli->real_escape_string($_SESSION['contact_mail2'])."', 
															   stellvertreter='1', tel='".$mysqli->real_escape_string($_SESSION['contact_phone2'])."', 
															   fax='".$mysqli->real_escape_string($_SESSION['contact_fax2'])."', 
															   fk_bibliothek_id='".$mysqli->real_escape_string($_SESSION['GLOBAL_BIBLIOTHEK_ID'])."' ";
				$result=$mysqli->query($qry);
				if($result) {
					//echo "Ihre Daten wurden erfolgreich in der Datenbank gespeichert!";
				}
				else {
						echo $mysqli->error();
						die("Query failed");
				}
			}
		
	}
}
sendRegistrationMail();
header("location: writedb_final.php");
?>