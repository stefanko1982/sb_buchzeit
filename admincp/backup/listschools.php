<?php
/**
* Buchzeit APS-Verwaltung
*
* @author Manuel Postlbauer
* @version 0.9
*/


session_start();
unset($_SESSION['listschools_ids']);
unset($_SESSION['listschools_index']);
unset($_SESSION['searchquery_no_limit']);
$displayloginregister=0;

if(isset($_SESSION['LOGGEDIN'])){
	$displayloginregister=1;
}

if((!isset($_SESSION['LOGGEDIN']))||($_SESSION['ADMIN']!=1)){
	header("location: index.php");
	exit();
}

$list=($_GET['list']);
if(!(is_numeric($list))){
	$list=0;
}
$_SESSION['listback']=$list;

include("../localconf.php");
include("header.php");

$colorcounter=0;
function coloredRow(){
	global $colorcounter;
	if($colorcounter%2==0){
		$colorcounter++;
		return("<tr class='listschooltablealtbg'>");
	}
	else{
		$colorcounter++;
		return("<tr class='listschooltablealtbg2'>");
	}
}
function getEntries(){
	global $bundeslaender;
	$qry="SELECT COUNT(id) AS anzahl FROM schuladressen WHERE schulkennzahl LIKE '".$bundeslaender[$_SESSION['USERNAME']]."%'";
	$result=$mysqli->query($qry);
	if($result) {
		if($row = mysql_fetch_assoc($result)){
			return($row['anzahl']);
		}
		else{
			echo $mysqli->error;
			die("Query failed");
		}
	}
	else {
		echo $mysqli->error;
		die("Query failed");
	}
}
function listNext($list){
	if($list+30 >= getEntries()){
		return (getEntries()-30);
	}
	else{
		return($list+30);
	}
}
function listPrev($list){
	if($list-30 <=0){
		return("0");
	}
	else{
		return($list-30);
	}
}
//Für Datensätze x bis x
function showNext($list){
	if($list+30 >= getEntries()){
		return(getEntries());
	}
	else{
		return($list+30);
}	}

$_SESSION['searchquery_no_limit']="SELECT * FROM schuladressen WHERE schulkennzahl LIKE '".$bundeslaender[$_SESSION['USERNAME']]."%'ORDER BY id";
function getSchuladressen(){
	$schuladressentable;
	$counter=0;

	$qry="SELECT * FROM schuladressen WHERE schulkennzahl LIKE '".$bundeslaender[$_SESSION['USERNAME']]."%'ORDER BY id LIMIT ".$list.", 30";
	$result=$mysqli->query($qry);
	if($result) {
		while($row = mysql_fetch_assoc($result)){
			$schuladressentable[$counter]['schultitel']=echo $row['schultitel'];
			$schuladressentable[$counter]['postleitzahl']=echo $row['postleitzahl'];
			$schuladressentable[$counter]['ort']=echo $row['ort'];
			$schuladressentable[$counter]['strasse_hausnummer']=echo $row['strasse_hausnummer'];
			$schuladressentable[$counter]['schulart']=echo $row['schulart'];
			$counter++;
		}
		return($schuladressentable);
	}
	else {
		echo $mysqli->error;
		die("Query failed");
	}
}
function getBibliothek($fk_bibliothek_id){
	$bibliothekstable;
	$counter=0;
		
	$qry="SELECT * FROM bibliothek WHERE id='".$fk_bibliothek_id."'";
	$result=$mysqli->query($qry);
	if($result) {
		while($row = mysql_fetch_assoc($result)){
			$bibliothekstable[$counter]['oeffnungsstunden']=echo $row['oeffnungsstunden']=;
			$bibliothekstable[$counter]['webopacurl']=echo $row['webopacurl'];
			$bibliothekstable[$counter]['homepageurl']=echo $row['homepageurl'];
			$bibliothekstable[$counter]['email']=echo $row['email'];
			$bibliothekstable[$counter]['telefon']=echo $row['telefon'];
			$bibliothekstable[$counter]['budget']=echo $row['budget'];
			$bibliothekstable[$counter]['anz_schulbibliothekar']=echo $row['anz_schulbibliothekar'];
			$bibliothekstable[$counter]['abgeltung_wochenstunden']=echo $row['abgeltung_wochenstunden'];
			$bibliothekstable[$counter]['mitarbeit_schueler']=echo $row['mitarbeit_schueler'];
			$bibliothekstable[$counter]['mitarbeit_eltern']=echo $row['mitarbeit_eltern'];
			$counter++;
		}
		return($bibliothekstable);
	}
	else {
		echo $mysqli->error;
		die("Query failed");
	}
}
function getBibliothekId($schuladressen_id){
	
	$qry="SELECT fk_bibliothek_id FROM schuladressen WHERE id='".$schuladressen_id."'";
	$result=$mysqli->query($qry);
	if($result) {
		if($row = mysql_fetch_assoc($result)){
			return($row['fk_bibliothek_id']);
		}
	}
	else {
		echo $mysqli->error;
		die("Query failed");
	}
}
function getSoftwareId($fk_bibliothek_id){
	
	$qry="SELECT fk_bibliothek_verw_prog_id FROM bibliothek WHERE id='".$fk_bibliothek_id."'";
	$result=$mysqli->query($qry);
	if($result) {
		if($row = mysql_fetch_assoc($result)){
			return($row['fk_bibliothek_verw_prog_id']);
		}
	}
	else {
		echo $mysqli->error;
		die("Query failed");
	}
}
function getMedienbestandId($fk_bibliothek_id){
	
	$qry="SELECT fk_bibliothek_medienbest_id FROM bibliothek WHERE id='".$fk_bibliothek_id."'";
	$result=$mysqli->query($qry);
	if($result) {
		if($row = mysql_fetch_assoc($result)){
			return($row['fk_bibliothek_medienbest_id']);
		}
	}
	else {
		echo $mysqli->error;
		die("Query failed");
	}
}
function getRaumEinrichtungsId($fk_bibliothek_id){
	
	$qry="SELECT fk_raum_einrichtung_id FROM bibliothek WHERE id='".$fk_bibliothek_id."'";
	$result=$mysqli->query($qry);
	if($result) {
		if($row = mysql_fetch_assoc($result)){
			return($row['fk_raum_einrichtung_id']);
		}
	}
	else {
		echo $mysqli->error;
		die("Query failed");
	}
}
function getFunktionsNutzungsId($fk_bibliothek_id){
	
	$qry="SELECT fk_bibliothek_funkt_nutzung_id FROM bibliothek WHERE id='".$fk_bibliothek_id."'";
	$result=$mysqli->query($qry);
	if($result) {
		if($row = mysql_fetch_assoc($result)){
			return($row['fk_bibliothek_funkt_nutzung_id']);
		}
	}
	else {
		echo $mysqli->error;
		die("Query failed");
	}
}

?>
<h1>Schulliste</h1>
<p>Datensätze&nbsp;<?php echo $list+1?>&nbsp;bis&nbsp;<?php echo showNext($list);?>&nbsp;von&nbsp;<?php echo getEntries();?></p>
<p><a href="listschools.php?list=<?php echo listPrev($list); ?>">Zurück |&nbsp;</a><a href="listschools.php?list=<?php echo listNext($list); ?>">Weiter</a></p>
<table class="listschooltable" cellpadding="5" cellspacing="1" width="100%">
	<tr>
		<th>Schulkennzahl</th>
		<th>Schulbezeichnung</th>
		<th>Postleitzahl</th>
		<th>Ort</th>
		<th>Straße</th>
		<th>Schulart</th>
		<th>Bibliothek</th>
	</tr>
	<?php
	$counter=0;
	if($result) {
		while($row = mysql_fetch_assoc($result)){
			echo coloredRow();

			$_SESSION['listschools_ids'][$counter]=$row['id'];
			$counter++;	
			?>
			
				<td><a href="listschools_detail.php?id=<?php echo $row['id'];?>"><?php echo $row['schulkennzahl'];?></a></td>
				<td><?php echo $row['schultitel'];?></td>
				<td><?php echo $row['postleitzahl'];?></td>
				<td><?php echo $row['ort'];?></td>
				<td><?php echo $row['strasse_hausnummer'];?></td>
				<td><?php echo $row['schulart'];?></td>
				<td><?php
						if(($row['eigene_bibliothek']==0)&&($row['gemeinsame_bibliothek']==0)&&($row['oeff_gemeinsame_bibliothek']==0)){
							echo("Nein");
						}
						else{
							echo("Ja");
						}
					?>
				</td>	
		    </tr>
			<?php
		}
	}
	else {
		echo $mysqli->error;
		die("Query failed");
	}
	?>
</table>
<a class="csv-exportbutton" href="csv_output.php" target="_blank" style="display:block;">&nbsp;</a>
<?php
include("footer.php");
?>