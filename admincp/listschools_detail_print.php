<?php
/**
* Buchzeit APS-Verwaltung
*
* @author Manuel Postlbauer
* @version 0.9
*/

session_start();
$displayloginregister=0;

if(isset($_SESSION['LOGGEDIN'])){
	$displayloginregister=1;
}

if((!isset($_SESSION['LOGGEDIN']))||($_SESSION['ADMIN']!=1)){
	header("location: index.php");
	exit();
}

$id=$_GET['id'];
if(!(is_numeric($id))){
	header("location: index.php");
}

$_SESSION['listschools_index']=array_search($id,$_SESSION['listschools_ids']);

function getEntries(){
	return(count($_SESSION['listschools_ids']));
}
function NextId(){
	if(($_SESSION['listschools_index']+1) == getEntries()){
		return($_SESSION['listschools_index']);
	}
	else{
		return($_SESSION['listschools_index']+1);
	}
}
function PrevId(){
	if(($_SESSION['listschools_index']-1) <0){
		return($_SESSION['listschools_index']);
	}
	else{
		return($_SESSION['listschools_index']-1);
	}
}

include("../localconf.php");

$qry="SELECT * FROM schuladressen WHERE id='".$_SESSION['listschools_ids'][$_SESSION['listschools_index']]."' AND schulkennzahl LIKE '".$bundeslaender[$_SESSION['USERNAME']]."%'";
$result=$mysqli->query($qry);
	
if($result) {
	if(mysqli_num_rows($result) == 1) {
		$resultarray = $result->fetch_assoc();

		$school_id=$resultarray['schulkennzahl'];
		$school_name=$resultarray['schultitel'];
		$address_plz=$resultarray['postleitzahl'];
		$address_school_loc=$resultarray['ort'];
		$address_street=$resultarray['strasse_hausnummer'];
		$schooltype=$schultypen[$resultarray['schulart']];
		$gemeinde=$resultarray['schulerhalter_gemeinde'];
		$schulerhalter=$resultarray['schulerhalter_privat'];		
		$classroomcount=$resultarray['klassenanzahl'];
		
		if($resultarray['eigene_bibliothek']==1)
			$librarykind_0=1;
		if($resultarray['gemeinsame_bibliothek']==1)
			$librarykind_1=1;
		if($resultarray['oeff_gemeinsame_bibliothek']==1)
			$librarykind_2=1;
if(!empty($row['andere_bibliothek'])){
				$librarykind_3=$resultarray['andere_bibliothek'];
			}
		if($resultarray['fk_bibliothek_id']!=0)
			$GLOBAL_BIBLIOTHEK_ID=$resultarray['fk_bibliothek_id'];
	}	
}
else {
	echo $mysqli->error;
	die("Query failed");
}

if(isset($GLOBAL_BIBLIOTHEK_ID)){

	$qry="SELECT * FROM bibliothek WHERE id='".$GLOBAL_BIBLIOTHEK_ID."'";
	$result=$mysqli->query($qry);
		
	if($result) {
		if(mysqli_num_rows($result) == 1) {
			$resultarray = $result->fetch_assoc();

			if($resultarray['fk_bibliothek_verw_prog_id']!=0)
				$GLOBAL_VERW_PROG_ID=$resultarray['fk_bibliothek_verw_prog_id'];
			if($resultarray['fk_bibliothek_medienbest_id']!=0)
				$GLOBAL_MEDIENBESTAND_ID=$resultarray['fk_bibliothek_medienbest_id'];
			if($resultarray['fk_raum_einrichtung_id']!=0)
				$GLOBAL_RAUM_EINRICHTUNG_ID=$resultarray['fk_raum_einrichtung_id'];
			if($resultarray['fk_bibliothek_funkt_nutzung_id']!=0)
				$GLOBAL_FUNKT_NUTZUNG_ID=$resultarray['fk_bibliothek_funkt_nutzung_id'];
			//Step 5 des Formulars	
			$oeffnungstunden=$resultarray['oeffnungsstunden'];
			$webopac=$resultarray['webopacurl'];
			$homepage=$resultarray['homepageurl'];
			$mail=$resultarray['email'];
			$phone=$resultarray['telefon'];	
			//Step 6 des Formulars
			$comments_pos=$resultarray['anregung_gut'];
			$comments_neg=$resultarray['anregung_schlecht'];	
			//Step 5 des Formulars
			$bibliothekaranzahl=$resultarray['anz_schulbibliothekar'];
			$abgwochenstunden=$resultarray['abgeltung_wochenstunden'];
			//Gleiches Prinzip wie bei den Radiobuttons. Ein 1er in der Sessionvariable setzt die Checkbox
			if($resultarray['mitarbeit_schueler']==1)
				$mitarbeitschueler=1;
			if($resultarray['mitarbeit_eltern']==1)
				$mitarbeiteltern=1;
			//Step 5 des Formulars
			$ankaufsbudget=$resultarray['budget'];
		}	
	}
	else {
		echo $mysqli->error;
		die("Query failed");
	}
	//Ist ein Fremdschlüssel für die Bibliothekssoftware vorhanden, lese die Daten für die Bibliothekssoftware aus und speichere sie ins Formular
	if(isset($GLOBAL_VERW_PROG_ID)){
		//Der Fremdschlüssel der Bibliothekssoftware in der Bibliothekstabelle entspricht den Primary Key eines Eintrages in der Tabelle für die Bibliothekssoftware
		$qry="SELECT * FROM bibliothek_verw_prog WHERE id='".$GLOBAL_VERW_PROG_ID."'";
		$result=$mysqli->query($qry);
			
		if($result) {
			if(mysqli_num_rows($result) == 1) {
				$resultarray = $result->fetch_assoc();
				//Hier wird der Name der Bibliothekssoftware abgefragt es wird der jeweilige Name in der Sessionvariable $_SESSION['name'] gespeichert. Es gibt nur eine Variable
				//für den Namen, da nur eine Software verwendet wird.
				
				//Step 3 des Formulars
				if($resultarray['name']=="LITTERA"){
					$islittera=1;
					$litteraver=$resultarray['version'];
				}
				if($resultarray['name']=="EXLIBRIS"){
					$isexlibris=1;
					$exlibrisver=$resultarray['version'];
				}
				if($resultarray['name']=="BOND"){
					$isbond=1;
					$bondver=$resultarray['version'];
				}
				if($resultarray['name']=="BIBLIOWEB"){
					$isbiblioweb=1;
					$bibliowebver=$resultarray['version'];
				}
				//Wurde kein Name der obigen Software gefunden, dann wird eine eigene Software verwendet. Der Name und die Version dieser wird in eigenen Sessionvariablen gespeichert
				if(($resultarray['name']!="LITTERA")&&($resultarray['name']!="EXLIBRIS")&&($resultarray['name']!="BOND")&&($resultarray['name']!="BIBLIOWEB")){
					$isanderes=1;
					$anderesname=$resultarray['name'];
					$anderesver=$resultarray['version'];	
				}
			}	
		}
		else {
			echo $mysqli->error;
			die("Query failed");
		}
	}
	//Ist der Fremdschlüssel für den Medienbestand in der Bibliothekstabelle vorhanden, dann werden die Daten über den Bestand ausgelesen und ins Formular gespeichert.
	if(isset($GLOBAL_MEDIENBESTAND_ID)){
		$qry="SELECT * FROM bibliothek_medienbest WHERE id='".$GLOBAL_MEDIENBESTAND_ID."'";
		$result=$mysqli->query($qry);

		if($result) {
			if(mysqli_num_rows($result) == 1) {
				$resultarray = $result->fetch_assoc();	
				//Step 4 des Formulars
				$belletristik=$resultarray['printmedien_belletristik'];
				$zeitschriften=$resultarray['printmedien_zeitschriften'];
				$tageszeitungen=$resultarray['printmedien_zeitungen'];
				$cddvd=$resultarray['digitale_medien'];
				$downloadang=$resultarray['downloadangebote'];
				$videos=$resultarray['av_medien'];
				$spiele=$resultarray['spiele'];
				$mehrfachtitel=$resultarray['anz_titel_klassenst'];
				$exemplareprotitel=$resultarray['anzahl_titel'];
			}	
		}
		else {
			echo $mysqli->error;
			die("Query failed");
		}
	}
	//Ist der Fremdschlüssel für die Raum und Einrichtungstabelle in der Bibliothekstabelle vorhanden, dann werden die Daten über den Raum und die Einrichung ausgelesen und ins Formular gespeichert.
	if(isset($GLOBAL_RAUM_EINRICHTUNG_ID)){
		
		$qry="SELECT * FROM raum_einrichtung WHERE id='".$GLOBAL_RAUM_EINRICHTUNG_ID."'";
		$result=$mysqli->query($qry);

		if($result) {
			if(mysqli_num_rows($result) == 1) {
				$resultarray = $result->fetch_assoc();
				//Step 3 des Formulars
				$roomsize=$resultarray['raumgroesse'];
				if($resultarray['lage_zentral']==1)
					$zentral=1;
				if($resultarray['lage_nahe_edv']==1)
					$lage_nahe_edv=1;
				if($resultarray['lage_schwer_zu_erreichen']==1)
					$schwerzuerr=1;
		
				$arbeitsplatzanzahl=$resultarray['anzahl_arbeitsp'];
				$internetarbeits=$resultarray['anzahl_arbeitsp_i'];
				
				if($resultarray['tvgeraet']==1)
					$tvgeraet=1;
				if($resultarray['beamer']==1)
					$beamer=1;
				if($resultarray['drucker']==1)
					$printer=1;
				if($resultarray['scanner']==1)
					$scanner=1;
				if($resultarray['hoerbuchstation']==1)
					$hoerbuchstation=1;
				if($resultarray['kopiergeraet']==1)
					$kopierger=1;
			}	
		}
		else {
			echo $mysqli->error;
			die("Query failed");
		}
	
	}
	//Ist der Fremdschlüssel für die Funktion und Nutzungstabelle in der Bibliothekstabelle vorhanden, dann werden die Daten über die Funktion und Nutzung ausgelesen und ins Formular gespeichert.
	if(isset($GLOBAL_FUNKT_NUTZUNG_ID)){
	
		$qry="SELECT * FROM bibliothek_funkt_nutzung WHERE id='".$GLOBAL_FUNKT_NUTZUNG_ID."'";
		$result=$mysqli->query($qry);

		if($result) {
			if(mysqli_num_rows($result) == 1) {
				$resultarray = $result->fetch_assoc();		
				//Step 6 des Formulars
				if($resultarray['lesefoerderung']==1)
					$usage_reading=1;
				if($resultarray['individuallernen']==1)
					$usage_learning=1;
				if($resultarray['erwerbmedienkomp']==1)
					$usage_media=1;

				$usage_other=$resultarray['anderes'];
				
				if($resultarray['unterrichttaeg']==1)
					$dailyclass=1;
				if($resultarray['unterrichtwoech']==1)
					$weeklyclass=1;
				if($resultarray['unterrichtmonat']==1)	
					$monthlyclass=1;
				if($resultarray['unterrichtsporad']==1)
					$sporadicclass=1;
				if($resultarray['unterrichtnicht']==1)
					$noclass=1;
				if($resultarray['nutzungveranstaltungen']==1)
					$outsideclass=1;
			}	
		}
		else {
			echo $mysqli->error;
			die("Query failed");
		}
	}
	
	//Filtert die Kontaktpersonen aus der Tabelle bei denen der Fremdschlüssel für die Bibliothek mit dem der Bibliothek für den eingeloggten User übereinstimmt. (schuladressentabelle fk_bibliothek_id == bibliothek_kontaktperson fk_bibliothek_id)
	$qry="SELECT * FROM bibliothek_kontaktperson WHERE fk_bibliothek_id='".$GLOBAL_BIBLIOTHEK_ID."'";
	$result=$mysqli->query($qry);

	if($result) {
			while($resultarray = $result->fetch_assoc()){
				//Ist im Feld Stellvertreter 0 eingetragen, dann ist der Kontakt die Hauptkontaktperson
				if($resultarray['stellvertreter']==0){
					$contact_name=$resultarray['name'];
					$contact_mail=$resultarray['email'];
					$contact_phone=$resultarray['tel'];
					$contact_fax=$resultarray['fax'];
					$contact_person_anz+=1;
				}
				//sonst ist es der Stellvertreter
				else{
					$contact_name2=$resultarray['name'];
					$contact_mail2=$resultarray['email'];
					$contact_phone2=$resultarray['tel'];
					$contact_fax2=$resultarray['fax'];
					$contact_person_anz+=1;
				}
			}
	}
	else {
		echo $mysqli->error;
		die("Query failed");
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" type="text/css" href="../style.css" />
<link rel="stylesheet" type="text/css" href="admin_style.css" />
<title>SB - Verwaltung (APS) - Adminbereich - Druckansicht</title>
</head>
<body>
<div style="width:640px;">
	<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="100%">
		<tr>
			<th colspan="2">Angaben zur Schule</th>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Schulkennzahl:</td>
			<td><?php echo $school_id; ?></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Schulbezeichnung:&nbsp;</td>
			<td><?php echo $school_name; ?></td>
		</tr>
	</table>
	
	<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="100%">
		<tr>
			<th colspan="2">Adresse</th>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Postleitzahl:&nbsp;</td>
			<td><?php echo $address_plz; ?></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Schulort:&nbsp;</td>
			<td><?php echo $address_school_loc; ?></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Straße:&nbsp;</td>
			<td><?php echo $address_street; ?></td>
		</tr>
	</table>
	
	<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="100%">
		<tr>
			<th colspan="2">Schulart</th>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Volksschule:</td>
			<td><?php if($schooltype=="volksschule") echo "Ja"; else echo "Nein"; ?></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Hauptschule:</td>
			<td><?php if($schooltype=="hauptschule") echo "Ja"; else echo "Nein";?></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Sonderschule:</td>
			<td><?php if($schooltype=="sonderschule") echo "Ja"; else echo "Nein";?></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Polytechnische Schule:</td>
			<td><?php if($schooltype=="polytechnischeschule") echo "Ja"; else echo "Nein";?></td>
		</tr>
	</table>
	
	<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="100%">
		<tr>
			<th colspan="2">Schulerhalter</th>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Gemeinde/Magistrat:</td>
			<td><?php echo $gemeinde; ?></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Privater Schulerhalter:<br />(nämlich)</td>
			<td><?php echo $schulerhalter; ?></td>
		</tr>
	</table>
	
	<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="100%">
		<tr>
			<th colspan="2">Kategorisierung</th>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Anzahl der Klassen: </td>
			<td><?php echo $classroomcount;?></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Eigene formell eingerichtete (genehmigte) Schulbibliothek:</td>
			<td><?php if(!empty($librarykind_0)) echo "Ja"; else echo "Nein"; ?></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Mit einer anderen Schule gemeinsam geführte Schulbibliothek:</td>
			<td><?php if(!empty($librarykind_1)) echo "Ja"; else echo "Nein"; ?></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Mit einer öffentlichen Bibliothek gemeinsam geführte Schulbibliothek:</td>
			<td><?php if(!empty($librarykind_2)) echo "Ja"; else echo "Nein"; ?></td>
		</tr>
             <tr class="listschooltablealtbg">
			<td>Andere Varianten:</td>
			<td><?php if(!empty($librarykind_3)) echo $librarykind_3; else echo $librarykind_3; ?></td>
		</tr>
	</table>
	
	<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="100%">
		<tr>
			<th colspan="2">Raum, Einrichtung, EDV</th>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Raumgröße (in m²):&nbsp;</td>
			<td><?php echo $roomsize; ?></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Lage: zentral</td>
			<td><?php if(!empty($zentral)) echo "Ja"; else echo "Nein"; ?></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Lage: in der Nähe von EDV-Sälen</td>
			<td><?php if(!empty($edvsaele)) echo "Ja"; else echo "Nein"; ?></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Lage: eher schwer zu erreichen</td>
			<td><?php if(!empty($schwerzuerr)) echo "Ja"; else echo "Nein"; ?></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Anzahl der Schüler-Arbeitsplätze insgesamt:</td>
			<td><?php if(isset($arbeitsplatzanzahl)) echo $arbeitsplatzanzahl; else echo"0"; ?></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Anzahl der Schüler-Internet-Arbeitsplätze:</td>
			<td><?php if(isset($internetarbeits)) echo $internetarbeits; else echo"0"; ?></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Fernseher:</td>
			<td><?php if(!empty($tvgeraet)) echo "Ja"; else echo "Nein"; ?></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Beamer:</td>
			<td><?php if(!empty($beamer)) echo "Ja"; else echo "Nein"; ?></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Drucker:</td>
			<td><?php if(!empty($printer)) echo "Ja"; else echo "Nein"; ?></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Handscanner:</td>
			<td><?php if(!empty($scanner)) echo "Ja"; else echo "Nein"; ?></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Hörbuchstation:</td>
			<td><?php if(!empty($hoerbuchstation)) echo "Ja"; else echo "Nein"; ?></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Eigenes Kopiergerät:</td>
			<td><?php if(!empty($kopierger)) echo "Ja"; else echo "Nein"; ?></td>
		</tr>
	</table>
	
	<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="100%">
		<tr>
			<th colspan="2">Bibliotheksverwaltungsprogramm</th>
		</tr>
		<?php
		if(isset($islittera)){
		?>
		<tr class="listschooltablealtbg">
			<td>Name:</td>
			<td>LITTERA</td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Version:</td>
			<td><?php if(isset($litteraver)) echo $litteraver; ?></td>
		</tr>									
		<?php
		}
		if(isset($isexlibris)){
		?>
		<tr class="listschooltablealtbg">
			<td>Name:</td>
			<td>EXLIBRIS</td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Version:</td>
			<td><?php if(isset($exlibrisver)) echo $exlibrisver; ?></td>
		</tr>									
		<?php
		}
		if(isset($isbond)){
		?>
		<tr class="listschooltablealtbg">
			<td>Name:</td>
			<td>BOND</td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Version:</td>
			<td><?php if(isset($bondver)) echo $bondver; ?></td>
		</tr>									
		<?php
		}
		if(isset($isbiblioweb)){
		?>
		<tr class="listschooltablealtbg">
			<td>Name:</td>
			<td>BIBLIOWEB</td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Version:</td>
			<td><?php if(isset($bibliowebver)) echo $bibliowebver; ?></td>
		</tr>									
		<?php
		}
		if(isset($isanderes)){
		?>
		<tr class="listschooltablealtbg">
			<td>Name:</td>
			<td><?php if(isset($anderesname)) echo $anderesname; ?></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Version:</td>
			<td><?php if(isset($anderesver)) echo $anderesver; ?></td>
		</tr>									
		<?php
		}
		?>
	</table>
	
	<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="100%">
		<tr>
			<th colspan="2">Medienbestand - Anzahl</th>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Printmedien - Belletristik, Sach- und Fachbuchbestand, Comics:</td>
			<td><?php if(isset($belletristik)) echo $belletristik; else echo"0"; ?></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Printmedien - Zeitschriften (Abo):</td>
			<td><?php if(isset($zeitschriften)) echo $zeitschriften; else echo"0"; ?></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Printmedien - Tageszeitungen (Abo):</td>
			<td><?php if(isset($tageszeitungen)) echo $tageszeitungen; else echo"0"; ?></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Digitale  Medien (CDs, DVDs):</td>
			<td><?php if(isset($cddvd)) echo $cddvd; else echo"0"; ?></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Downloadangebote:</td>
			<td><?php if(isset($downloadang)) echo $downloadang; else echo"0"; ?></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Herkömmliche AV-Medien (Kassetten, Videos):</td>
			<td><?php if(isset($videos)) echo $videos; else echo"0"; ?></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Spiele:</td>
			<td><?php if(isset($spiele)) echo $spiele; else echo"0"; ?></td>
		</tr>
	</table>
	
	<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="100%">
		<tr>
			<th colspan="2">Mehrfachexemplare</th>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Anzahl der Titel in Klassenstärke (mindestens 25 Exemplare):</td>
			<td><?php if(isset($mehrfachtitel)) echo $mehrfachtitel; else echo"0"; ?></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>5 bis 10 Exemplare pro Titel - Anzahl der Titel:</td>
			<td><?php if(isset($exemplareprotitel)) echo $exemplareprotitel; else echo"0"; ?></td>
		</tr>
	</table>	
</div>
<div style="width:640px;">
	<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="100%">
		<tr>
			<th colspan="2">Öffnungszeiten und Erreichbarkeit</th>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Öffnungszeiten in Anzahl der Stunden pro Woche:</td>
			<td><?php if(isset($oeffnungstunden)) echo $oeffnungstunden; else echo"0"; ?></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>WebOPAC? - URL:</td>
			<td><?php if(isset($webopac)) echo $webopac;?></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Homepage? - URL:</td>
			<td><?php if(isset($homepage)) echo $homepage;?></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>E-Mail-Adresse:&nbsp;</td>
			<td><?php if(isset($mail)) echo $mail;?></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Telefon:&nbsp;</td>
			<td><?php if(isset($phone)) echo $phone;?></td>
		</tr>
	</table>
	
	<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="100%">
		<tr>
			<th colspan="2">Betreuung</th>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Ausgebildete Schulbibliothekar/innen (Anzahl):</td>
			<td><?php if(isset($bibliothekaranzahl)) echo $bibliothekaranzahl; else echo"0"; ?></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Abgeltung: Unterschreitung - Anzahl der Wochenstunden:</td>
			<td><?php if(isset($abgwochenstunden)) echo $abgwochenstunden; else echo"0"; ?></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Mitarbeit von Schüler/innen:</td>
			<td><?php if(!empty($mitarbeitschueler)) echo "Ja"; else echo "Nein"; ?></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Mittarbeit von Eltern:</td>
			<td><?php if(!empty($mitarbeiteltern)) echo "Ja"; else echo "Nein"; ?></td>
		</tr>
	</table>
	
	<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="100%">
		<tr>
			<th colspan="2">Budget</th>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Jährliches Ankaufsbudget für Medien / durchschnittlich:&nbsp;</td>
			<td><?php if(isset($ankaufsbudget)) echo $ankaufsbudget; else echo"0"; ?></td>
		</tr>
	</table>
	
	<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="100%">
		<tr>
			<th colspan="2">Funktion und Nutzung</th>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Funktion der Schulbibliothek bei der Leseförderung:</td>
			<td><?php if(!empty($usage_reading)) echo "Ja"; else echo "Nein"; ?></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Funktion der Schulbibliothek bei individualisiertem Lernen:</td>
			<td><?php if(!empty($usage_learning)) echo "Ja"; else echo "Nein"; ?></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Funktion der Schulbibliothek beim Erwerb von Medienkompetenz:</td>
			<td><?php if(!empty($usage_media)) echo "Ja"; else echo "Nein"; ?></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td colspan="2">Andere Funktionen, nämlich:</td>
		</tr>
		<tr class="listschooltablealtbg">
			<td colspan="2"><?php if(isset($usage_other)) echo $usage_other; else echo"-"?></td>
		</tr>
	</table>
	
	<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="100%">	
		<tr>
			<th colspan="2">Unterricht in der Schulbibliothek (alle Klassen/alle Lehrkräfte)</th>
		</tr>
		<tr class="listschooltablealtbg">
			<td>täglich</td>
			<td><?php if(!empty($dailyclass)) echo "Ja"; else echo "Nein"; ?></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>mehrmals pro Woche</td>
			<td><?php if(!empty($weeklyclass)) echo "Ja"; else echo "Nein"; ?></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>mehrmals im Monat</td>
			<td><?php if(!empty($monthlyclass)) echo "Ja"; else echo "Nein"; ?></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>sporadisch</td>
			<td><?php if(!empty($sporadicclass)) echo "Ja"; else echo "Nein"; ?></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>gar nicht</td>
			<td><?php if(!empty($noclass)) echo "Ja"; else echo "Nein"; ?></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Nutzung der Schulbibliothek für Veranstaltungen außerhalb der Unterrichtszeit:</td>
			<td><?php if(!empty($outsideclass)) echo "Ja"; else echo "Nein"; ?></td>
		</tr>
	</table>
	
	<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="100%">
		<tr>
			<th colspan="2">Anregungen</th>
		</tr>
		<tr class="listschooltablealtbg">
			<td colspan="2">Was funktioniert gut in unserer Schulbibliothek und sollte daher beibehalten werden?</td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td colspan="2"><?php if(isset($comments_pos)) echo $comments_pos; else echo"-"?></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td colspan="2">Was funktioniert in unserer Schulbibliothek nicht und sollte daher unbedingt geändert werden?</td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td colspan="2"><?php if(isset($comments_neg)) echo $comments_neg; else echo"-"?></td>
		</tr>
	</table>
	
	<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="100%">
		<tr>
			<th colspan="2">Kontaktperson (ausgebildete Schulbibliothekar/in)</th>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Name:&nbsp;</td>
			<td><?php if(isset($contact_name)) echo $contact_name;?></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>E-Mail-Adresse:&nbsp;</td>
			<td><?php if(isset($contact_mail)) echo $contact_mail;?></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Telefon:&nbsp;</td>
			<td><?php if(isset($contact_phone)) echo $contact_phone;?></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>FAX:&nbsp;</td>
			<td><?php if(isset($contact_fax)) echo $contact_fax;?></td>
		</tr>
	</table>
	
	<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="100%">
		<tr>
			<th colspan="2">2.Kontaktperson (ausgebildete Schulbibliothekar/in)</th>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Name:</td>
			<td><?php if(isset($contact_name2)) echo $contact_name2;?></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>E-Mail-Adresse:</td>
			<td><?php if(isset($contact_mail2)) echo $contact_mail2;?></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Telefon:</td>
			<td><?php if(isset($contact_phone2)) echo $contact_phone2;?></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>FAX:</td>
			<td><?php if(isset($contact_fax2)) echo $contact_fax2;?></td>
		</tr>
	</table>
</div>
<?php
include("footer.php");
?>