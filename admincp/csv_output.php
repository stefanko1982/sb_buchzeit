<?php
/**
* Buchzeit APS-Verwaltung
*
* @author Manuel Postlbauer
* @version 0.9
*/
session_start();

if((!isset($_SESSION['LOGGEDIN']))||($_SESSION['ADMIN']!=1)||(!isset($_SESSION['searchquery_no_limit']))){
	header("location: index.php");
	exit();
}
include("../localconf.php");

function getKontaktpersonen($id, $stellvertreter)
{
    include("../localconf.php");
	if($id == "")
	{
		$id = 0;
	}
	
	$qry = "SELECT * FROM bibliothek_kontaktperson WHERE bibliothek_kontaktperson.stellvertreter =".$stellvertreter." AND bibliothek_kontaktperson.fk_bibliothek_id = " .$id;

	$result=$mysqli->query($qry);

	if($result)
	{
		if($row = $result->fetch_assoc())
		{
			$array['name']  = $row['name'];
			$array['email'] = $row['email'];
			$array['tel']   = $row['tel'];
			$array['fax']   = $row['fax'];
			return($array);
		}
	}
	else
	{
		echo $mysqli->error;
		die("Query failed");
	}
}

$zeile  = "Schulkennzahl;Schulname;Postleitzahl;Bundesland;Ort;Strasse und Hausnummer;Schulart;Bibliotheksart;WebOpacURL;HomepageURL;E-Mail;Telefon;Raumgröße;Bibliotheksprogramm;Version;Name;Email;Tel;Medienzugang;Medienabgang;Anzahl Schüler;Anzahl Erwachsene im Schulbetrieb;Anzahl ext. Leser;Anzahl komb. Bibliotheken;Anzahl Lesungen;Anzahl Workshops;Anzahl Bibliotheksschulungen;Anzahl Sonstige Veranstaltungen;Gesamt VA;Anzahl Mitarbeiter mit abg. Ausbildung;Anzahl Mitarbeiter ohne abg. Ausbildung;Öffentlichkeitsarbeit\r\n";
$result = $mysqli->query($_SESSION['searchquery_no_limit']);

while($row = $result->fetch_assoc())
{
			/*Schuladressen*/
		  	$zeile .=$row['schulkennzahl'].";";
			$zeile .=$row['schultitel'].";";
			$zeile .=$row['postleitzahl'].";";
			/*Dividiert die Schulkennzahl durch 1000000 und durch das int Typcasting wird die Kommazahl weggeschnitten und es kommt ein gültiger ArrayIndex raus*/
			$zeile .=$row['bundesland'].";";//utf8_decode($bundeslaender_detail[(int)($row['schulkennzahl']/100000)]).";";
			$zeile .=$row['ort'].";";
			$zeile .=$row['strasse_hausnummer'].";";			
			$zeile .=$schultypen2[$row['schulart']].";";			
                          if($row['eigene_bibliothek']==1)
				$zeile .= "Schulbibliothek";
			if($row['gemeinsame_bibliothek']==1)
				$zeile .= "Kombination 2 oder mehrere Schulen";
			if($row['oeff_gemeinsame_bibliothek']==1)
				$zeile .= "Kombination mit öffentlicher Bibliothek";                      
                
                
if(!empty($row['andere_bibliothek'])){
				$zeile .= $row['andere_bibliothek'];
			}                      
                        
                        
			
			$zeile .=";";
			/*Bibliothek*/			
			$zeile .=$row['webopacurl'].";";
			$zeile .=$row['homepageurl'].";";
			$zeile .=$row['email'].";";
			$zeile .=$row['telefon'].";";
                        /*RaumEinrichtung*/
			$zeile .=$row['raumgroesse'].";";			
			/*Verwendetes-Programm*/
			$zeile .=$row['name'].";";
			$zeile .=$row['version'].";";
			/*Kontaktpersonen*/
			/*Erste Person 0, weil kein Stellvertreter*/
			$row2=getKontaktpersonen($row['bid'],0);
			$zeile .=$row2['name'].";";
			$zeile .=$row2['email'].";";
			$zeile .=$row2['tel'].";";			
                        
                        $zeile .=$row['medienzugang'].";";
                        //var_dump($row);die();
$zeile .=$row['medienabgang'].";";
$zeile .=$row['anz_schueler'].";";
$zeile .=$row['anz_erw_in_schulbetrieb'].";";
$zeile .=$row['ext_leser'].";";
$zeile .=$row['kom_bibliotheken'].";";
$zeile .=$row['anz_lesungen'].";";
$zeile .=$row['anz_wokshops'].";";
$zeile .=$row['anz_bibliotheks_schulungen'].";";
$zeile .=$row['anz_sonstige_va'].";";
$zeile .=$row['anz_lesungen']+$row['anz_wokshops']+$row['anz_bibliotheks_schulungen']+$row['anz_sonstige_va'].";";
$zeile .=$row['anz_ma_mit_abg_ausbildung'].";";
$zeile .=$row['anz_ma_ohne_abg_ausbildung'].";";

/*Öffentlichkeitsarbeit
 * 
 */
$oeffentlichkeitsarbeit_text='';
$andere_oeffentlichkeitsarbeit=$row['andere_oeffentlichkeitsarbeit'];
$art_oeffentlichkeitsarbeit=json_decode($row['art_oeffentlichkeitsarbeit']);
        $array_keys_oeffentlichkeitsarbeit=array_keys($art_oeffentlichkeitsarbeit);
        $last_key = end($array_keys_oeffentlichkeitsarbeit);
        
foreach( $art_oeffentlichkeitsarbeit as $key => $value ) {
   if ($key == $last_key) {
       if($value=='6')
     {
          if($andere_oeffentlichkeitsarbeit!='')
            $oeffentlichkeitsarbeit_text.=$andere_oeffentlichkeitsarbeit; 
     }
     else
     {
         $oeffentlichkeitsarbeit_text.=$oeffentlichkeitsarbeit[$value];
     }
     
     
  }else
  {
        if($value=='6')
     {
         if($andere_oeffentlichkeitsarbeit!='')
            $oeffentlichkeitsarbeit_text.=$andere_oeffentlichkeitsarbeit.", "; 
     }
     else
     {
         $oeffentlichkeitsarbeit_text.=$oeffentlichkeitsarbeit[$value].", ";
     }
     
  }
}

//$zeile .=json_decode($row['art_oeffentlichkeitsarbeit']).";";
$zeile .=$oeffentlichkeitsarbeit_text.";";
       $zeile .="\r\n";                 
                        
			
}
$zeile = str_replace("%&quot;%", "", $zeile);
header( "Content-Type: text/comma-separated-values" ); 
header( "Content-Transfer-Encoding: BASE64;" ); 
header( "Content-Disposition: attachment; filename=export.csv" );

echo $zeile;
?>