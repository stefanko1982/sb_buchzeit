<?php 
/**
* Buchzeit APS-Verwaltung
*
* @author Manuel Postlbauer
* @version 0.9
*/

/**
* login
*
* Der Benutzer gibt über ein Formular Benutzername und Passwort ein. Diese Daten werden
* mit den Einträgen in der Datenbank verglichen. Sind diese valide, wird die Sessionvariable "LOGGEDIN" auf 1 gesetzt, der
* Benutzer ist somit eingeloggt und hat Zugriff auf den geschützten Bereich
*
* @param User, Passwort über Formulareingabe
*/

//Beim klicken des Logoutbuttons auf der Internetseite wird man auf die Loginseite weitergeleitet. Durch session_start(); session_destroy(); session_start(); erfolgt der Logout
//und es wird gleich darauf eine Neue Anfrage für den Login vorbereitet.
session_start();
session_destroy();
session_start();
//Variable für das Usermenü, d.h ist der Benutzer eingeloggt wird ein zusätzliches Menü oben auf der Seite angezeigt.  HOME | DATEN EDITIEREN | PROFIL | LOGOUT , dieses Menü wird
//nur angezeigt wenn die Variable auf 1 gesetzt ist.
$displayloginregister=0;

if(isset($_SESSION['LOGGEDIN'])){
	$displayloginregister=1;
}

if(isset($_POST['Submit']))
{
	//Datenbankverbindung aufbauen
	include("../localconf.php");
	
	$error;
	$login = $mysqli->real_escape_string($_POST['login']);
	$password = $mysqli->real_escape_string($_POST['password']);

	if(($login == '')||($password == '')){
		$error='Benutzername oder Passwort fehlt';
	}
	//Gibt es keinen Fehler
	if(!$error){
		//Vergleiche Benutzername und Kennwort mit den Einträgen in der DB
		$qry="SELECT * FROM user WHERE name='".$login."' AND password='".md5($_POST['password'])."'";
		$result=$mysqli->query($qry);

		if($result) {
			if(mysqli_num_rows($result) == 1) {
				//Login erfolgreich
				$member = $result->fetch_assoc();
				//LOGGEDIN wird auf 1 gesetzt, der Benutzer ist erfolgreich eingeloggt.
				$_SESSION['LOGGEDIN'] = 1;
				//Der Primärschlüssel der Tabelle schuladressen wird zur USERID des eingeloggten Benutzers.
				$_SESSION['USERID'] = $member['id'];
				$_SESSION['ADMIN'] = 1;
				$_SESSION['USERNAME'] = $member['name'];
				//Weiterleitung auf den Mitgliedsbereich
				header("location: listschools.php");
				exit();
			}else {
				$error='Ungültiger Benutzername oder ungültiges Passwort eingegeben';
			}
		}else {
			die("Query failed");
		}
	}
}
//Header des Seitendesigns
include("header.php");
?>
<h1>Admin-Login</h1>
<form id="login" name="login" method="post" action="<?php echo $PHP_SELF ?>">
  <table class="listschooltable" width="400" border="0" cellpadding="5" cellspacing="1">
    <tr>
	  <th colspan="2">Login</th>
	</tr>
    <tr class="listschooltablealtbg">
      <td width="120">Benutzername:</td>
      <td width="280"><input name="login" type="text" class="textfield" id="login" /></td>
    </tr>
    <tr class="listschooltablealtbg2">
      <td>Kennwort:</td>
      <td><input name="password" type="password" class="textfield" id="password" /></td>
    </tr>
        <tr class="listschooltablealtbg">
      <td>&nbsp;</td>
      <td><input type="submit" name="Submit" value="" class="loginbutton"/></td>
    </tr>
  </table>
  <div class="important-text">
		<?php
			echo $error;
		?>
  </div>
</form>
<?php
//Footer des Seitendesigns
include("footer.php");
?>