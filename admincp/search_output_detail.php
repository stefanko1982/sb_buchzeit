<?php
/**
* Buchzeit APS-Verwaltung
*
* @author Manuel Postlbauer
* @version 0.9
*/

session_start();
$displayloginregister=0;

if(isset($_SESSION['LOGGEDIN'])){
	$displayloginregister=1;
}

if((!isset($_SESSION['LOGGEDIN']))||($_SESSION['ADMIN']!=1)){
	header("location: index.php");
	exit();
}

$id=$_GET['id'];
if(!(is_numeric($id))){
	header("location: index.php");
}
$_SESSION['search_output_index']=array_search($id,$_SESSION['search_output_ids']);

function getEntries(){
	return(count($_SESSION['search_output_ids']));
}
function NextId(){
	if(($_SESSION['search_output_index']+1) == getEntries()){
		return($_SESSION['search_output_index']);
	}
	else{
		return($_SESSION['search_output_index']+1);
	}
}
function PrevId(){
	if(($_SESSION['search_output_index']-1) <0){
		return($_SESSION['search_output_index']);
	}
	else{
		return($_SESSION['search_output_index']-1);
	}
}

include("../localconf.php");
include("header.php");


$qry="SELECT * FROM schuladressen WHERE id='".$_SESSION['search_output_ids'][$_SESSION['search_output_index']]."' AND schulkennzahl LIKE '".$bundeslaender[$_SESSION['USERNAME']]."%'";
$result=$mysqli->query($qry);
	
if($result) {
	if(mysqli_num_rows($result) == 1) {
		$resultarray = $result->fetch_assoc();

		$school_id=$resultarray['schulkennzahl'];
		$school_name=$resultarray['schultitel'];
		$address_plz=$resultarray['postleitzahl'];
		$address_school_loc=$resultarray['ort'];
		$address_street=$resultarray['strasse_hausnummer'];
		$schooltype=$schultypen[$resultarray['schulart']];
		$gemeinde=$resultarray['schulerhalter_gemeinde'];
		$schulerhalter=$resultarray['schulerhalter_privat'];		
		$classroomcount=$resultarray['klassenanzahl'];
		$address_bundesland=$resultarray['bundesland'];
		if($resultarray['eigene_bibliothek']==1)
				$librarykind_0=1;
			if($resultarray['gemeinsame_bibliothek']==1)
				$librarykind_1=1;
			if($resultarray['oeff_gemeinsame_bibliothek']==1)
				$librarykind_2=1;
                      
                
if(!empty($row['andere_bibliothek'])){
				$librarykind_3=$resultarray['andere_bibliothek'];
			}
		if($resultarray['fk_bibliothek_id']!=0)
			$GLOBAL_BIBLIOTHEK_ID=$resultarray['fk_bibliothek_id'];
	}	
}
else {
	echo $mysqli->error;
	die("Query failed");
}

if(isset($GLOBAL_BIBLIOTHEK_ID)){

	$qry="SELECT * FROM bibliothek WHERE id='".$GLOBAL_BIBLIOTHEK_ID."'";
	$result=$mysqli->query($qry);
		
	if($result) {
		if(mysqli_num_rows($result) == 1) {
			$resultarray = $result->fetch_assoc();

			if($resultarray['fk_bibliothek_verw_prog_id']!=0)
				$GLOBAL_VERW_PROG_ID=$resultarray['fk_bibliothek_verw_prog_id'];
			if($resultarray['fk_bibliothek_medienbest_id']!=0)
				$GLOBAL_MEDIENBESTAND_ID=$resultarray['fk_bibliothek_medienbest_id'];
			if($resultarray['fk_raum_einrichtung_id']!=0)
				$GLOBAL_RAUM_EINRICHTUNG_ID=$resultarray['fk_raum_einrichtung_id'];
			if($resultarray['fk_bibliothek_funkt_nutzung_id']!=0)
				$GLOBAL_FUNKT_NUTZUNG_ID=$resultarray['fk_bibliothek_funkt_nutzung_id'];
			//Step 5 des Formulars	
			$oeffnungstunden=$resultarray['oeffnungsstunden'];
			$webopac=$resultarray['webopacurl'];
			$homepage=$resultarray['homepageurl'];
			$mail=$resultarray['email'];
			$phone=$resultarray['telefon'];	
			//Step 6 des Formulars
			$comments_pos=$resultarray['anregung_gut'];
			$comments_neg=$resultarray['anregung_schlecht'];	
			//Step 5 des Formulars
			$bibliothekaranzahl=$resultarray['anz_schulbibliothekar'];
			$abgwochenstunden=$resultarray['abgeltung_wochenstunden'];
			//Gleiches Prinzip wie bei den Radiobuttons. Ein 1er in der Sessionvariable setzt die Checkbox
			if($resultarray['mitarbeit_schueler']==1)
				$mitarbeitschueler=1;
			if($resultarray['mitarbeit_eltern']==1)
				$mitarbeiteltern=1;
			//Step 5 des Formulars
			$ankaufsbudget=$resultarray['budget'];
		}	
	}
	else {
		echo $mysqli->error;
		die("Query failed");
	}
	//Ist ein Fremdschlüssel für die Bibliothekssoftware vorhanden, lese die Daten für die Bibliothekssoftware aus und speichere sie ins Formular
	if(isset($GLOBAL_VERW_PROG_ID)){
		//Der Fremdschlüssel der Bibliothekssoftware in der Bibliothekstabelle entspricht den Primary Key eines Eintrages in der Tabelle für die Bibliothekssoftware
		$qry="SELECT * FROM bibliothek_verw_prog WHERE id='".$GLOBAL_VERW_PROG_ID."'";
		$result=$mysqli->query($qry);
			
		if($result) {
			if(mysqli_num_rows($result) == 1) {
				$resultarray = $result->fetch_assoc();
				//Hier wird der Name der Bibliothekssoftware abgefragt es wird der jeweilige Name in der Sessionvariable $_SESSION['name'] gespeichert. Es gibt nur eine Variable
				//für den Namen, da nur eine Software verwendet wird.
				
				//Step 3 des Formulars
				if($resultarray['name']=="LITTERA"){
					$islittera=1;
					$litteraver=$resultarray['version'];
				}
				if($resultarray['name']=="EXLIBRIS"){
					$isexlibris=1;
					$exlibrisver=$resultarray['version'];
				}
				if($resultarray['name']=="BOND"){
					$isbond=1;
					$bondver=$resultarray['version'];
				}
				if($resultarray['name']=="BIBLIOWEB"){
					$isbiblioweb=1;
					$bibliowebver=$resultarray['version'];
				}
				//Wurde kein Name der obigen Software gefunden, dann wird eine eigene Software verwendet. Der Name und die Version dieser wird in eigenen Sessionvariablen gespeichert
				if(($resultarray['name']!="LITTERA")&&($resultarray['name']!="EXLIBRIS")&&($resultarray['name']!="BOND")&&($resultarray['name']!="BIBLIOWEB")){
					$isanderes=1;
					$anderesname=$resultarray['name'];
					$anderesver=$resultarray['version'];	
				}
			}	
		}
		else {
			echo $mysqli->error;
			die("Query failed");
		}
	}
	//Ist der Fremdschlüssel für den Medienbestand in der Bibliothekstabelle vorhanden, dann werden die Daten über den Bestand ausgelesen und ins Formular gespeichert.
	if(isset($GLOBAL_MEDIENBESTAND_ID)){
		$qry="SELECT * FROM bibliothek_medienbest WHERE id='".$GLOBAL_MEDIENBESTAND_ID."'";
		$result=$mysqli->query($qry);

		if($result) {
			if(mysqli_num_rows($result) == 1) {
				$resultarray = $result->fetch_assoc();	
				//Step 4 des Formulars
				$belletristik=$resultarray['printmedien_belletristik'];
				$zeitschriften=$resultarray['printmedien_zeitschriften'];
				$tageszeitungen=$resultarray['printmedien_zeitungen'];
				$cddvd=$resultarray['digitale_medien'];
				$downloadang=$resultarray['downloadangebote'];
				$videos=$resultarray['av_medien'];
				$spiele=$resultarray['spiele'];
				$mehrfachtitel=$resultarray['anz_titel_klassenst'];
				$exemplareprotitel=$resultarray['anzahl_titel'];
                                
                                $medienzugang=$resultarray['medienzugang'];
                                        $medienabgang=$resultarray['medienabgang'];
                                        $anz_schueler=$resultarray['anz_schueler'];
                                        $anz_erw_in_schulbetrieb=$resultarray['anz_erw_in_schulbetrieb'];
                                        $ext_leser=$resultarray['ext_leser'];
                                        $kom_bibliotheken=$resultarray['kom_bibliotheken'];
                                        $anz_lesungen=$resultarray['anz_lesungen'];
                                        $anz_wokshops=$resultarray['anz_wokshops'];
                                        $anz_bibliotheks_schulungen=$resultarray['anz_bibliotheks_schulungen'];
                                        $anz_sonstige_va=$resultarray['anz_sonstige_va'];
                                        $anz_ma_mit_abg_ausbildung=$resultarray['anz_ma_mit_abg_ausbildung'];
                                        $anz_ma_ohne_abg_ausbildung=$resultarray['anz_ma_ohne_abg_ausbildung'];
                                        $art_oeffentlichkeitsarbeit=json_decode($resultarray['art_oeffentlichkeitsarbeit']);
                                        $andere_oeffentlichkeitsarbeit=$resultarray['andere_oeffentlichkeitsarbeit'];
                                       
			}	
		}
		else {
			echo $mysqli->error;
			die("Query failed");
		}
	}
        
        $oeffentlichkeitsarbeit_text='';
        $array_keys_oeffentlichkeitsarbeit=array_keys($art_oeffentlichkeitsarbeit);
        $last_key = end($array_keys_oeffentlichkeitsarbeit);
        
foreach( $art_oeffentlichkeitsarbeit as $key => $value ) {
   if ($key == $last_key) {
       if($value=='6')
     {
          if($andere_oeffentlichkeitsarbeit!='')
            $oeffentlichkeitsarbeit_text.=$andere_oeffentlichkeitsarbeit; 
     }
     else
     {
         $oeffentlichkeitsarbeit_text.=$oeffentlichkeitsarbeit[$value];
     }
     
     
  }else
  {
        if($value=='6')
     {
         if($andere_oeffentlichkeitsarbeit!='')
            $oeffentlichkeitsarbeit_text.=$andere_oeffentlichkeitsarbeit."<br /> "; 
     }
     else
     {
         $oeffentlichkeitsarbeit_text.=$oeffentlichkeitsarbeit[$value]."<br /> ";
     }
     
  }
}
	//Ist der Fremdschlüssel für die Raum und Einrichtungstabelle in der Bibliothekstabelle vorhanden, dann werden die Daten über den Raum und die Einrichung ausgelesen und ins Formular gespeichert.
	if(isset($GLOBAL_RAUM_EINRICHTUNG_ID)){
		
		$qry="SELECT * FROM raum_einrichtung WHERE id='".$GLOBAL_RAUM_EINRICHTUNG_ID."'";
		$result=$mysqli->query($qry);

		if($result) {
			if(mysqli_num_rows($result) == 1) {
				$resultarray = $result->fetch_assoc();
				//Step 3 des Formulars
				$roomsize=$resultarray['raumgroesse'];
				if($resultarray['lage_zentral']==1)
					$zentral=1;
				if($resultarray['lage_nahe_edv']==1)
					$lage_nahe_edv=1;
				if($resultarray['lage_schwer_zu_erreichen']==1)
					$schwerzuerr=1;
		
				$arbeitsplatzanzahl=$resultarray['anzahl_arbeitsp'];
				$internetarbeits=$resultarray['anzahl_arbeitsp_i'];
				
				if($resultarray['tvgeraet']==1)
					$tvgeraet=1;
				if($resultarray['beamer']==1)
					$beamer=1;
				if($resultarray['drucker']==1)
					$printer=1;
				if($resultarray['scanner']==1)
					$scanner=1;
				if($resultarray['hoerbuchstation']==1)
					$hoerbuchstation=1;
				if($resultarray['kopiergeraet']==1)
					$kopierger=1;
			}	
		}
		else {
			echo $mysqli->error;
			die("Query failed");
		}
	
	}
	//Ist der Fremdschlüssel für die Funktion und Nutzungstabelle in der Bibliothekstabelle vorhanden, dann werden die Daten über die Funktion und Nutzung ausgelesen und ins Formular gespeichert.
	if(isset($GLOBAL_FUNKT_NUTZUNG_ID)){
	
		$qry="SELECT * FROM bibliothek_funkt_nutzung WHERE id='".$GLOBAL_FUNKT_NUTZUNG_ID."'";
		$result=$mysqli->query($qry);

		if($result) {
			if(mysqli_num_rows($result) == 1) {
				$resultarray = $result->fetch_assoc();		
				//Step 6 des Formulars
				if($resultarray['lesefoerderung']==1)
					$usage_reading=1;
				if($resultarray['individuallernen']==1)
					$usage_learning=1;
				if($resultarray['erwerbmedienkomp']==1)
					$usage_media=1;

				$usage_other=$resultarray['anderes'];
				
				if($resultarray['unterrichttaeg']==1)
					$dailyclass=1;
				if($resultarray['unterrichtwoech']==1)
					$weeklyclass=1;
				if($resultarray['unterrichtmonat']==1)	
					$monthlyclass=1;
				if($resultarray['unterrichtsporad']==1)
					$sporadicclass=1;
				if($resultarray['unterrichtnicht']==1)
					$noclass=1;
				if($resultarray['nutzungveranstaltungen']==1)
					$outsideclass=1;
			}	
		}
		else {
			echo $mysqli->error;
			die("Query failed");
		}
	}
	
	//Filtert die Kontaktpersonen aus der Tabelle bei denen der Fremdschlüssel für die Bibliothek mit dem der Bibliothek für den eingeloggten User übereinstimmt. (schuladressentabelle fk_bibliothek_id == bibliothek_kontaktperson fk_bibliothek_id)
	$qry="SELECT * FROM bibliothek_kontaktperson WHERE fk_bibliothek_id='".$GLOBAL_BIBLIOTHEK_ID."'";
	$result=$mysqli->query($qry);

	if($result) {
			while($resultarray = $result->fetch_assoc()){
				//Ist im Feld Stellvertreter 0 eingetragen, dann ist der Kontakt die Hauptkontaktperson
				if($resultarray['stellvertreter']==0){
					$contact_name=$resultarray['name'];
					$contact_mail=$resultarray['email'];
					$contact_phone=$resultarray['tel'];
					$contact_fax=$resultarray['fax'];
					$contact_person_anz+=1;
				}
				//sonst ist es der Stellvertreter
				else{
					$contact_name2=$resultarray['name'];
					$contact_mail2=$resultarray['email'];
					$contact_phone2=$resultarray['tel'];
					$contact_fax2=$resultarray['fax'];
					$contact_person_anz+=1;
				}
			}
	}
	else {
		echo $mysqli->error;
		die("Query failed");
	}
}
?>
<h1><a href="search_output.php?list=<?php echo $_SESSION['listback']."&tableorder=".array_search($_SESSION['tableorder'],$globaltableorder)."&steigung=".-1*($_SESSION['steigung_status']-1);?>">Erweiterte Suche - Ergebnisse>></a>&nbsp;Detail</h1>
<p>Datensatz&nbsp;<?php echo $_SESSION['search_output_index']+1?>&nbsp;von&nbsp;<?php echo getEntries();?></p>
<p><a href="search_output_detail.php?id=<?php echo $_SESSION['search_output_ids'][PrevId()]; ?>">Zurück</a>&nbsp;|&nbsp;<a href="search_output_detail.php?id=<?php echo $_SESSION['search_output_ids'][NextId()]; ?>">Weiter</a></p>

<div style="float:left; width:410px; margin-left:10px;">
            <table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="400px">
			<tr>
				<th colspan="2">Bibliotheksart</th>
			</tr>
			<tr class="listschooltablealtbg2">
				<td>Schulbibliothek:</td>
				<td><?php if(!empty($librarykind_0)) echo "Ja"; else echo "Nein"; ?></td>
			</tr>
			<tr class="listschooltablealtbg">
				<td>Kombination 2 oder mehrere Schulen:</td>
				<td><?php if(!empty($librarykind_1)) echo "Ja"; else echo "Nein"; ?></td>
			</tr>
			<tr class="listschooltablealtbg2">
				<td>Kombination mit öffentlicher Bibliothek:</td>
				<td><?php if(!empty($librarykind_2)) echo "Ja"; else echo "Nein"; ?></td>
			</tr>
                        <tr class="listschooltablealtbg2">
				<td>Andere Variante:</td>
				<td><?php if(!empty($librarykind_3)) echo $librarykind_3; ?></td>
			</tr>
		</table>
		<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="400px">
			<tr>
				<th colspan="2">Angaben zur Schule</th>
			</tr>
                        <tr class="listschooltablealtbg">
				<td>Schulart:</td>
				<td><?php if((isset($schooltype))) echo $schultypen[$schooltype]; ?></td>
			</tr>                        
			<tr class="listschooltablealtbg2">
				<td>Schulkennzahl:&nbsp;</td>
				<td><?php if(isset($school_id)) echo strip_tags($school_id); ?></td>
			</tr>
			<tr class="listschooltablealtbg">
				<td>Schulname:&nbsp;</td>
				<td><?php if(isset($school_name)) echo strip_tags($school_name); ?></td>
			</tr>
		</table>
		
		<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="400px">
			<tr>
				<th colspan="2">Adresse</th>
			</tr>
			<tr class="listschooltablealtbg">
				<td>Postleitzahl:&nbsp;</td>
				<td><?php if(isset($address_plz)) echo strip_tags($address_plz); ?></td>
			</tr>
                        <tr class="listschooltablealtbg2">
				<td>Schulort:&nbsp;</td>
				<td><?php if(isset($address_school_loc)) echo strip_tags($address_school_loc); ?></td>
			</tr>
                        <tr class="listschooltablealtbg">
				<td>Bundesland:&nbsp;</td>
                                
				<td><?php if(isset($address_bundesland)) echo strip_tags($bundeslaender_detail[$address_bundesland]); ?></td>
			</tr>  
			<tr class="listschooltablealtbg">
				<td>Straße:&nbsp;</td>
				<td><?php if(isset($address_street)) echo strip_tags($address_street); ?></td>
			</tr>
                        
                        <tr class="listschooltablealtbg2">
						<td>Telefon:&nbsp;</td>
						<td><?php if(isset($phone)) echo strip_tags($phone);?></td>
					</tr>
					<tr class="listschooltablealtbg">
						<td>E-Mail-Adresse:&nbsp;</td>
						<td><?php if(isset($mail)) echo strip_tags($mail);?></td>
					</tr>
					<tr class="listschooltablealtbg2">
						<td>Telefon:&nbsp;</td>
						<td><?php if(isset($phone)) echo strip_tags($phone);?></td>
					</tr>
                                        <tr class="listschooltablealtbg">
						<td>Homepage? - URL:</td>
						<td><?php if(isset($homepage)) echo strip_tags($homepage);?></td>
					</tr>
                                        <tr class="listschooltablealtbg2">
						<td>WebOPAC? - URL:</td>
						<td><?php if(isset($webopac)) echo strip_tags($webopac);?></td>
					</tr>
                                        <tr class="listschooltablealtbg">
							<td>Raumgröße (in m²):&nbsp;</td>
							<td><?php if(isset($roomsize)) echo strip_tags($roomsize); ?></td>
						</tr>
                                                <tr>
							<th colspan="2">Bibliotheksverwaltungsprogramm</th>
						</tr>
						<?php
						if(isset($islittera)){
						?>
						<tr class="listschooltablealtbg">
							<td>Name:</td>
							<td>LITTERA</td>
						</tr>
						<tr class="listschooltablealtbg2">
							<td>Version:</td>
							<td><?php if(isset($litteraver)) echo strip_tags($litteraver); ?></td>
						</tr>									
						<?php
						}
						if(isset($isexlibris)){
						?>
						<tr class="listschooltablealtbg">
							<td>Name:</td>
							<td>EXLIBRIS</td>
						</tr>
						<tr class="listschooltablealtbg2">
							<td>Version:</td>
							<td><?php if(isset($exlibrisver)) echo strip_tags($exlibrisver); ?></td>
						</tr>									
						<?php
						}
						if(isset($isbond)){
						?>
						<tr class="listschooltablealtbg">
							<td>Name:</td>
							<td>BOND</td>
						</tr>
						<tr class="listschooltablealtbg2">
							<td>Version:</td>
							<td><?php if(isset($bondver)) echo strip_tags($bondver); ?></td>
						</tr>									
						<?php
						}
						if(isset($isbiblioweb)){
						?>
						<tr class="listschooltablealtbg">
							<td>Name:</td>
							<td>BIBLIOWEB</td>
						</tr>
						<tr class="listschooltablealtbg2">
							<td>Version:</td>
							<td><?php if(isset($bibliowebver)) echo strip_tags($bibliowebver); ?></td>
						</tr>									
						<?php
						}
						if(isset($isanderes)){
						?>
						<tr class="listschooltablealtbg">
							<td>Name:</td>
							<td><?php if(isset($anderesname)) echo strip_tags($anderesname); ?></td>
						</tr>
						<tr class="listschooltablealtbg2">
							<td>Version:</td>
							<td><?php if(isset($anderesver)) echo strip_tags($anderesver); ?></td>
						</tr>									
						<?php
						}
					?>
                </table>
                                                <table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="400px">
		
                                                <tr>
						<th colspan="2">Kontaktperson (ausgebildete Schulbibliothekar/in)</th>
					</tr>
					<tr class="listschooltablealtbg">
						<td>Name:&nbsp;</td>
						<td><?php if(isset($contact_name)) echo strip_tags($contact_name);?></td>
					</tr>
					<tr class="listschooltablealtbg2">
						<td>E-Mail-Adresse:&nbsp;</td>
						<td><?php if(isset($contact_mail)) echo strip_tags($contact_mail);?></td>
					</tr>
					<tr class="listschooltablealtbg">
						<td>Telefon:&nbsp;</td>
						<td><?php if(isset($contact_phone)) echo strip_tags($contact_phone);?></td>
					</tr>
                                         <tr>
						<th colspan="2">Anzahl der MitarbeiterInnen /KollegInnen:</th>
					</tr>                                         
                                        <tr class="listschooltablealtbg2">
						<td>mit abgeschlossener Ausbildung:&nbsp;</td>
						<td><?php if(isset($anz_ma_mit_abg_ausbildung)) echo strip_tags($anz_ma_mit_abg_ausbildung);?></td>
					</tr>
                                        <tr class="listschooltablealtbg">
						<td>ohne abgeschlossener Ausbildung:&nbsp;</td>
						<td><?php if(isset($anz_ma_ohne_abg_ausbildung)) echo strip_tags($anz_ma_ohne_abg_ausbildung);?></td>
					</tr>
                                        
		</table>
            
            
             <table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="400px">
		
                                                <tr>
						<th colspan="2">Medienbestand / Entlehnungen: </th>
					</tr>
					<tr class="listschooltablealtbg">
						<td>Medienzugang im Kalenderjahr:&nbsp;</td>
						<td><?php if(isset($medienzugang)) echo strip_tags($medienzugang);?></td>
					</tr>
					<tr class="listschooltablealtbg2">
						<td>Medienabgang im Kalenderjahr:&nbsp;</td>
						<td><?php if(isset($medienabgang)) echo strip_tags($medienabgang);?></td>
					</tr>
					                                      
                                        
		</table>
            
             <table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="400px">
		
                                                <tr>
						<th colspan="2">Benutzer und Benutzerinnen: </th>
					</tr>
					<tr class="listschooltablealtbg">
						<td>SchülerInnen:&nbsp;</td>
						<td><?php if(isset($anz_schueler)) echo strip_tags($anz_schueler);?></td>
					</tr>
					<tr class="listschooltablealtbg2">
						<td>Erwachsene, im Schulbetrieb tätige, Personen:&nbsp;</td>
						<td><?php if(isset($anz_erw_in_schulbetrieb)) echo strip_tags($anz_erw_in_schulbetrieb);?></td>
					</tr>
					<tr class="listschooltablealtbg">
						<td>externe LeserInnen:&nbsp;</td>
						<td><?php if(isset($ext_leser)) echo strip_tags($ext_leser);?></td>
					</tr>                                                                               
                                        <tr class="listschooltablealtbg2">
						<td>komb. Bibliotheken:&nbsp;</td>
						<td><?php if(isset($kom_bibliotheken)) echo strip_tags($kom_bibliotheken);?></td>
					</tr>                                        
                                        
		</table>	
		
		
		
				
		
		
		
										
							
					
			</div>
<div style="float:left; width:410px; margin-left:10px;">
                            
                              <table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="400px">
		
                                                <tr>
						<th colspan="2">Veranstaltungen und Aktivitäten: </th>
					</tr>
					<tr class="listschooltablealtbg">
						<td>Lesungen:&nbsp;</td>
						<td><?php if(isset($anz_lesungen)) echo strip_tags($anz_lesungen);?></td>
					</tr>
					<tr class="listschooltablealtbg2">
						<td>Workshops Projekte:&nbsp;</td>
						<td><?php if(isset($anz_wokshops)) echo strip_tags($anz_wokshops);?></td>
					</tr>
					<tr class="listschooltablealtbg">
						<td>Bibliotheksdidaktische Schulungen:&nbsp;</td>
						<td><?php if(isset($anz_bibliotheks_schulungen)) echo strip_tags($anz_bibliotheks_schulungen);?></td>
					</tr>                                                                               
                                        <tr class="listschooltablealtbg2">
						<td>Sonstige Veranstaltungen:&nbsp;</td>
						<td><?php if(isset($anz_sonstige_va)) echo strip_tags($anz_sonstige_va);?></td>
					</tr>   
                                        <tr class="listschooltablealtbg2">
						<td>Gesamt:&nbsp;</td>
						<td><?php echo $anz_sonstige_va+$anz_bibliotheks_schulungen+$anz_wokshops+$anz_lesungen;?></td>
					</tr>   
                                        
		</table>	
				 <table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="400px">
					<tr>
						<th colspan="2">Öffentlichkeitsarbeit</th>
					</tr>
					<tr class="listschooltablealtbg">
						<td>Öffentlichkeitsarbeit:</td>
						<td><?php if(isset($oeffentlichkeitsarbeit_text)) echo ($oeffentlichkeitsarbeit_text);?></td>
					</tr>
					
				</table>                          
                            
                            
                           
		
		</table>
	</div>
<div style="clear:both;"></div>
<a class="druckansichtbutton" href="listschools_detail_print.php?id=<?php echo $id;?>" target="_blank" style="display:block;">&nbsp;</a>
<?php
include("footer.php");
?>