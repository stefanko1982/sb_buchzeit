<?php
/**
* Buchzeit APS-Verwaltung
*
* @author Manuel Postlbauer
* @version 0.9
*/
session_start();
unset($_SESSION['searchquery']);
unset($_SESSION['searchquery_no_limit']);
$displayloginregister=0;

if(isset($_SESSION['LOGGEDIN'])){
	$displayloginregister=1;
}

if((!isset($_SESSION['LOGGEDIN']))||($_SESSION['ADMIN']!=1)){
	header("location: index.php");
	exit();
}

$counter=0;
function coloredRow(){
	global $counter;
	if($counter%2==0){
		$counter++;
		return("<tr class='listschooltablealtbg'>");
	}
	else{
		$counter++;
		return("<tr class='listschooltablealtbg2'>");
	}
}

function field($name, $value) {
	$form  = coloredRow();
	$form .= "<td>" .$name. "</td>";
	$form .= "<td style='width: 75px'>";
	$form .= "<select name='operator[" .$value. "]' class='klein' style='width: 60px'>";
	$form .= "<option value='0'>=</option>";
	$form .= "<option value='1'>Like</option>";
	$form .= "<option value='2'>>=</option>";
	$form .= "<option value='3'>=<</option>";
	$form .= "</select>";
	$form .= "</td>";
	$form .= '<td><input name="value[' .$value. ']" type="text" size="30" style="width: 200px" /></td>';
	$form .= "</tr>";
	
	echo $form;
}

function field_array($name, $value, $array)  {
	$form  = coloredRow();
	$form .= "<td>" .$name. "</td>";
	$form .= "<td>";
	$form .= "<select name='operator[" .$value. "]' class='klein' style='width: 60px'>";
	$form .= "<option value='0'>=</option>";
	$form .= "</select>";
	$form .= "</td>";
	$form .= '<td>';
	$form .= "<select name='value[" .$value. "]' style='width: 206px'>";
	$form .= "<option value=''></option>";
	foreach($array as $key => $value) {
		$form .= "<option value='" .$key. "'>" .utf8_decode($value). "</option>";
	}
	$form .= "</select>";
	$form .= '</td>';
	$form .= "</tr>";
	
	echo $form;
}

function field_array_multi($name, $value, $array)  {
	$form  = coloredRow();
	$form .= "<td style='vertical-align: top'>" .$name. "</td>";
	$form .= "<td style='vertical-align: top'>";
	$form .= "<select name='operator[" .$value. "]' class='klein' style='width: 60px'>";
	$form .= "<option value='0'>=</option>";
	$form .= "</select>";
	$form .= "</td>";
	$form .= '<td style="vertical-align: top">';
	$form .= "<select name='value[" .$value. "][]' style='width: 206px' multiple='multiple'>";
	foreach($array as $key => $value) {
		$form .= "<option value='" .$key. "'>" .$value. "</option>";
	}
	$form .= "</select>";
	$form .= '</td>';
	$form .= "</tr>";
	
	echo $form;
}

function field_bool($name, $value) {
	$form  = coloredRow();
	$form .= "<td>" .$name. "</td>";
	$form .= "<td>";
	$form .= "<select name='operator[" .$value. "]' class='klein' style='width: 60px'>";
	$form .= "<option value='0'>=</option>";
	$form .= "</select>";
	$form .= "</td>";
	$form .= "<td>";
	$form .= "<select name='value[" .$value. "]' style='width: 206px'>";
	$form .= "<option value=''></option>";
	$form .= "<option value='0'>nein</option>";
	$form .= "<option value='1'>ja</option>";
	$form .= "</select>";
	$form .= "</tr>";	
	
	echo $form;
}
include("../localconf.php");
include("header.php");
?>

<h1>Erweiterte Suche</h1>

<form action="search_output.php" method="post">

<table class="listschooltable" cellpadding="5" cellspacing="1">
	<tr>
		<th colspan="10">Filter</th>
	<tr>
	<?php field("Schulkennzahl:", "schulkennzahl"); ?>
    <?php field("Schulbezeichnung:", "schultitel"); ?>
    <?php field("Postleitzahl:", "postleitzahl"); ?>
    <?php field("Ort:", "ort"); ?>
    <?php field("Straße und Hausnummer:", "strasse_hausnummer"); ?>
    <?php field("Schulerhalter Gemeinde:", "schulerhalter_gemeinde"); ?>
    <?php field("Schulerhalter Privat:", "schulerhalter_privat"); ?>
    <?php field_array("Schulart:", "schulart", $schultypen2); ?>
    <?php field("Klassenanzahl:", "klassenanzahl"); ?>
    <?php field_array("Bibliotheksart:", "bibliotheksart",$bibliotheksart); ?>
	<?php 
	//Prüft ob der Adminbenutzer eingeloggt ist oder einer der Benutzer für ein jeweiliges Bundesland
	//Nur wenn der Admin eingeloggt ist kommt ein Leerstring zurück. Dann kann man bei der Auswahl nach Bundesländern filtern.
	global $bundeslaender;
	if($bundeslaender[$_SESSION['USERNAME']]=="")
		field_array("Bundesländer:", "bundeslaender",$bundeslaender_detail); 
	?>
</table>

<input name="submit" type="submit" value="" class="suchbutton"/>

</form>

<?php
include("footer.php");
?>