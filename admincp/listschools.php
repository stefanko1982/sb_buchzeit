<?php
/**
* Buchzeit APS-Verwaltung
*
* @author Manuel Postlbauer
* @version 0.9
*/
include("../localconf.php");
session_start();
unset($_SESSION['listschools_ids']);
unset($_SESSION['listschools_index']);
unset($_SESSION['searchquery_no_limit']);
$displayloginregister=0;

if(isset($_SESSION['LOGGEDIN'])){
	$displayloginregister=1;
}

if((!isset($_SESSION['LOGGEDIN']))||($_SESSION['ADMIN']!=1)){
	header("location: index.php");
	exit();
}

if(!isset($_SESSION['FIRSTLOGIN'])){
	$_SESSION['opt_schulkennzahl']=1;
	$_SESSION['opt_schultitel']=1;
	$_SESSION['opt_schulart']=1;
	$_SESSION['opt_bibliotheksart']=1;
	$_SESSION['FIRSTLOGIN']=1;
}


$list=($_GET['list']);
if(!(is_numeric($list))){
	$list=0;
}
$_SESSION['listback']=$list;

$tableorder=($_GET['tableorder']);
$steigung=($_GET['steigung']);
if(!(is_numeric($steigung))){
	$_SESSION['steigung']=0;
}
$_SESSION['steigung_status']=$steigung;
if($_SESSION['steigung_status']==0){
	$_SESSION['steigung']="ASC";
	$_SESSION['steigung_status']=1;
}
else{
	$_SESSION['steigung']="DESC";
	$_SESSION['steigung_status']=0;
}	
if(!(is_numeric($tableorder))){
	$_SESSION['tableorder']=$globaltableorder[0];
}
else{
	if($tableorder>8){
		$tableorder=8;
	}
	else{
		$_SESSION['tableorder']=$globaltableorder[$tableorder];
	}
}
if($tableorder=""){
	$_SESSION['tableorder']=$globaltableorder[8];
}
	
include("header.php");

function getEntries(){
	global $bundeslaender;
        include("../localconf.php");
        //$mysqli->query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'");
	$qry="SELECT COUNT(id) AS anzahl FROM schuladressen WHERE schulkennzahl LIKE '".$bundeslaender[$_SESSION['USERNAME']]."%'";
	$result=$mysqli->query($qry);
	if($result) {
		if($row =$result->fetch_assoc()){
			return($row['anzahl']);
		}
		else{
			echo $mysqli->error;
			die("Query failed");
		}
	}
	else {
		echo $mysqli->error;
		die("Query failed");
	}
}
function listNext($list){
	if($list+30 >= getEntries()){
		//Wenn weniger als 30 Einträge sind wäre limit -29 bei der nächsten Seite übergeben worden wenn zb nur 1 Eintrag
		if(getEntries()<30)
			return (getEntries()-1);
		else
			return (getEntries()-30);
	}
	else{
		return($list+30);
	}
}
function listPrev($list){
	if($list-30 <=0){
		return("0");
	}
	else{
		return($list-30);
	}
}
//Für Datensätze x bis x
function showNext($list){
	if($list+30 >= getEntries()){
		return(getEntries());
	}
	else{
		return($list+30);
	}
}
if(isset($_POST['Submit'])){
/*******
*Optionsmenü
***********/

	/*Schuladressen*/
	
	if(isset($_POST['opt_schulkennzahl'])){
		$_SESSION['opt_schulkennzahl']=1;
	}
	else{
		unset($_SESSION['opt_schulkennzahl']);
	}
	if(isset($_POST['opt_schultitel'])){
		$_SESSION['opt_schultitel']=1;
	}
	else{
		unset($_SESSION['opt_schultitel']);
	}
	
	if(isset($_POST['opt_postleitzahl'])){
		$_SESSION['opt_postleitzahl']=1;
	}
	else{	
		unset($_SESSION['opt_postleitzahl']);
	}
	
	if(isset($_POST['opt_ort'])){
		$_SESSION['opt_ort']=1;
	}
	else{	
		unset($_SESSION['opt_ort']);
	}
	
	if(isset($_POST['opt_strasse_hausnummer'])){
		$_SESSION['opt_strasse_hausnummer']=1;
	}
	else{	
		unset($_SESSION['opt_strasse_hausnummer']);
	}
	
	if(isset($_POST['opt_schulart'])){
		$_SESSION['opt_schulart']=1;
	}
	else{	
		unset($_SESSION['opt_schulart']);
	}

	if(isset($_POST['opt_klassenanzahl'])){
		$_SESSION['opt_klassenanzahl']=1;
	}
	else{	
		unset($_SESSION['opt_klassenanzahl']);
	}
	
	/*Bibliothek*/
	
	if(isset($_POST['opt_budget'])){
		$_SESSION['opt_budget']=1;
	}
	else{	
		unset($_SESSION['opt_budget']);
	}
	
	if(isset($_POST['opt_bibliotheksart'])){
		$_SESSION['opt_bibliotheksart']=1;
	}
	else{	
		unset($_SESSION['opt_bibliotheksart']);
	}
	
	if(isset($_POST['opt_oeffnungsstunden'])){
		$_SESSION['opt_oeffnungsstunden']=1;
	}
	else{	
		unset($_SESSION['opt_oeffnungsstunden']);
	}
	
	/*Bibliothek Medienbestand*/
	
	if(isset($_POST['opt_printmedien_belletristik'])){
		$_SESSION['opt_printmedien_belletristik']=1;
	}
	else{	
		unset($_SESSION['opt_printmedien_belletristik']);
	}
	
	if(isset($_POST['opt_printmedien_zeitschriften'])){
		$_SESSION['opt_printmedien_zeitschriften']=1;
	}
	else{	
		unset($_SESSION['opt_printmedien_zeitschriften']);
	}
	
	if(isset($_POST['opt_printmedien_zeitungen'])){
		$_SESSION['opt_printmedien_zeitungen']=1;
	}
	else{	
		unset($_SESSION['opt_printmedien_zeitungen']);
	}
	
	if(isset($_POST['opt_digitale_medien'])){
		$_SESSION['opt_digitale_medien']=1;
	}
	else{	
		unset($_SESSION['opt_digitale_medien']);
	}	
	
	if(isset($_POST['opt_downloadangebote'])){
		$_SESSION['opt_downloadangebote']=1;
	}
	else{	
		unset($_SESSION['opt_downloadangebote']);
	}
	
	if(isset($_POST['opt_av_medien'])){
		$_SESSION['opt_av_medien']=1;
	}
	else{	
		unset($_SESSION['opt_av_medien']);
	}
	
	if(isset($_POST['opt_spiele'])){
		$_SESSION['opt_spiele']=1;
	}
	else{	
		unset($_SESSION['opt_spiele']);
	}
	
	if(isset($_POST['opt_anz_titel_klassenst'])){
		$_SESSION['opt_anz_titel_klassenst']=1;
	}
	else{	
		unset($_SESSION['opt_anz_titel_klassenst']);
	}
	
	if(isset($_POST['opt_anzahl_titel'])){
		$_SESSION['opt_anzahl_titel']=1;
	}
	else{	
		unset($_SESSION['opt_anzahl_titel']);
	}
	/*Raum Einrichtung*/
	if(isset($_POST['opt_raumgroesse'])){
		$_SESSION['opt_raumgroesse']=1;
	}
	else{	
		unset($_SESSION['opt_raumgroesse']);
	}
	
	if(isset($_POST['opt_lage'])){
		$_SESSION['opt_lage']=1;
	}
	else{	
		unset($_SESSION['opt_lage']);
	}
	
	if(isset($_POST['opt_anzahl_arbeitsp'])){
		$_SESSION['opt_anzahl_arbeitsp']=1;
	}
	else{	
		unset($_SESSION['opt_anzahl_arbeitsp']);
	}
	
	if(isset($_POST['opt_anzahl_arbeitsp_i'])){
		$_SESSION['opt_anzahl_arbeitsp_i']=1;
	}
	else{	
		unset($_SESSION['opt_anzahl_arbeitsp_i']);
	}
	
	if(isset($_POST['opt_tvgeraet'])){
		$_SESSION['opt_tvgeraet']=1;
	}
	else{	
		unset($_SESSION['opt_tvgeraet']);
	}
	
	if(isset($_POST['opt_beamer'])){
		$_SESSION['opt_beamer']=1;
	}
	else{	
		unset($_SESSION['opt_beamer']);
	}
	
	if(isset($_POST['opt_drucker'])){
		$_SESSION['opt_drucker']=1;
	}
	else{	
		unset($_SESSION['opt_drucker']);
	}
	
	if(isset($_POST['opt_scanner'])){
		$_SESSION['opt_scanner']=1;
	}
	else{	
		unset($_SESSION['opt_scanner']);
	}
	
	if(isset($_POST['opt_hoerbuchstation'])){
		$_SESSION['opt_hoerbuchstation']=1;
	}
	else{	
		unset($_SESSION['opt_hoerbuchstation']);
	}
	
	if(isset($_POST['opt_kopiergeraet'])){
		$_SESSION['opt_kopiergeraet']=1;
	}
	else{	
		unset($_SESSION['opt_kopiergeraet']);
	}
	/*Funktion - Nutzung*/
	if(isset($_POST['opt_lesefoerderung'])){
		$_SESSION['opt_lesefoerderung']=1;
	}
	else{	
		unset($_SESSION['opt_lesefoerderung']);
	}
	
	if(isset($_POST['opt_individuallernen'])){
		$_SESSION['opt_individuallernen']=1;
	}
	else{	
		unset($_SESSION['opt_individuallernen']);
	}
	
	if(isset($_POST['opt_erwerbmedienkomp'])){
		$_SESSION['opt_erwerbmedienkomp']=1;
	}
	else{	
		unset($_SESSION['opt_erwerbmedienkomp']);
	}
	
	if(isset($_POST['opt_anderes'])){
		$_SESSION['opt_anderes']=1;
	}
	else{	
		unset($_SESSION['opt_anderes']);
	}
	
	if(isset($_POST['opt_unterricht'])){
		$_SESSION['opt_unterricht']=1;
	}
	else{	
		unset($_SESSION['opt_unterricht']);
	}
	
	if(isset($_POST['opt_nutzungveranstaltungen'])){
		$_SESSION['opt_nutzungveranstaltungen']=1;
	}
	else{	
		unset($_SESSION['opt_nutzungveranstaltungen']);
	}
}	if(isset($_POST['opt_bundesland'])){
		$_SESSION['opt_bundesland']=1;
	}
	else{	
		unset($_SESSION['opt_bundesland']);
	}
	if(isset($_POST['opt_bibliothek_verw_prog'])){
		$_SESSION['opt_bibliothek_verw_prog']=1;
	}
	else{	
		unset($_SESSION['opt_bibliothek_verw_prog']);
	}
	if(isset($_POST['opt_oeffentlichkeitsarbeit'])){
		$_SESSION['opt_oeffentlichkeitsarbeit']=1;
	}
	else{	
		unset($_SESSION['opt_oeffentlichkeitsarbeit']);
	}

?>
<h1>Filter</h1>
<form id="filter" name="filter" method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>">
	<div class="filterfloat" style="float:left; margin-right:10px;">
		<table>
                    <tr class="listschooltablealtbg">
				<td>Bibliotheksart:</td>
				<td><input type="checkbox" name="opt_bibliotheksart" value="1" <?php if(!empty($_SESSION['opt_bibliotheksart'])) echo "checked"; ?> /></td>
			</tr>
                    <tr class="listschooltablealtbg2">
					<td>Schulart:</td>
					<td><input type="checkbox" name="opt_schulart" value="1" <?php if(!empty($_SESSION['opt_schulart'])) echo "checked"; ?> /></td>
			</tr>
			<tr class="listschooltablealtbg">
					<td>Schulkennzahl:</td>
					<td><input type="checkbox" name="opt_schulkennzahl" value="1" <?php if(!empty($_SESSION['opt_schulkennzahl'])) echo "checked"; ?> /></td>
			</tr>
			<tr class="listschooltablealtbg2">
					<td>Schulname:</td>
					<td><input type="checkbox" name="opt_schultitel" value="1" <?php if(!empty($_SESSION['opt_schultitel'])) echo "checked"; ?> /></td>
			</tr>
			<tr class="listschooltablealtbg">
					<td>Postleitzahl:</td>
					<td><input type="checkbox" name="opt_postleitzahl" value="1" <?php if(!empty($_SESSION['opt_postleitzahl'])) echo "checked"; ?> /></td>
			</tr>
                        <tr class="listschooltablealtbg2">
					<td>Bundesland:</td>
					<td><input type="checkbox" name="opt_bundesland" value="1" <?php if(!empty($_SESSION['opt_bundesland'])) echo "checked"; ?> /></td>
			</tr>
			<tr class="listschooltablealtbg2">
					<td>Ort:</td>
					<td><input type="checkbox" name="opt_ort" value="1" <?php if(!empty($_SESSION['opt_ort'])) echo "checked"; ?> /></td>
			</tr>
			<tr class="listschooltablealtbg">
					<td>Strasse und Hausnummer:</td>
					<td><input type="checkbox" name="opt_strasse_hausnummer" value="1" <?php if(!empty($_SESSION['opt_strasse_hausnummer'])) echo "checked"; ?> /></td>
			</tr>
				
                        <tr class="listschooltablealtbg">
				<td>Raumgröße:</td>
				<td><input type="checkbox" name="opt_raumgroesse" value="1" <?php if(!empty($_SESSION['opt_raumgroesse'])) echo "checked"; ?> /></td>
			</tr>
		</table>
	</div>

	<div class="filterfloat" style="float:left; margin-right:10px;">
		<table>
                    <tr class="listschooltablealtbg">
				<td>Bibliotheksverwaltungsprogramm:</td>
				<td><input type="checkbox" name="opt_bibliothek_verw_prog" value="1" <?php if(!empty($_SESSION['opt_bibliothek_verw_prog'])) echo "checked"; ?> /></td>
			</tr>	
                         <tr class="listschooltablealtbg">
				<td>Öffentlichkeitsarbeit:</td>
				<td><input type="checkbox" name="opt_oeffentlichkeitsarbeit" value="1" <?php if(!empty($_SESSION['opt_oeffentlichkeitsarbeit'])) echo "checked"; ?> /></td>
			</tr>
			
		</table>
	</div>
	<!--Medienbestand-->
	<div class="filterfloat" style="float:left; margin-right:10px;">
		
	</div>
	<!--Raum-Einrichtung-->
	<div class="filterfloat" style="float:left; margin-right:10px;">
		
	</div>
	<!--Funktion-Nutzung-->
	<div class="filterfloat" style="float:left; margin-right:10px;">
		
	</div>

	<div style="clear:both;"></div>
	
<input type="submit" name="Submit" value="" class="bestaetigenbutton"/>
</form>
<div style="border-bottom:1px solid #8A93A2;width:100%;height:1px; margin-top:10px; margin-bottom:10px;"></div>
<h1>Schulliste</h1>
<p>Datensätze&nbsp;<?php echo $list+1?>&nbsp;bis&nbsp;<?php echo showNext($list);?>&nbsp;von&nbsp;<?php echo getEntries();?></p>
<p><a href="listschools.php?list=<?php echo listPrev($list); ?>&tableorder=<?php echo array_search($_SESSION['tableorder'],$globaltableorder)."&steigung=".-1*($_SESSION['steigung_status']-1);?>">Zurück |&nbsp;</a><a href="listschools.php?list=<?php echo listNext($list); ?>&tableorder=<?php echo array_search($_SESSION['tableorder'],$globaltableorder)."&steigung=".-1*($_SESSION['steigung_status']-1);?>">Weiter</a></p>
<table class="listschooltable" cellpadding="5" cellspacing="1" width="100%">
	<tr>
	<?php
		/*Schuladressen*/
		if(isset($_SESSION['opt_schulkennzahl']))
			echo"<th><a href='listschools.php?list=".$list."&tableorder=0&steigung=".$_SESSION['steigung_status']."'>Schulkennzahl</a></th>";
		if(isset($_SESSION['opt_schultitel']))
			echo"<th><a href='listschools.php?list=".$list."&tableorder=1&steigung=".$_SESSION['steigung_status']."'>Schulname</a></th>";
		if(isset($_SESSION['opt_postleitzahl']))
			echo"<th><a href='listschools.php?list=".$list."&tableorder=2&steigung=".$_SESSION['steigung_status']."'>Postleitzahl</a></th>";
		if(isset($_SESSION['opt_ort']))
			echo"<th><a href='listschools.php?list=".$list."&tableorder=3&steigung=".$_SESSION['steigung_status']."'>Ort</a></th>";
		if(isset($_SESSION['opt_strasse_hausnummer']))
			echo"<th><a href='listschools.php?list=".$list."&tableorder=4&steigung=".$_SESSION['steigung_status']."'>Straße</a></th>";
		if(isset($_SESSION['opt_schulart']))
			echo"<th><a href='listschools.php?list=".$list."&tableorder=5&steigung=".$_SESSION['steigung_status']."'>Schulart</a></th>";
		if(isset($_SESSION['opt_klassenanzahl']))
			echo"<th><a href='listschools.php?list=".$list."&tableorder=6&steigung=".$_SESSION['steigung_status']."'>Klassenanzahl</a></th>";
		/*Bibliothek*/
		if(isset($_SESSION['opt_budget']))
			echo"<th><a href='listschools.php?list=".$list."&tableorder=7&steigung=".$_SESSION['steigung_status']."'>Budget</a></th>";
		if(isset($_SESSION['opt_bibliotheksart']))
			echo"<th>Bibliotheksart</th>";
		if(isset($_SESSION['opt_oeffnungsstunden']))
			echo"<th>Öffnungsstunden</th>";
		/*Medienbestand*/
		if(isset($_SESSION['opt_printmedien_belletristik']))
			echo"<th>Printmedien - Belletristik</th>";
		if(isset($_SESSION['opt_printmedien_zeitschriften']))
			echo"<th>Printmedien - Zeitschriften (Abo)</th>";
		if(isset($_SESSION['opt_printmedien_zeitungen']))
			echo"<th>Printmedien - Zeitungen (Abo)</th>";
		if(isset($_SESSION['opt_digitale_medien']))
			echo"<th>Digitale Medien</th>";
		if(isset($_SESSION['opt_downloadangebote']))
			echo"<th>Downloadangebote</th>";
		if(isset($_SESSION['opt_av_medien']))
			echo"<th>AV Medien</th>";
		if(isset($_SESSION['opt_spiele']))
			echo"<th>Spiele</th>";
		if(isset($_SESSION['opt_anz_titel_klassenst']))
			echo"<th>Anzahl der Titel in Klassenstärke</th>";
		if(isset($_SESSION['opt_anzahl_titel']))
			echo"<th>Anzahl der Titel</th>";
		/*Raum-Einrichtung*/
		if(isset($_SESSION['opt_raumgroesse']))
			echo"<th>Raumgröße</th>";
		if(isset($_SESSION['opt_lage']))
			echo"<th>Lage</th>";
		if(isset($_SESSION['opt_anzahl_arbeitsp']))
			echo"<th>Anzahl der Schüler-Arbeitsplätze</th>";
		if(isset($_SESSION['opt_anzahl_arbeitsp_i']))
			echo"<th>Anzahl der Internet-Arbeitsplätze</th>";
		if(isset($_SESSION['opt_tvgeraet']))
			echo"<th>Fernseher</th>";
		if(isset($_SESSION['opt_beamer']))
			echo"<th>Beamer</th>";
		if(isset($_SESSION['opt_drucker']))
			echo"<th>Drucker</th>";
		if(isset($_SESSION['opt_scanner']))
			echo"<th>Scanner</th>";
		if(isset($_SESSION['opt_hoerbuchstation']))
			echo"<th>Hörbuchstation</th>";
		if(isset($_SESSION['opt_kopiergeraet']))
			echo"<th>Kopiergerät</th>";
		/*Funktion-Nutzung*/
		if(isset($_SESSION['opt_lesefoerderung']))
			echo"<th>Leseförderung</th>";
		if(isset($_SESSION['opt_individuallernen']))
			echo"<th>Individualisiertes Lernen</th>";
		if(isset($_SESSION['opt_erwerbmedienkomp']))
			echo"<th>Erwerb Medienkompetenz</th>";
		if(isset($_SESSION['opt_anderes']))
			echo"<th>Anderes</th>";
		if(isset($_SESSION['opt_unterricht']))
			echo"<th>Unterricht</th>";
		if(isset($_SESSION['opt_nutzungveranstaltungen']))
			echo"<th>Nutzung für Veranstaltungen</th>";
                if(isset($_SESSION['opt_bundesland']))
			echo"<th>Bundesland</th>";			
		if(isset($_SESSION['opt_bibliothek_verw_prog']))
			echo"<th>Bibliotheksverwaltungsprogramm</th>";
		if(isset($_SESSION['opt_oeffentlichkeitsarbeit']))
			echo"<th>Öffentlichkeitsarbeit</th>";
	?>
	</tr>
	<?php
	$_SESSION['searchquery_no_limit']=$qry="SELECT * , schuladressen.id AS sid, schuladressen.fk_bibliothek_id AS bid
			FROM 
			schuladressen

			LEFT JOIN
			bibliothek
			ON
			schuladressen.fk_bibliothek_id=bibliothek.id

			LEFT JOIN
			bibliothek_verw_prog
			ON
			bibliothek.fk_bibliothek_verw_prog_id=bibliothek_verw_prog.id

			LEFT JOIN
			bibliothek_medienbest
			ON
			bibliothek.fk_bibliothek_medienbest_id=bibliothek_medienbest.id

			LEFT JOIN
			raum_einrichtung
			ON
			bibliothek.fk_raum_einrichtung_id=raum_einrichtung.id

			LEFT JOIN
			bibliothek_funkt_nutzung
			ON
			bibliothek.fk_bibliothek_funkt_nutzung_id=bibliothek_funkt_nutzung.id

			WHERE
			schuladressen.schulkennzahl LIKE '".$bundeslaender[$_SESSION['USERNAME']]."%'

			ORDER BY ".$_SESSION['tableorder']." ".$_SESSION['steigung']."";
			
	$qry="SELECT * , schuladressen.id AS sid, schuladressen.fk_bibliothek_id AS bid
			FROM 
			schuladressen

			LEFT JOIN
			bibliothek
			ON
			schuladressen.fk_bibliothek_id=bibliothek.id

			LEFT JOIN
			bibliothek_verw_prog
			ON
			bibliothek.fk_bibliothek_verw_prog_id=bibliothek_verw_prog.id

			LEFT JOIN
			bibliothek_medienbest
			ON
			bibliothek.fk_bibliothek_medienbest_id=bibliothek_medienbest.id

			LEFT JOIN
			raum_einrichtung
			ON
			bibliothek.fk_raum_einrichtung_id=raum_einrichtung.id

			LEFT JOIN
			bibliothek_funkt_nutzung
			ON
			bibliothek.fk_bibliothek_funkt_nutzung_id=bibliothek_funkt_nutzung.id

			WHERE
			schuladressen.schulkennzahl LIKE '".$bundeslaender[$_SESSION['USERNAME']]."%'

			ORDER BY ".$_SESSION['tableorder']." ".$_SESSION['steigung']." LIMIT ".$list.",30";
			
	$result=$mysqli->query($qry);        
	$counter=0;
	if($result) {
		while($row = $result->fetch_assoc()){
			if($counter%2==0){
			?>
				<tr class="listschooltablealtbg">
			<?php
			}
			else{
			?>
				<tr class="listschooltablealtbg2">
			<?php
			}
			$_SESSION['listschools_ids'][$counter]=$row['sid'];
			$counter++;	
			
				if(isset($_SESSION['opt_schulkennzahl']))
					echo"<td><a href='listschools_detail.php?id=".$row['sid']."'>".$row['schulkennzahl']."</a></td>";
				if(isset($_SESSION['opt_schultitel']))
					echo"<td>".$row['schultitel']."</td>";
				if(isset($_SESSION['opt_postleitzahl']))	
					echo"<td>".$row['postleitzahl']."</td>";
				if(isset($_SESSION['opt_ort']))
					echo"<td>".$row['ort']."</td>";
				if(isset($_SESSION['opt_strasse_hausnummer']))
					echo"<td>".$row['strasse_hausnummer']."</td>";
				if(isset($_SESSION['opt_schulart']))
					echo"<td>".$schultypen2[$row['schulart']]."</td>";
				if(isset($_SESSION['opt_klassenanzahl']))
					echo"<td>".$row['klassenanzahl']."</td>";
				/*Bibliothek*/
				if(isset($_SESSION['opt_budget']))
					if($row['budget']!="")
						echo"<td>".$row['budget']." €</td>";
					else
						echo"<td>&nbsp;</td>";
				if(isset($_SESSION['opt_bibliotheksart'])){
					echo"<td>";
                                        
                                        
                                        
					if($row['fk_bibliothek_id']!=0){
                                            if($row['eigene_bibliothek']==1)
				echo "Schulbibliothek:";
			if($row['gemeinsame_bibliothek']==1)
				echo "Kombination 2 oder mehrere Schulen:";
			if($row['oeff_gemeinsame_bibliothek']==1)
				echo "Kombination mit öffentlicher Bibliothek:";                      
                
                
if(!empty($row['andere_bibliothek'])){
				echo "Andere Variante: ".$row['andere_bibliothek'];
			}
						
					}
					else{
						echo "Keine Bibliothek";
					}
					echo"</td>";
				}
				if(isset($_SESSION['opt_oeffnungsstunden']))
					echo"<td>".$row['oeffnungsstunden']."</td>";
				/*Medienbestand*/
				if(isset($_SESSION['opt_printmedien_belletristik']))
					echo"<td>".$row['printmedien_belletristik']."</td>";
				if(isset($_SESSION['opt_printmedien_zeitschriften']))
					echo"<td>".$row['printmedien_zeitschriften']."</td>";
				if(isset($_SESSION['opt_printmedien_zeitungen']))
					echo"<td>".$row['printmedien_zeitungen']."</td>";
				if(isset($_SESSION['opt_digitale_medien']))
					echo"<td>".$row['digitale_medien']."</td>";
				if(isset($_SESSION['opt_downloadangebote']))
					echo"<td>".$row['downloadangebote']."</td>";
				if(isset($_SESSION['opt_av_medien']))
					echo"<td>".$row['av_medien']."</td>";
				if(isset($_SESSION['opt_spiele']))
					echo"<td>".$row['spiele']."</td>";
				if(isset($_SESSION['opt_anz_titel_klassenst']))
					echo"<td>".$row['anz_titel_klassenst']."</td>";
				if(isset($_SESSION['opt_anzahl_titel']))
					echo"<td>".$row['anzahl_titel']."</td>";
				/*RaumEinrichtung*/
				if(isset($_SESSION['opt_raumgroesse']))
					echo"<td>".$row['raumgroesse']."</td>";
				if(isset($_SESSION['opt_lage'])){
				echo"<td>";
					if($row['lage_zentral']==1)
						echo"zentral ";
					if($row['lage_nahe_edv']==1)
						echo"nahe EDV ";
					if($row['lage_schwer_zu_erreichen']==1)
						echo"schwer zu erreichen ";
					else
						echo"&nbsp;";
				echo"</td>";
				}
				if(isset($_SESSION['opt_anzahl_arbeitsp']))
					echo"<td>".$row['anzahl_arbeitsp']."</td>";
				if(isset($_SESSION['opt_anzahl_arbeitsp_i']))
					echo"<td>".$row['anzahl_arbeitsp_i']."</td>";
				if(isset($_SESSION['opt_tvgeraet']))
					echo"<td>".$yesorno[$row['tvgeraet']]."</td>";
				if(isset($_SESSION['opt_beamer']))
					echo"<td>".$yesorno[$row['beamer']]."</td>";
				if(isset($_SESSION['opt_drucker']))
					echo"<td>".$yesorno[$row['drucker']]."</td>";
				if(isset($_SESSION['opt_scanner']))
					echo"<td>".$yesorno[$row['scanner']]."</td>";
				if(isset($_SESSION['opt_hoerbuchstation']))
					echo"<td>".$yesorno[$row['hoerbuchstation']]."</td>";
				if(isset($_SESSION['opt_kopiergeraet']))
					echo"<td>".$yesorno[$row['kopiergeraet']]."</td>";
				/*Funktion-Nutzung*/
				if(isset($_SESSION['opt_lesefoerderung']))
					echo"<td>".$yesorno[$row['lesefoerderung']]."</td>";
				if(isset($_SESSION['opt_individuallernen']))
					echo"<td>".$yesorno[$row['individuallernen']]."</td>";
				if(isset($_SESSION['opt_erwerbmedienkomp']))
					echo"<td>".$yesorno[$row['erwerbmedienkomp']]."</td>";
				if(isset($_SESSION['opt_anderes']))
					echo"<td>".$row['anderes']."</td>";
				if(isset($_SESSION['opt_unterricht'])){
				echo"<td>";
					if($row['unterrichttaeg']==1)
						echo"täglich";
					if($row['unterrichtwoech']==1)
						echo"wöchentlich";
					if($row['unterrichtmonat']==1)
						echo"monatlich";
					if($row['unterrichtsporad']==1)
						echo"sporadisch";
					if($row['unterrichtnicht']==1)
						echo"kein";
					else
						echo"&nbsp;";
				echo"</td>";                                
				}
				if(isset($_SESSION['opt_nutzungveranstaltungen']))
					echo"<td>".$yesorno[$row['nutzungveranstaltungen']]."</td>";
					if(isset($_SESSION['opt_bundesland']))
					echo"<td>".$bundeslaender_detail[$row['bundesland']]."</td>";
	if(isset($_SESSION['opt_bibliothek_verw_prog']))
					echo"<td>".$row['name']." ".$row['version']."</td>";
	if(isset($_SESSION['opt_oeffentlichkeitsarbeit']))
        {            
                  $oeffentlichkeitsarbeit_text='';
                  $art_oeffentlichkeitsarbeit=json_decode($row['art_oeffentlichkeitsarbeit']);
        $array_keys_oeffentlichkeitsarbeit=array_keys(($art_oeffentlichkeitsarbeit));
        $last_key = end($array_keys_oeffentlichkeitsarbeit);
        
foreach( $art_oeffentlichkeitsarbeit as $key => $value ) {
   if ($key == $last_key) {
       if($value=='6')
     {
          if($andere_oeffentlichkeitsarbeit!='')
            $oeffentlichkeitsarbeit_text.=$andere_oeffentlichkeitsarbeit; 
     }
     else
     {
         $oeffentlichkeitsarbeit_text.=$oeffentlichkeitsarbeit[$value];
     }
     
     
  }else
  {
        if($value=='6')
     {
         if($andere_oeffentlichkeitsarbeit!='')
            $oeffentlichkeitsarbeit_text.=$andere_oeffentlichkeitsarbeit.", "; 
     }
     else
     {
         $oeffentlichkeitsarbeit_text.=$oeffentlichkeitsarbeit[$value].", ";
     }
     
  }
}
                    
					
					echo"<td>".substr($oeffentlichkeitsarbeit_text,0,40)."</td>";
        }
       
                                ?>	
		    </tr>
			<?php
                        
		}
	}
	else {
		echo $mysqli->error;
		die("Query failed");
	}
	?>
</table>
<a class="csv-exportbutton" href="csv_output.php" target="_blank" style="display:block;">&nbsp;</a>
<?php
include("footer.php");
?>