<?php
/**
* Buchzeit APS-Verwaltung
*
* @author Manuel Postlbauer
* @version 0.9
*/

/**
* profile
*
* Hier kann das Passwort für den aktuell angemeldeten Benutzer geändert werden
* Es wird ein neues Passwort eingeben und zur Bestätigung wird es noch ein zweites mal eingegeben.
* Danach werden beide Felder miteinander verglichen. Stimmen diese überein wird das neue Kennwort md5 verschlüsselt
* in der Datenbank gespeichert.
*
*/
session_start();

if(isset($_SESSION['LOGGEDIN'])){
	$displayloginregister=1;
}
if((!isset($_SESSION['LOGGEDIN']))||($_SESSION['ADMIN']!=1)){
	header("location: index.php");
	exit();
}

include("../localconf.php");
include("header.php");

if(isset($_POST['new']['submit']))
{
	$query = "INSERT INTO schuladressen SET schulkennzahl = '" .$mysqli->real_escape_string($_POST['new']['schulkennzahl']). "',
	                                        schultitel    = '" .$mysqli->real_escape_string($_POST['new']['schulname']). "',
											postleitzahl  = '" .$mysqli->real_escape_string($_POST['new']['postleitzahl']). "',
											ort           = '" .$mysqli->real_escape_string($_POST['new']['ort']). "',
											schulart      = '" .(int)$_POST['new']['schultyp']. "',
											password      = '" .md5($_POST['new']['postleitzahl']). "'";
											
	$mysqli->query($query);
	
	?>
    
    <div style="width: 100%; background-color: #FFC; border: solid 1px #666">Schule erfolgreich hinzugefügt</div>
    
    <?php
}
?>
<h1>Neue Schule anlegen</h1>

<form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post">

<table width="950" cellpadding="3" cellspacing="1" border="0">
<tr>
	<td width="200">Schulname</td>
    <td><input name="new[schulname]" type="text" size="30" /></td>
</tr>
<tr>
	<td>Schulkennzahl</td>
    <td><input name="new[schulkennzahl]" type="text" size="30" /></td>
</tr>
<tr>
	<td>Postleitzahl ( = Passwort)</td>
    <td><input name="new[postleitzahl]" type="text" size="30" /></td>
</tr>
<tr>
	<td>Ort</td>
    <td><input name="new[ort]" type="text" size="30" /></td>
</tr>
<tr>
	<td>Schultyp</td>
    <td>
    <select name="new[schultyp]">
    <?php
	foreach($schultypen2 as $key => $schultyp)
	{
		echo "<option value='" .$key. "'>" .$schultyp. "</option>\n";
	}
	?>
    </select>
    </td>
</tr>
<tr>
	<td>&nbsp;</td>
    <td><input name="new[submit]" type="submit" value="Schule anlegen"</td>
</tr>
</table>

</form>

<?php
include("footer.php");
?>