<?php
/**
* Buchzeit APS-Verwaltung
*
* @author Manuel Postlbauer
* @version 0.9
*/

/**
* step 1 (Teil von formular.php)
*/
	if(!isset($_SESSION['LOGGEDIN'])){
		header("location: index.php");
		exit();
	}
	include("header.php");
	
	$_SESSION['stepcounter']=2;
?>
<form action="formular.php?step=7" method="post" onsubmit="return checkFields(1)">
	<h1>Daten editieren</h1>	
	<br/>
	<br/>
        <table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="481px">
            <tr>
                <th colspan="2"><h2>Stammdaten</h2>&nbsp;</th>
		</tr>
                
                            <tr>
                <th colspan="2"><h3>Bibliothek</h3>&nbsp;</th>
		</tr>
		<tr>
			<th colspan="2">Schulart&nbsp;</th>
		</tr>
		<tr class="listschooltablealtbg">
			<td colspan="2"><input type="radio" name="schooltype" value="1" id="schooltype0" <?php if((isset($_SESSION['schooltype'])) AND ($_SESSION['schooltype']=="1")) echo "checked"; ?>/> APS
			<input type="radio" name="schooltype" value="3" id="schooltype2" <?php if((isset($_SESSION['schooltype'])) AND ($_SESSION['schooltype']=="3")) echo "checked"; ?>/> BMHS
                        <input type="radio" name="schooltype" value="2" id="schooltype1" <?php if((isset($_SESSION['schooltype'])) AND ($_SESSION['schooltype']=="2")) echo "checked"; ?>/> AHS
                        </td>
		</tr>				
	
		<tr>
			<th colspan="2">Bibliotheksart</th>
		</tr>		
		<?php
		//Damit man nicht auf einmal sagen kann man hat keine Bibliothek mehr
		?>
		<tr class="listschooltablealtbg2" <?php if($_SESSION['GLOBAL_BIBLIOTHEK_ID']!=0) echo"style='display:none;'";?>>
			<td>Schulbibliothek:</td>
			<td><input type="checkbox" name="librarykind_0" value="1" id="librarykind_0" <?php if(!empty($_SESSION['librarykind_0'])) echo "checked"; ?>/></td>
		</tr>
		<tr class="listschooltablealtbg" <?php if($_SESSION['GLOBAL_BIBLIOTHEK_ID']!=0) echo"style='display:none;'";?>>
			<td>Kombination 2 oder mehrere Schulen:</td>
			<td><input type="checkbox" name="librarykind_1" value="1" id="librarykind_1" <?php if(!empty($_SESSION['librarykind_1'])) echo "checked"; ?>/></td>
		</tr>
		<tr class="listschooltablealtbg2" <?php if($_SESSION['GLOBAL_BIBLIOTHEK_ID']!=0) echo"style='display:none;'";?>>
			<td>Kombination mit öffentlicher Bibliothek:</td>
			<td><input type="checkbox" name="librarykind_2" value="1" id="librarykind_2" <?php if(!empty($_SESSION['librarykind_2'])) echo "checked"; ?>/></td>
		</tr>
                <tr class="listschooltablealtbg2" <?php if($_SESSION['GLOBAL_BIBLIOTHEK_ID']!=0) echo"style='display:none;'";?>>
			<td>Andere Varianten:</td>
			<td><input type="textbox" name="librarykind_3" id="librarykind_3" <?php if(!empty($_SESSION['librarykind_3'])) echo "value='".$_SESSION['librarykind_3']."'"; ?>/></td>
		</tr>
	
		<tr>
			<th colspan="2">Angaben zur Schule</th>
		</tr>
		<tr class="listschooltablealtbg">
			<td width="200px">Schulkennzahl:&nbsp;<span class="starcolor">*</span></td>
			<td><input type="text" name="school_id" id="school_id" size="45" value="<?php if(isset($_SESSION['school_id'])) echo $_SESSION['school_id'];?>"/></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Schulname:&nbsp;<span class="starcolor">*</span></td>
			<td><input type="text" name="school_name" id="school_name" size="45" value="<?php if(isset($_SESSION['school_name'])) echo $_SESSION['school_name'];?>"/></td>
		</tr>	
		<tr>
			<th colspan="2">Adresse</th>
		</tr>
		<tr class="listschooltablealtbg">
			<td width="200px">Postleitzahl:&nbsp;<span class="starcolor">*</span></td>
			<td><input type="text" name="address_plz" id="address_plz" size="45" value="<?php if(isset($_SESSION['address_plz'])) echo $_SESSION['address_plz'];?>"/></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Schulort:&nbsp;<span class="starcolor">*</span></td>
			<td><input type="text" name="address_school_loc" id="address_school_loc" size="45" value="<?php if(isset($_SESSION['address_school_loc'])) echo $_SESSION['address_school_loc'];?>"/></td>
		</tr>
                <tr class="listschooltablealtbg">                    
			<td width="200px">Bundesland:&nbsp;<span class="starcolor">*</span></td>
			<td>
                            <select name="bundesland" id="bundesland">
                                <option <?php if(isset($_SESSION['address_bundesland']) && empty($_SESSION['address_bundesland'])) echo 'selected';?> >Bitte auswählen</option>
  <option value="1" <?php if(isset($_SESSION['address_bundesland']) && $_SESSION['address_bundesland']=='1') echo 'selected';?>>Burgenland</option>
  <option value="2" <?php if(isset($_SESSION['address_bundesland']) && $_SESSION['address_bundesland']=='2') echo 'selected';?>>Kärnten</option>
  <option value="3" <?php if(isset($_SESSION['address_bundesland']) && $_SESSION['address_bundesland']=='3') echo 'selected';?>>Niederösterreich</option>
  <option value="4" <?php if(isset($_SESSION['address_bundesland']) && $_SESSION['address_bundesland']=='4') echo 'selected';?>>Oberösterreich</option>
  <option value="5" <?php if(isset($_SESSION['address_bundesland']) && $_SESSION['address_bundesland']=='5') echo 'selected';?>>Salzburg</option>
  <option value="6" <?php if(isset($_SESSION['address_bundesland']) && $_SESSION['address_bundesland']=='6') echo 'selected';?>>Steiermark</option>
  <option value="7" <?php if(isset($_SESSION['address_bundesland']) && $_SESSION['address_bundesland']=='7') echo 'selected';?>>Tirol</option>
  <option value="8" <?php if(isset($_SESSION['address_bundesland']) && $_SESSION['address_bundesland']=='8') echo 'selected';?>>Vorarlberg</option>
  <option value="9" <?php if(isset($_SESSION['address_bundesland']) && $_SESSION['address_bundesland']=='9') echo 'selected';?>>Wien</option>
                            </select>
                            
                            
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Straße:&nbsp;<span class="starcolor">*</span></td>
			<td><input type="text" name="address_street" id="address_street" size="45" value="<?php if(isset($_SESSION['address_street'])) echo $_SESSION['address_street'];?>"/></td>
		</tr>
                <tr class="listschooltablealtbg">
			<td>Telefon:&nbsp;<span class="starcolor">*</span></td>
			<td><input type="text" name="phone" id="phone" size="45" value="<?php if(isset($_SESSION['phone'])) echo $_SESSION['phone'];?>" /></td>
		</tr>
                <tr class="listschooltablealtbg2">
			<td>E-Mail-Adresse:&nbsp;<span class="starcolor">*</span></td>
			<td><input type="text" name="mail" id="mail" size="45" value="<?php if(isset($_SESSION['mail'])) echo $_SESSION['mail'];?>" /></td>
		</tr>
                <tr class="listschooltablealtbg2">
			<td>Homepage? - URL:</td>
			<td><input type="text" name="homepage" id="homepage" size="45" value="<?php if(isset($_SESSION['homepage'])) echo $_SESSION['homepage'];?>" /></td>
		</tr>
                <tr class="listschooltablealtbg">
			<td>WebOPAC? - URL:</td>
			<td><input type="text" name="webopac" id="webopac" size="45" value="<?php if(isset($_SESSION['webopac'])) echo $_SESSION['webopac'];?>" /></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td width="200px">Raumgröße (in m²):&nbsp;<span class="starcolor">*</span></td>
			<td><input type="text" name="roomsize" id="roomsize" value="<?php if(isset($_SESSION['roomsize'])) echo $_SESSION['roomsize']; ?>" /></td>
		</tr>                
		
		
	
		<tr>
			<th colspan="2">Bibliotheksverwaltungsprogramm</th>
		</tr>
		<tr class="listschooltablealtbg">
		<!-- Klickt man einen Radiobutton für eine Software im Formular an wird eine Javascriptfunktion aufgerufen welche die Eingabefelder für die zugehörige Software anzeigt und die anderen ausblendet-->
			<td colspan="2">
                            <input type="radio" name="programm" value="biblioweb" onClick="bibliotheksverwaltungsprogramm(4);"<?php if(!empty($_SESSION['isbiblioweb'])) echo "checked";?>/> BIBLIOWEB
				
				<input type="radio" name="programm" value="littera" onClick="bibliotheksverwaltungsprogramm(1);" <?php if(!empty($_SESSION['islittera'])) echo "checked";?>/> LITTERA
				<input type="radio" name="programm" value="anderes" onClick="bibliotheksverwaltungsprogramm(5);"<?php if(!empty($_SESSION['isanderes'])) echo "checked";?>/> ANDERE
			</td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td width="200px">
				<div id="verw1" style="display:none;">LITTERA (Version):</div>
				<div id="verw2" style="display:none;">EXLIBRIS (Version):</div>
				<div id="verw3" style="display:none;">BOND (Version):</div>
				<div id="verw4" style="display:none;">BIBLIOWEB (Version):</div>
				<div id="verw5" style="display:none;">Anderes (Name,Version):</div>
			</td>
			<td>
				<div id="verw1_1" style="display:none;"><input type="text" name="litteraver" id="litteraver" value="<?php if(isset($_SESSION['litteraver'])) echo $_SESSION['litteraver']; ?>"/></div>
				<div id="verw2_1" style="display:none;"><input type="text" name="exlibrisver" id="exlibrisver" value="<?php if(isset($_SESSION['exlibrisver'])) echo $_SESSION['exlibrisver']; ?>"/></div>
				<div id="verw3_1" style="display:none;"><input type="text" name="bondver" id="bondver" value="<?php if(isset($_SESSION['bondver'])) echo $_SESSION['bondver']; ?>"/></div>
				<div id="verw4_1" style="display:none;"><input type="text" name="bibliowebver" id="bibliowebver" value="<?php if(isset($_SESSION['bibliowebver'])) echo $_SESSION['bibliowebver']; ?>"/></div>
				<div id="verw5_1" style="display:none;"><input type="text" name="anderesname" id="anderesname" value="<?php if(isset($_SESSION['anderesname'])) echo $_SESSION['anderesname']; ?>"/><input type="text" name="anderesver" id="anderesver" value="<?php if(isset($_SESSION['anderesver'])) echo $_SESSION['anderesver']; ?>"/></div>
			</td>
		</tr>
		<?php
		//Wenn unter formular.php als Name in der Tabelle für die Bibliothekssoftware Littera, Exlibris, Bond, oder der Name einer eigenen Software ausgelesen wird,
		//wird die zum Namen gehörige Version in der jeweilgen Sessionvariable gespeichert.. Das heißt diese ist nicht leer. Z.B. Name ist Exlibris dann wird die Version im Feld version in der Datenbank in die
		//Sessionvariable exlibrisver gespeichert, d.h diese ist gesetzt. Ist es eine andere Software, wird eine andere Sessionvariable für die Version gesetzt. Danach wird die Javascriptfunktion aufgerufen
		//welche die Felder für die zugehörige Software auf der Webseite anzeigt und die anderen ausblendet.
			if(!empty($_SESSION['islittera']))
				echo"<SCRIPT LANGUAGE='javascript'>bibliotheksverwaltungsprogramm(1);</SCRIPT>";
			if(!empty($_SESSION['isexlibris']))
				echo"<SCRIPT LANGUAGE='javascript'>bibliotheksverwaltungsprogramm(2);</SCRIPT>";
			if(!empty($_SESSION['isbond']))
				echo"<SCRIPT LANGUAGE='javascript'>bibliotheksverwaltungsprogramm(3);</SCRIPT>";
			if(!empty($_SESSION['isbiblioweb']))
				echo"<SCRIPT LANGUAGE='javascript'>bibliotheksverwaltungsprogramm(4);</SCRIPT>";
			if(!empty($_SESSION['isanderes']))
				echo"<SCRIPT LANGUAGE='javascript'>bibliotheksverwaltungsprogramm(5);</SCRIPT>";
		?>
	</table>
	
	
	<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="481px">
		<tr>
                    <th colspan="2"><h3>MitarbeiterInnen und Mitarbeiter (inkl. LeiterIn)&nbsp;</h3></th>
		</tr>	
                <tr>
			<th colspan="2">Ansprechperson f. d. SB mit abgeschl. Ausbildung:</th>
		</tr>
		<tr class="listschooltablealtbg">
			<td width="200px">Name:&nbsp;<span class="starcolor">*</span></td>
			<td><input type="text" name="contact_name" id="contact_name" size="45" value="<?php if(isset($_SESSION['contact_name'])) echo $_SESSION['contact_name'];?>" /></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>E-Mail-Adresse:&nbsp;<span class="starcolor">*</span></td>
			<td><input type="text" name="contact_mail" id="contact_mail" size="45" value="<?php if(isset($_SESSION['contact_mail'])) echo $_SESSION['contact_mail'];?>" /></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Telefon:&nbsp;<span class="starcolor">*</span></td>
			<td><input type="text" name="contact_phone" id="contact_phone" size="45" value="<?php if(isset($_SESSION['contact_phone'])) echo $_SESSION['contact_phone'];?>"/></td>
		</tr>	
                <tr>
			<th colspan="2">Anzahl der MitarbeiterInnen /KollegInnen:&nbsp;</th>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>mit abgeschlossener Ausbildung:&nbsp;<span class="starcolor">*</span></td>
			<td><input type="text" name="anz_ma_mit_abg_ausbildung" id="anz_ma_mit_abg_ausbildung" size="10" value="<?php if(isset($_SESSION['anz_ma_mit_abg_ausbildung'])) echo $_SESSION['anz_ma_mit_abg_ausbildung'];?>"/></td>
		</tr>
                <tr class="listschooltablealtbg">
			<td>ohne abgeschlossener Ausbildung:&nbsp;<span class="starcolor">*</span></td>
			<td><input type="text" name="anz_ma_ohne_abg_ausbildung" id="anz_ma_ohne_abg_ausbildung" size="10" value="<?php if(isset($_SESSION['anz_ma_ohne_abg_ausbildung'])) echo $_SESSION['anz_ma_ohne_abg_ausbildung'];?>"/></td>
		</tr>
                </table>
	
	
	<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="481px">
                <tr>
                    <th colspan="2"><h3>Medienbestand / Entlehnungen:&nbsp;</h3></th>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Medienzugang im Kalenderjahr:&nbsp;<span class="starcolor">*</span></td>
			<td><input type="text" name="medienzugang" id="medienzugang" size="10" value="<?php if(isset($_SESSION['medienzugang'])) echo $_SESSION['medienzugang'];?>"/></td>
		</tr>
                <tr class="listschooltablealtbg2">
			<td>Medienabgang im Kalenderjahr:&nbsp;<span class="starcolor">*</span></td>
			<td><input type="text" name="medienabgang" id="medienabgang" size="10" value="<?php if(isset($_SESSION['medienabgang'])) echo $_SESSION['medienabgang'];?>"/></td>
		</tr>
                
	</table>
        <table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="481px">
                <tr>
                    <th colspan="2"><h3>Benutzer und Benutzerinnen:&nbsp;</h3></th>
		</tr>
		<tr class="listschooltablealtbg">
			<td>&nbsp;</td>
			<td>LeserInnen (mind. 1 Entlehnung)</td>
		</tr>
                <tr class="listschooltablealtbg2">
			<td>SchülerInnen:&nbsp;<span class="starcolor">*</span></td>
			<td><input type="text" name="anz_schueler" id="anz_schueler" size="10" value="<?php if(isset($_SESSION['anz_schueler'])) echo $_SESSION['anz_schueler'];?>"/></td>
		</tr>
                <tr class="listschooltablealtbg2">
			<td>Erwachsene, im Schulbetrieb tätige, Personen:&nbsp;<span class="starcolor">*</span></td>
			<td><input type="text" name="anz_erw_in_schulbetrieb" id="anz_erw_in_schulbetrieb" size="10" value="<?php if(isset($_SESSION['anz_erw_in_schulbetrieb'])) echo $_SESSION['anz_erw_in_schulbetrieb'];?>"/></td>
		</tr>
                <tr class="listschooltablealtbg2">
			<td>externe LeserInnen bei:&nbsp;<span class="starcolor">*</span></td>
			<td><input type="text" name="ext_leser" id="ext_leser" size="10" value="<?php if(isset($_SESSION['ext_leser'])) echo $_SESSION['ext_leser'];?>"/></td>
		</tr>
                <tr class="listschooltablealtbg2">
			<td>komb. Bibliotheken:&nbsp;<span class="starcolor">*</span></td>
			<td><input type="text" name="kom_bibliotheken" id="kom_bibliotheken" size="10" value="<?php if(isset($_SESSION['kom_bibliotheken'])) echo $_SESSION['kom_bibliotheken'];?>"/></td>
		</tr>                
                
	</table>
         <table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="481px">
                <tr>
                    <th colspan="2"><h3>Veranstaltungen und Aktivitäten:&nbsp;</h3></th>
		</tr>
		<tr class="listschooltablealtbg">
			<td>&nbsp;</td>
			<td>Anzahl der statt-gefundenen VA</td>
		</tr>
                <tr class="listschooltablealtbg2">
			<td>Lesungen:&nbsp;<span class="starcolor">*</span></td>
			<td><input type="text" name="anz_lesungen" id="anz_lesungen" oninput="calculatesum(this);" size="10" value="<?php if(isset($_SESSION['anz_lesungen'])) echo $_SESSION['anz_lesungen'];?>"/></td>
		</tr>
                <tr class="listschooltablealtbg2">
			<td>Workshops Projekte:&nbsp;<span class="starcolor">*</span></td>
			<td><input type="text" name="anz_wokshops" id="anz_wokshops"oninput="calculatesum(this);" size="10" value="<?php if(isset($_SESSION['anz_wokshops'])) echo $_SESSION['anz_wokshops'];?>"/></td>
		</tr>
                <tr class="listschooltablealtbg2">
			<td>Bibliotheksdidaktische Schulungen:&nbsp;<span class="starcolor">*</span></td>
			<td><input type="text" name="anz_bibliotheks_schulungen"  id="anz_bibliotheks_schulungen" size="10" oninput="calculatesum(this);" value="<?php if(isset($_SESSION['anz_bibliotheks_schulungen'])) echo $_SESSION['anz_bibliotheks_schulungen'];?>"/></td>
		</tr>
                <tr class="listschooltablealtbg2">
			<td>Sonstige Veranstaltungen:&nbsp;<span class="starcolor">*</span></td>
			<td><input type="text" name="anz_sonstige_va"  id="anz_sonstige_va" size="10" oninput="calculatesum(this);" value="<?php if(isset($_SESSION['anz_sonstige_va'])) echo $_SESSION['anz_sonstige_va'];?>"/></td>
		</tr> 
                <tr class="listschooltablealtbg2">
			<td>Gesamt:&nbsp;<span class="starcolor">*</span></td>
                        <td ><input type="text" name="sum_va" value="0" id="sum_va" size="10" value="0" disabled="1"/></td>
		</tr>
                
	</table>
        <?php
       
				echo"<SCRIPT LANGUAGE='javascript'>calculatesum();</SCRIPT>"; ?>
         <table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="481px">
                <tr>
                    <th colspan="2"><h3>Öffentlichkeitsarbeit:&nbsp;</h3></th>
		</tr>
                <tr>
                    <td colspan="2"><h4>In welcher Form betreiben Sie Öffentlichkeitsarbeit? (Zutreffendes bitte ankreuzen!):&nbsp;</h4></td>
		</tr>                
                <tr class="listschooltablealtbg">
					<td>eigene Website der Schulbibliothek</td>
					<td><input type="checkbox" name="art_oeffentlichkeitsarbeit[]" value="1" <?php if(!empty($_SESSION['art_oeffentlichkeitsarbeit']) && in_array('1',$_SESSION['art_oeffentlichkeitsarbeit'])) echo "checked"; ?> /></td>
			</tr>
			<tr class="listschooltablealtbg2">
					<td>Soziale Medien (z.B. Facebook, Twitter etc.)</td>
					<td><input type="checkbox" name="art_oeffentlichkeitsarbeit[]" value="2" <?php if(!empty($_SESSION['art_oeffentlichkeitsarbeit']) && in_array('2',$_SESSION['art_oeffentlichkeitsarbeit'])) echo "checked"; ?> /></td>
			</tr>
			<tr class="listschooltablealtbg">
					<td>im Rahmen der Schulwebsite</td>
					<td><input type="checkbox" name="art_oeffentlichkeitsarbeit[]" value="3" <?php if(!empty($_SESSION['art_oeffentlichkeitsarbeit']) && in_array('3',$_SESSION['art_oeffentlichkeitsarbeit'])) echo "checked"; ?> /></td>
			</tr>
                         <tr class="listschooltablealtbg2">
					<td>Plakate, Folder, Flugblätter:</td>
					<td><input type="checkbox" name="art_oeffentlichkeitsarbeit[]" value="4" <?php if(!empty($_SESSION['art_oeffentlichkeitsarbeit']) && in_array('4',$_SESSION['art_oeffentlichkeitsarbeit'])) echo "checked"; ?> /></td>
			</tr>
			<tr class="listschooltablealtbg">
					<td>Beitrag im Jahresbericht der Schule:</td>
					<td><input type="checkbox" name="art_oeffentlichkeitsarbeit[]" value="5" <?php if(!empty($_SESSION['art_oeffentlichkeitsarbeit']) && in_array('5',$_SESSION['art_oeffentlichkeitsarbeit'])) echo "checked"; ?> /></td>
			</tr>
			<tr class="listschooltablealtbg">
					<td>Andere Varianten </td>
					<td><input type="checkbox" name="art_oeffentlichkeitsarbeit[]" value="6" onClick="andereoeffentlichkeitsarbeit(this);" <?php if(!empty($_SESSION['art_oeffentlichkeitsarbeit']) && in_array('6',$_SESSION['art_oeffentlichkeitsarbeit'])) echo "checked"; ?> /></td>
			</tr>
                        <tr class="listschooltablealtbg2">	
                            <td>
				Andere Varianten:
				</td>
			<td>
				<div id="andere_oeffentlichkeitsarbeit_div" <?php if((!in_array('6',$_SESSION['art_oeffentlichkeitsarbeit']))) echo 'style="display:none;"';?>><input type="text" name="andere_oeffentlichkeitsarbeit" id="andere_oeffentlichkeitsarbeit" value="<?php if(isset($_SESSION['andere_oeffentlichkeitsarbeit'])) echo $_SESSION['andere_oeffentlichkeitsarbeit']; ?>"/></div>
				</td>
		</tr>
		<?php
		//Wenn unter formular.php als Name in der Tabelle für die Bibliothekssoftware Littera, Exlibris, Bond, oder der Name einer eigenen Software ausgelesen wird,
		//wird die zum Namen gehörige Version in der jeweilgen Sessionvariable gespeichert.. Das heißt diese ist nicht leer. Z.B. Name ist Exlibris dann wird die Version im Feld version in der Datenbank in die
		//Sessionvariable exlibrisver gespeichert, d.h diese ist gesetzt. Ist es eine andere Software, wird eine andere Sessionvariable für die Version gesetzt. Danach wird die Javascriptfunktion aufgerufen
		//welche die Felder für die zugehörige Software auf der Webseite anzeigt und die anderen ausblendet.
			if(!empty($_SESSION['art_oeffentlichkeitsarbeit']) && in_array('6',$_SESSION['art_oeffentlichkeitsarbeit']))
				echo"<SCRIPT LANGUAGE='javascript'>andere_oeffentlichkeitsarbeit();</SCRIPT>";
			
		?>
                        
                	
		
                
	</table>
	<input type="submit" value="" class="weiterbutton"/>
	<p><span class="starcolor">*</span> Pflichtfelder bitte korrekt ausfüllen</p>
</form>
<?php
include("footer.php");
?>