<?php
//Datenbankzugangsdaten					 
$host = "localhost";
$user = "bibliabh";
$pass = "db4SB3";
$db   = "usrdb_bibliabh_sb";

// create PDO database object
$dbh = new PDO("mysql:host=$host;dbname=$db", $user, $pass);
$dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );

// read import file
$file = file("AHS-Liste.csv");

// prepare statement to import data rows
$stmnt = $dbh->prepare("INSERT INTO schuladressen SET schulkennzahl = :skz, postleitzahl = :postleitzahl, schultitel = :schultitel, ort = :ort, strasse_hausnummer = :strasse_hausnummer, schulart = 5");

$i = 0;

foreach($file as $row)
{
	if($i > 0)
	{
		$data = explode(";", $row);
		$stmnt->bindValue(":skz",                $data[0], PDO::PARAM_INT);
		$stmnt->bindValue(":postleitzahl",       $data[1], PDO::PARAM_INT);
		$stmnt->bindValue(":schultitel",         $data[2], PDO::PARAM_STR);
		$stmnt->bindValue(":ort",                $data[5], PDO::PARAM_STR);
		$stmnt->bindValue(":strasse_hausnummer", $data[6], PDO::PARAM_STR);
		
		$stmnt->execute();
	}
	$i++;
}

// Datenbankverbindung trennen
$dbh = NULL;
?>