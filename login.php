<?php 
error_reporting(E_ERROR | E_PARSE);
/**
* Buchzeit APS-Verwaltung
*
* @author Manuel Postlbauer
* @version 0.9
*/

/**
* login
*
* Der Benutzer gibt �ber ein Formular Benutzername und Passwort ein. Diese Daten werden
* mit den Eintr�gen in der Datenbank verglichen. Sind diese valide, wird die Sessionvariable "LOGGEDIN" auf 1 gesetzt, der
* Benutzer ist somit eingeloggt und hat Zugriff auf den gesch�tzten Bereich
*
* @param User, Passwort �ber Formulareingabe
*/

//Beim klicken des Logoutbuttons auf der Internetseite wird man auf die Loginseite weitergeleitet. Durch session_start(); session_destroy(); session_start(); erfolgt der Logout
//und es wird gleich darauf eine Neue Anfrage f�r den Login vorbereitet.
session_start();
session_destroy();
session_start();
//Variable f�r das Usermen�, d.h ist der Benutzer eingeloggt wird ein zus�tzliches Men� oben auf der Seite angezeigt.  HOME | DATEN EDITIEREN | PROFIL | LOGOUT , dieses Men� wird
//nur angezeigt wenn die Variable auf 1 gesetzt ist.
$displayloginregister=0;

if(isset($_SESSION['LOGGEDIN'])){
	$displayloginregister=1;
}

if(isset($_POST['Submit']))
{
	//Datenbankverbindung aufbauen
	include("localconf.php");
	
	$error;
	$login = $mysqli->real_escape_string($_POST['login']);
	$password = $mysqli->real_escape_string($_POST['password']);

	if(($login == '')||($password == '')){
		$error='Benutzername oder Passwort fehlt';
	}
	//Gibt es keinen Fehler
	if(!$error){
		//Vergleiche Benutzername und Kennwort mit den Eintr�gen in der DB
		$qry="SELECT * FROM schuladressen WHERE schulkennzahl='".$login."' AND password='".md5($_POST['password'])."'";
		$result=$mysqli->query($qry);

		if($result) {
			if(mysqli_num_rows($result) == 1) {
				//Login erfolgreich
				$member = $result->fetch_assoc();
				//LOGGEDIN wird auf 1 gesetzt, der Benutzer ist erfolgreich eingeloggt.
				$_SESSION['LOGGEDIN'] = 1;
				//Der Prim�rschl�ssel der Tabelle schuladressen wird zur USERID des eingeloggten Benutzers.
				$_SESSION['USERID'] = $member['id'];
				//Weiterleitung auf den Mitgliedsbereich
				header("location: index_member.php");
				exit();
			}else {
				$error='Ungültiger Benutzername oder ungültiges Passwort eingegeben';
			}
		}else {
			die("Query failed");
		}
	}
}
//Header des Seitendesigns
include("header.php");
?>
<script>
document.write("<h1>Login</h1>");
document.write("<form id='login' name='login' method='post' action='<?php echo $PHP_SELF ?>'>");
document.write("	  <table class='listschooltable' width='400' border='0' cellpadding='5' cellspacing='1'>");
document.write("		<tr>");
document.write("			<th colspan='2'>Login</th>");
document.write("		<tr>");
document.write("		<tr class='listschooltablealtbg'>");
document.write("		  <td width='120'>Benutzername:</td>");
document.write("		  <td width='280'><input name='login' type='text' class='textfield' id='login' /></td>");
document.write("		</tr>");
document.write("		<tr class='listschooltablealtbg2'>");
document.write("		  <td>Kennwort:</td>");
document.write("		  <td><input name='password' type='password' class='textfield' id='password' /></td>");
document.write("		</tr>");
document.write("		<tr class='listschooltablealtbg'>");
document.write("		  <td>&nbsp;</td>");
document.write("		  <td><input type='submit' name='Submit' value='' class='loginbutton'/></td>");
document.write("		</tr>");
document.write("	  </table>");
document.write("	  <div class='important-text'>");
document.write("			<?php echo $error; ?>");
document.write("	  </div>");
document.write("	  <p>Falls sie Ihr Kennwort vergessen haben, dann können Sie&nbsp;<a href='password_recover.php'>hier</a>&nbsp;ein Neues anfordern</p>");
document.write("	</form>");
</script>
<noscript>
<p>Sie müssen JavaScript aktivieren, um diese Seite nutzen zu können!</p>
</noscript>
<?php
//Footer des Seitendesigns
include("footer.php");
?>