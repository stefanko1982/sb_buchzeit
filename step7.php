<?php
/**
* Buchzeit APS-Verwaltung
*
* @author Manuel Postlbauer
* @version 0.9
*/

/**
* step 7 (Teil von formular.php)
*/
if(!isset($_SESSION['LOGGEDIN'])){
	header("location: index.php");
	exit();
}

include("localconf.php");
if($_SESSION['stepcounter']==2){
    $_SESSION['school_id']=$_POST['school_id'];
$_SESSION['school_name']=$_POST['school_name'];
$_SESSION['address_plz']=$_POST['address_plz'];
$_SESSION['bundesland']=$_POST['bundesland'];
$_SESSION['address_school_loc']=$_POST['address_school_loc'];
$_SESSION['address_street']=$_POST['address_street'];
$_SESSION['schooltype']=$_POST['schooltype'];
//$_SESSION['gemeinde']=$_POST['gemeinde'];
//$_SESSION['schulerhalter']=$_POST['schulerhalter'];


//$oeffentlichkeitsarbeit
        $_SESSION['art_oeffentlichkeitsarbeit']=$_POST['art_oeffentlichkeitsarbeit'];
        $_SESSION['andere_oeffentlichkeitsarbeit']=$_POST['andere_oeffentlichkeitsarbeit'];
        $oeffentlichkeitsarbeit_text='';
        $array_keys_oeffentlichkeitsarbeit=array_keys($_SESSION['art_oeffentlichkeitsarbeit']);
        $last_key = end($array_keys_oeffentlichkeitsarbeit);
        
foreach( $_SESSION['art_oeffentlichkeitsarbeit'] as $key => $value ) {
   if ($key == $last_key) {
       if($value=='6')
     {
          if($_SESSION['andere_oeffentlichkeitsarbeit']!='')
            $oeffentlichkeitsarbeit_text.=$_SESSION['andere_oeffentlichkeitsarbeit']; 
     }
     else
     {
         $oeffentlichkeitsarbeit_text.=$oeffentlichkeitsarbeit[$value];
     }
     
     
  }else
  {
        if($value=='6')
     {
         if($_SESSION['andere_oeffentlichkeitsarbeit']!='')
            $oeffentlichkeitsarbeit_text.=$_SESSION['andere_oeffentlichkeitsarbeit']."<br /> "; 
     }
     else
     {
         $oeffentlichkeitsarbeit_text.=$oeffentlichkeitsarbeit[$value]."<br /> ";
     }
     
  }
   
  //print "The name is ".$name." and email is ".$email[$key].", thank you\n";
}
$_SESSION['anz_lesungen']=$_POST['anz_lesungen'];
$_SESSION['anz_wokshops']=$_POST['anz_wokshops'];
$_SESSION['anz_bibliotheks_schulungen']=$_POST['anz_bibliotheks_schulungen'];
$_SESSION['anz_sonstige_va']=$_POST['anz_sonstige_va'];


      $_SESSION['medienzugang']=$_POST['medienzugang'];
                                        $_SESSION['medienabgang']=$_POST['medienabgang'];
                                        $_SESSION['anz_schueler']=$_POST['anz_schueler'];
                                        $_SESSION['anz_erw_in_schulbetrieb']=$_POST['anz_erw_in_schulbetrieb'];
                                        $_SESSION['ext_leser']=$_POST['ext_leser'];
                                        $_SESSION['kom_bibliotheken']=$_POST['kom_bibliotheken'];
                                        $_SESSION['anz_lesungen']=$_POST['anz_lesungen'];
                                        $_SESSION['anz_wokshops']=$_POST['anz_wokshops'];
                                        $_SESSION['anz_bibliotheks_schulungen']=$_POST['anz_bibliotheks_schulungen'];
                                        $_SESSION['anz_sonstige_va']=$_POST['anz_sonstige_va'];
                                        $_SESSION['anz_ma_mit_abg_ausbildung']=$_POST['anz_ma_mit_abg_ausbildung'];
                                        $_SESSION['anz_ma_ohne_abg_ausbildung']=$_POST['anz_ma_ohne_abg_ausbildung'];
                                        //$_SESSION['art_oeffentlichkeitsarbeit']=$resultarray['art_oeffentlichkeitsarbeit'];
                                        //$_SESSION['andere_oeffentlichkeitsarbeit']=$resultarray['andere_oeffentlichkeitsarbeit'];


if((!isset($_POST['librarykind_0']))&&(!isset($_POST['librarykind_1']))&&(!isset($_POST['librarykind_2']))&&(!isset($_POST['librarykind_3']))) {
		//Falls der Benutzer vorher sagt er hat Bibliothek, dann zurückgeht anhakelt das er keine hat und wieder vorwärts
		//geht hat er trotzdem eine. Die 3 folgenden unset verhindern das.
		unset($_SESSION['librarykind_0']);
		unset($_SESSION['librarykind_1']);
		unset($_SESSION['librarykind_2']);
                unset($_SESSION['librarykind_3']);
		//Diese Variable wird benötig für die Kontrolle der Daten bei Step 7
		//Ist nämlich keine Bibliothek vorhanden, brauchen die restlichen Formularfelder bei der Gesamtansicht der eingegebenen Formulardaten
		//nicht angezeigt werden
		$_SESSION['leermeldung']=1;
		$_SESSION['classroomcount']=$_POST['classroomcount'];
		header("Location: formular.php?step=7");
		exit(); 
	}
	$_SESSION['leermeldung']=0;
	//$_SESSION['classroomcount']=$_POST['classroomcount'];
	if(!empty($_POST['librarykind_0']))
        $_SESSION['librarykind_0']=$_POST['librarykind_0'];
        if(!empty($_POST['librarykind_1']))
	$_SESSION['librarykind_1']=$_POST['librarykind_1'];
        if(!empty($_POST['librarykind_2']))
	$_SESSION['librarykind_2']=$_POST['librarykind_2'];
        if(!empty($_POST['librarykind_3']))
        $_SESSION['librarykind_3']=$_POST['librarykind_3'];
        $_SESSION['roomsize']=$_POST['roomsize'];
	//$_SESSION['zentral']=$_POST['zentral'];
	//$_SESSION['edvsaele']=$_POST['edvsaele'];
	//$_SESSION['schwerzuerr']=$_POST['schwerzuerr'];
	//$_SESSION['arbeitsplatzanzahl']=$_POST['arbeitsplatzanzahl'];
	//$_SESSION['internetarbeits']=$_POST['internetarbeits'];
	//$_SESSION['tvgeraet']=$_POST['tvgeraet'];
	//$_SESSION['beamer']=$_POST['beamer'];
	//$_SESSION['printer']=$_POST['printer'];
	//$_SESSION['scanner']=$_POST['scanner'];
	//$_SESSION['hoerbuchstation']=$_POST['hoerbuchstation'];
	//$_SESSION['kopierger']=$_POST['kopierger'];    
        $_SESSION['address_bundesland']=$_POST['bundesland'];        
	if($_POST['programm']=="littera"){
		$_SESSION['islittera']=1;
		unset($_SESSION['isexlibris']);
		unset($_SESSION['isbond']);
		unset($_SESSION['isbiblioweb']);
		unset($_SESSION['isanderes']);
	}
	if($_POST['programm']=="exlibris"){
		$_SESSION['isexlibris']=1;
		unset($_SESSION['islittera']);
		unset($_SESSION['isbond']);
		unset($_SESSION['isbiblioweb']);
		unset($_SESSION['isanderes']);
	}
	if($_POST['programm']=="bond"){
		$_SESSION['isbond']=1;
		unset($_SESSION['isexlibris']);
		unset($_SESSION['islittera']);
		unset($_SESSION['isbiblioweb']);
		unset($_SESSION['isanderes']);
	}
	if($_POST['programm']=="biblioweb"){
		$_SESSION['isbiblioweb']=1;
		unset($_SESSION['isexlibris']);
		unset($_SESSION['isbond']);
		unset($_SESSION['islittera']);
		unset($_SESSION['isanderes']);
	}
	if($_POST['programm']=="anderes"){
		$_SESSION['isanderes']=1;
		unset($_SESSION['isexlibris']);
		unset($_SESSION['isbond']);
		unset($_SESSION['isbiblioweb']);
		unset($_SESSION['islittera']);
	}
	$_SESSION['litteraver']=$_POST['litteraver'];
	$_SESSION['exlibrisver']=$_POST['exlibrisver'];
	$_SESSION['bondver']=$_POST['bondver'];
	$_SESSION['bibliowebver']=$_POST['bibliowebver'];
	$_SESSION['anderesname']=$_POST['anderesname'];
	$_SESSION['anderesver']=$_POST['anderesver'];
        
        /*$_SESSION['belletristik']=$_POST['belletristik'];
	$_SESSION['zeitschriften']=$_POST['zeitschriften'];
	$_SESSION['tageszeitungen']=$_POST['tageszeitungen'];
	$_SESSION['cddvd']=$_POST['cddvd'];
	$_SESSION['downloadang']=$_POST['downloadang'];
	$_SESSION['videos']=$_POST['videos'];
	$_SESSION['spiele']=$_POST['spiele'];
	$_SESSION['mehrfachtitel']=$_POST['mehrfachtitel'];
	$_SESSION['exemplareprotitel']=$_POST['exemplareprotitel'];
        
        $_SESSION['oeffnungstunden']=$_POST['oeffnungstunden'];*/
	$_SESSION['webopac']=$_POST['webopac'];
	$_SESSION['homepage']=$_POST['homepage'];
	$_SESSION['mail']=$_POST['mail'];
	$_SESSION['phone']=$_POST['phone'];
	/*$_SESSION['bibliothekaranzahl']=$_POST['bibliothekaranzahl'];
	$_SESSION['abgwochenstunden']=$_POST['abgwochenstunden'];
	$_SESSION['mitarbeitschueler']=$_POST['mitarbeitschueler'];
	$_SESSION['mitarbeiteltern']=$_POST['mitarbeiteltern'];
	$_SESSION['ankaufsbudget']=$_POST['ankaufsbudget'];
        
	$_SESSION['usage_reading']=$_POST['usage_reading'];
	$_SESSION['usage_learning']=$_POST['usage_learning'];
	$_SESSION['usage_media']=$_POST['usage_media'];
	$_SESSION['usage_other']=$_POST['usage_other'];
	$_SESSION['dailyclass']=$_POST['dailyclass'];
	$_SESSION['weeklyclass']=$_POST['weeklyclass'];
	$_SESSION['monthlyclass']=$_POST['monthlyclass'];
	$_SESSION['sporadicclass']=$_POST['sporadicclass'];
	$_SESSION['noclass']=$_POST['noclass'];
	$_SESSION['outsideclass']=$_POST['outsideclass'];
	$_SESSION['comments_pos']=$_POST['comments_pos'];
	$_SESSION['comments_neg']=$_POST['comments_neg'];*/
	$_SESSION['contact_name']=$_POST['contact_name'];
	$_SESSION['contact_mail']=$_POST['contact_mail'];
	$_SESSION['contact_phone']=$_POST['contact_phone'];
	/*$_SESSION['contact_fax']=$_POST['contact_fax'];
	$_SESSION['contact_name2']=$_POST['contact_name2'];
	$_SESSION['contact_mail2']=$_POST['contact_mail2'];
	$_SESSION['contact_phone2']=$_POST['contact_phone2'];
	$_SESSION['contact_fax2']=$_POST['contact_fax2'];*/

	$_SESSION['stepcounter']=2;
}
else{
	$_SESSION['stepcounter']=1;
}
include("header.php");
?>
<form action="formular.php?step=8" method="post">
	<h1>Daten editieren</h1>	
	
	<div class="important-text">
		<p>Überprüfen Sie Ihre Eingaben bitte nochmal auf Korrektheit. Speichern nicht vergessen.</p>
	</div>
	
	
	<div style="border-bottom:1px solid #8A93A2; width:800px; height:1px; margin-bottom:20px;"></div>
	<div style="float:left;">
            <table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="400px">
			<tr>
				<th colspan="2">Bibliotheksart</th>
			</tr>
			<tr class="listschooltablealtbg2">
				<td>Schulbibliothek:</td>
				<td><?php if(!empty($_SESSION['librarykind_0'])) echo "Ja"; else echo "Nein"; ?></td>
			</tr>
			<tr class="listschooltablealtbg">
				<td>Kombination 2 oder mehrere Schulen:</td>
				<td><?php if(!empty($_SESSION['librarykind_1'])) echo "Ja"; else echo "Nein"; ?></td>
			</tr>
			<tr class="listschooltablealtbg2">
				<td>Kombination mit öffentlicher Bibliothek:</td>
				<td><?php if(!empty($_SESSION['librarykind_2'])) echo "Ja"; else echo "Nein"; ?></td>
			</tr>
                        <tr class="listschooltablealtbg2">
				<td>Andere Variante:</td>
				<td><?php if(!empty($_SESSION['librarykind_3'])) echo $_SESSION['librarykind_3']; ?></td>
			</tr>
		</table>
		<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="400px">
			<tr>
				<th colspan="2">Angaben zur Schule</th>
			</tr>
                        <tr class="listschooltablealtbg">
				<td>Schulart:</td>
				<td><?php if((isset($_SESSION['schooltype']))) echo $schultypen[$_SESSION['schooltype']]; ?></td>
			</tr>                        
			<tr class="listschooltablealtbg2">
				<td>Schulkennzahl:&nbsp;<span class="starcolor">*</span></td>
				<td><?php if(isset($_SESSION['school_id'])) echo strip_tags($_SESSION['school_id']); ?></td>
			</tr>
			<tr class="listschooltablealtbg">
				<td>Schulname:&nbsp;<span class="starcolor">*</span></td>
				<td><?php if(isset($_SESSION['school_name'])) echo strip_tags($_SESSION['school_name']); ?></td>
			</tr>
		</table>
		
		<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="400px">
			<tr>
				<th colspan="2">Adresse</th>
			</tr>
			<tr class="listschooltablealtbg">
				<td>Postleitzahl:&nbsp;<span class="starcolor">*</span></td>
				<td><?php if(isset($_SESSION['address_plz'])) echo strip_tags($_SESSION['address_plz']); ?></td>
			</tr>
                        <tr class="listschooltablealtbg2">
				<td>Schulort:&nbsp;<span class="starcolor">*</span></td>
				<td><?php if(isset($_SESSION['address_school_loc'])) echo strip_tags($_SESSION['address_school_loc']); ?></td>
			</tr>
                        <tr class="listschooltablealtbg">
				<td>Bundesland:&nbsp;<span class="starcolor">*</span></td>
				<td><?php if(isset($_SESSION['address_bundesland'])) echo strip_tags($bundeslaender_detail[$_SESSION['address_bundesland']]); ?></td>
			</tr>  
			<tr class="listschooltablealtbg">
				<td>Straße:&nbsp;<span class="starcolor">*</span></td>
				<td><?php if(isset($_SESSION['address_street'])) echo strip_tags($_SESSION['address_street']); ?></td>
			</tr>
                        
                        <tr class="listschooltablealtbg2">
						<td>Telefon:&nbsp;<span class="starcolor">*</span></td>
						<td><?php if(isset($_SESSION['phone'])) echo strip_tags($_SESSION['phone']);?></td>
					</tr>
					<tr class="listschooltablealtbg">
						<td>E-Mail-Adresse:&nbsp;<span class="starcolor">*</span></td>
						<td><?php if(isset($_SESSION['mail'])) echo strip_tags($_SESSION['mail']);?></td>
					</tr>
					<tr class="listschooltablealtbg2">
						<td>Telefon:&nbsp;<span class="starcolor">*</span></td>
						<td><?php if(isset($_SESSION['phone'])) echo strip_tags($_SESSION['phone']);?></td>
					</tr>
                                        <tr class="listschooltablealtbg">
						<td>Homepage? - URL:</td>
						<td><?php if(isset($_SESSION['homepage'])) echo strip_tags($_SESSION['homepage']);?></td>
					</tr>
                                        <tr class="listschooltablealtbg2">
						<td>WebOPAC? - URL:</td>
						<td><?php if(isset($_SESSION['webopac'])) echo strip_tags($_SESSION['webopac']);?></td>
					</tr>
                                        <tr class="listschooltablealtbg">
							<td>Raumgröße (in m²):&nbsp;<span class="starcolor">*</span></td>
							<td><?php if(isset($_SESSION['roomsize'])) echo strip_tags($_SESSION['roomsize']); ?></td>
						</tr>
                                                <tr>
							<th colspan="2">Bibliotheksverwaltungsprogramm</th>
						</tr>
						<?php
						if(isset($_SESSION['islittera'])){
						?>
						<tr class="listschooltablealtbg">
							<td>Name:</td>
							<td>LITTERA</td>
						</tr>
						<tr class="listschooltablealtbg2">
							<td>Version:</td>
							<td><?php if(isset($_SESSION['litteraver'])) echo strip_tags($_SESSION['litteraver']); ?></td>
						</tr>									
						<?php
						}
						if(isset($_SESSION['isexlibris'])){
						?>
						<tr class="listschooltablealtbg">
							<td>Name:</td>
							<td>EXLIBRIS</td>
						</tr>
						<tr class="listschooltablealtbg2">
							<td>Version:</td>
							<td><?php if(isset($_SESSION['exlibrisver'])) echo strip_tags($_SESSION['exlibrisver']); ?></td>
						</tr>									
						<?php
						}
						if(isset($_SESSION['isbond'])){
						?>
						<tr class="listschooltablealtbg">
							<td>Name:</td>
							<td>BOND</td>
						</tr>
						<tr class="listschooltablealtbg2">
							<td>Version:</td>
							<td><?php if(isset($_SESSION['bondver'])) echo strip_tags($_SESSION['bondver']); ?></td>
						</tr>									
						<?php
						}
						if(isset($_SESSION['isbiblioweb'])){
						?>
						<tr class="listschooltablealtbg">
							<td>Name:</td>
							<td>BIBLIOWEB</td>
						</tr>
						<tr class="listschooltablealtbg2">
							<td>Version:</td>
							<td><?php if(isset($_SESSION['bibliowebver'])) echo strip_tags($_SESSION['bibliowebver']); ?></td>
						</tr>									
						<?php
						}
						if(isset($_SESSION['isanderes'])){
						?>
						<tr class="listschooltablealtbg">
							<td>Name:</td>
							<td><?php if(isset($_SESSION['anderesname'])) echo strip_tags($_SESSION['anderesname']); ?></td>
						</tr>
						<tr class="listschooltablealtbg2">
							<td>Version:</td>
							<td><?php if(isset($_SESSION['anderesver'])) echo strip_tags($_SESSION['anderesver']); ?></td>
						</tr>									
						<?php
						}
					?>
                </table>
                                                <table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="400px">
		
                                                <tr>
						<th colspan="2">Kontaktperson (ausgebildete Schulbibliothekar/in)</th>
					</tr>
					<tr class="listschooltablealtbg">
						<td>Name:&nbsp;<span class="starcolor">*</span></td>
						<td><?php if(isset($_SESSION['contact_name'])) echo strip_tags($_SESSION['contact_name']);?></td>
					</tr>
					<tr class="listschooltablealtbg2">
						<td>E-Mail-Adresse:&nbsp;<span class="starcolor">*</span></td>
						<td><?php if(isset($_SESSION['contact_mail'])) echo strip_tags($_SESSION['contact_mail']);?></td>
					</tr>
					<tr class="listschooltablealtbg">
						<td>Telefon:&nbsp;<span class="starcolor">*</span></td>
						<td><?php if(isset($_SESSION['contact_phone'])) echo strip_tags($_SESSION['contact_phone']);?></td>
					</tr>
                                         <tr>
						<th colspan="2">Anzahl der MitarbeiterInnen /KollegInnen:</th>
					</tr>                                         
                                        <tr class="listschooltablealtbg2">
						<td>mit abgeschlossener Ausbildung:&nbsp;<span class="starcolor">*</span></td>
						<td><?php if(isset($_SESSION['anz_ma_mit_abg_ausbildung'])) echo strip_tags($_SESSION['anz_ma_mit_abg_ausbildung']);?></td>
					</tr>
                                        <tr class="listschooltablealtbg">
						<td>ohne abgeschlossener Ausbildung:&nbsp;<span class="starcolor">*</span></td>
						<td><?php if(isset($_SESSION['anz_ma_ohne_abg_ausbildung'])) echo strip_tags($_SESSION['anz_ma_ohne_abg_ausbildung']);?></td>
					</tr>
                                        
		</table>
            
            
             <table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="400px">
		
                                                <tr>
						<th colspan="2">Medienbestand / Entlehnungen: </th>
					</tr>
					<tr class="listschooltablealtbg">
						<td>Medienzugang im Kalenderjahr:&nbsp;<span class="starcolor">*</span></td>
						<td><?php if(isset($_SESSION['medienzugang'])) echo strip_tags($_SESSION['medienzugang']);?></td>
					</tr>
					<tr class="listschooltablealtbg2">
						<td>Medienabgang im Kalenderjahr:&nbsp;<span class="starcolor">*</span></td>
						<td><?php if(isset($_SESSION['medienabgang'])) echo strip_tags($_SESSION['medienabgang']);?></td>
					</tr>
					                                      
                                        
		</table>
            
             <table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="400px">
		
                                                <tr>
						<th colspan="2">Benutzer und Benutzerinnen: </th>
					</tr>
					<tr class="listschooltablealtbg">
						<td>SchülerInnen:&nbsp;<span class="starcolor">*</span></td>
						<td><?php if(isset($_SESSION['anz_schueler'])) echo strip_tags($_SESSION['anz_schueler']);?></td>
					</tr>
					<tr class="listschooltablealtbg2">
						<td>Erwachsene, im Schulbetrieb tätige, Personen:&nbsp;<span class="starcolor">*</span></td>
						<td><?php if(isset($_SESSION['anz_erw_in_schulbetrieb'])) echo strip_tags($_SESSION['anz_erw_in_schulbetrieb']);?></td>
					</tr>
					<tr class="listschooltablealtbg">
						<td>externe LeserInnen:&nbsp;<span class="starcolor">*</span></td>
						<td><?php if(isset($_SESSION['ext_leser'])) echo strip_tags($_SESSION['ext_leser']);?></td>
					</tr>                                                                               
                                        <tr class="listschooltablealtbg2">
						<td>komb. Bibliotheken:&nbsp;<span class="starcolor">*</span></td>
						<td><?php if(isset($_SESSION['kom_bibliotheken'])) echo strip_tags($_SESSION['kom_bibliotheken']);?></td>
					</tr>                                        
                                        
		</table>	
		
		
		
				
		
		
		
										
							
					
			</div>
			<div style="float:right;">
                            
                              <table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="400px">
		
                                                <tr>
						<th colspan="2">Veranstaltungen und Aktivitäten: </th>
					</tr>
					<tr class="listschooltablealtbg">
						<td>Lesungen:&nbsp;<span class="starcolor">*</span></td>
						<td><?php if(isset($_SESSION['anz_lesungen'])) echo strip_tags($_SESSION['anz_lesungen']);?></td>
					</tr>
					<tr class="listschooltablealtbg2">
						<td>Workshops Projekte:&nbsp;<span class="starcolor">*</span></td>
						<td><?php if(isset($_SESSION['anz_wokshops'])) echo strip_tags($_SESSION['anz_wokshops']);?></td>
					</tr>
					<tr class="listschooltablealtbg">
						<td>Bibliotheksdidaktische Schulungen:&nbsp;<span class="starcolor">*</span></td>
						<td><?php if(isset($_SESSION['anz_bibliotheks_schulungen'])) echo strip_tags($_SESSION['anz_bibliotheks_schulungen']);?></td>
					</tr>                                                                               
                                        <tr class="listschooltablealtbg2">
						<td>Sonstige Veranstaltungen:&nbsp;<span class="starcolor">*</span></td>
						<td><?php if(isset($_SESSION['anz_sonstige_va'])) echo strip_tags($_SESSION['anz_sonstige_va']);?></td>
					</tr>   
                                        <tr class="listschooltablealtbg2">
						<td>Gesamt:&nbsp;</td>
						<td><?php echo $_SESSION['anz_sonstige_va']+$_SESSION['anz_bibliotheks_schulungen']+$_SESSION['anz_wokshops']+$_SESSION['anz_lesungen'];?></td>
					</tr>   
                                        
		</table>	
				 <table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="400px">
					<tr>
						<th colspan="2">Öffentlichkeitsarbeit</th>
					</tr>
					<tr class="listschooltablealtbg">
						<td>Öffentlichkeitsarbeit:</td>
						<td><?php if(isset($oeffentlichkeitsarbeit_text)) echo ($oeffentlichkeitsarbeit_text);?></td>
					</tr>
					
				</table>                          
                            
                            
                           
		
		</table>
	</div>
	<div style="height:1px; clear:both;"></div>
	<div style="border-bottom:1px solid #8A93A2; width:800px; height:1px;"></div>
	<div class="important-text"><p>Mit einen Klick auf Speichern, speichern Sie Ihre Änderungen in unserer Datenbank</p></div>
	<?php
	//Falls der Benutzer keine Schulbibliothek besitzt war die vorherige Stufe die 3.Stufe
		if($_SESSION['stepcounter']==2){
			$_SESSION['stepcounter']=4;
			?>
			<input type="button" value="" onclick="document.location.href='formular.php?step=1';" class="zurueckbutton"/>
			<?php
		}
		else{
			?>
			<input type="button" value="" onclick="document.location.href='formular.php?step=1';" class="zurueckbutton"/>
			<?php
		}
	?>
	<input type="submit" value="" class="speichernbutton"/>
</form>
<?php
include("footer.php");
?>