<?php 
/**
* Buchzeit APS-Verwaltung
*
* @author Manuel Postlbauer
* @version 0.9
*/

/**
* login
*
* Der Benutzer gibt �ber ein Formular Benutzername und Passwort ein. Diese Daten werden
* mit den Eintr�gen in der Datenbank verglichen. Sind diese valide, wird die Sessionvariable "LOGGEDIN" auf 1 gesetzt, der
* Benutzer ist somit eingeloggt und hat Zugriff auf den gesch�tzten Bereich
*
* @param User, Passwort �ber Formulareingabe
*/

//Beim klicken des Logoutbuttons auf der Internetseite wird man auf die Loginseite weitergeleitet. Durch session_start(); session_destroy(); session_start(); erfolgt der Logout
//und es wird gleich darauf eine Neue Anfrage f�r den Login vorbereitet.
session_start();
session_destroy();
session_start();
//Variable f�r das Usermen�, d.h ist der Benutzer eingeloggt wird ein zus�tzliches Men� oben auf der Seite angezeigt.  HOME | DATEN EDITIEREN | PROFIL | LOGOUT , dieses Men� wird
//nur angezeigt wenn die Variable auf 1 gesetzt ist.
$displayloginregister=0;

include("header.php");
?>
<script>
document.write("<h1>Login</h1>");
document.write("<form id='login' name='login' method='post' action='<?php echo $PHP_SELF ?>'>");
document.write("	  <table class='listschooltable' width='400' border='0' cellpadding='5' cellspacing='1'>");
document.write("		<tr>");
document.write("			<th colspan='2'>Login</th>");
document.write("		<tr>");
document.write("		<tr class='listschooltablealtbg'>");
document.write("		  <td width='120'>Diese Seite befindet sich derzeit im Wartungsmodus!</td>");
document.write("	  </table>");
document.write("	  <div class='important-text'>");
document.write("			<?php echo $error; ?>");
document.write("	  </div>");
document.write("	</form>");
</script>
<noscript>
<p>Sie m�ssen JavaScript aktivieren, um diese Seite nutzen zu k�nnen!</p>
</noscript>
<?php
//Footer des Seitendesigns
include("footer.php");
?>