<?php
/**
* Buchzeit APS-Verwaltung
*
* @author Manuel Postlbauer
* @version 0.9
*/

/**
* profile
*
* Hier kann das Passwort für den aktuell angemeldeten Benutzer geändert werden
* Es wird ein neues Passwort eingeben und zur Bestätigung wird es noch ein zweites mal eingegeben.
* Danach werden beide Felder miteinander verglichen. Stimmen diese überein wird das neue Kennwort md5 verschlüsselt
* in der Datenbank gespeichert.
*
*/
session_start();

if(isset($_SESSION['LOGGEDIN'])){
	$displayloginregister=1;
}
if(!isset($_SESSION['LOGGEDIN'])){
	header("location: index.php");
	exit();
}

if(isset($_POST['Submit']))
{
	include("localconf.php");
	
	$error;
	$passwordfield = $mysqli->real_escape_string($_POST['passwordfield']);
	$passwordfieldm = $mysqli->real_escape_string($_POST['passwordfieldm']);
	
	if($passwordfield!=$passwordfieldm){
		$error = "Die eingegebenen Kennwörter stimmen nicht überein.";
	}
	if(($passwordfield=='')||($passwordfieldm=='')){
		$error = "Bitte vervollständigen Sie Ihre Eingaben.";
	}
	if(!$error){		
		$qry="UPDATE schuladressen SET password='".md5($passwordfield)."' WHERE id='".$_SESSION['USERID']."'";
		$result=$mysqli->query($qry);
		if($result) {
			$error= "Ihr Kennwort wurde erfolgreich geändert";
		}
		else {
			die("Query failed");
		}
	}
}
?>
<?php
include("header.php");
?>
<h1>Passwort ändern</h1>
<p>Sie können Ihr derzeitiges Kennwort durch ein Neues ersetzen. Um das zu bewerkstelligen geben Sie unten Ihr gewünschtes Kennwort ein. Danach klicken Sie auf Bestätigen.</p>
<form id="password" name="password" method="post" action="<?php echo $PHP_SELF ?>">
	<table class="listschooltable" width="400" cellpadding="5" cellspacing="1">
		<tr>
			<th colspan="2">Änderung des persönlichen Kennworts</th>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Neues Kennwort:</td>
			<td><input type="password" name="passwordfield" id="passwordfield"/></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Neues Kennwort bestätigen:</td>
			<td><input type="password" name="passwordfieldm" id="passwordfieldm"/></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>&nbsp;</td>
			<td><input type="submit" name="Submit" value="" class="bestaetigenbutton"/></td>
		</tr>
	</table>
	<div class="important-text">
			<?php
				echo $error;
			?>
	</div>
</form>
<?php
include("footer.php");
?>