<?php
//header('Content-type: text/html; charset=utf-8');
/**
* Buchzeit APS-Verwaltung
*
* @author Manuel Postlbauer
* @version 0.9
*/

/**
* localconf
*
* Hier wird die Verbindung zur Datenbank hergestellt
* und ein Array für die Schultypen in der schuladress Tabelle definiert.
* wird in formular.php benötigt. Damit beim auslesen aus der Datenbank statt einer Zahl ein String in der
* Sessionvariable gespeichert wird.
*
*/
error_reporting(E_ERROR | E_PARSE);
$schultypen = array ("1" => "APS",
					 "2" => "AHS",
					 "3" => "BMHS",
					 //"4" => "polytechnischeschule",
					 //"5" => "gymnasium"
                                         );

//Für Admincp					 
$schultypen2 = array ("1" => "APS",
					  "2" => "AHS",
					  "3" => "BMHS",
					  //"4" => "Polytechnischeschule",
					  //"5" => "Gymnasium"
                                          );
					 
$bibliotheksart = array ("1" => "Schulbibliothek",
						 "2" => "Kombination 2 oder mehrere Schulen",
						 "3" => "Kombination mit öffentlicher Bibliothek");

//Bundesländer bei Suche
$bundeslaender_detail = array ("1" => "Burgenland",
							   "2" => "Kärnten",
							   "3" => "Niederösterreich",
							   "4" => "Oberösterreich",
							   "5" => "Salzburg",
							   "6" => "Steiermark",
							   "7" => "Tirol",
							   "8" => "Vorarlberg",
							   "9" => "Wien");
						 
//Bundeslandfilter	


$bundeslaender = array ("admin" => "",
						"b" => "1",
						"k" => "2",
						"noe" => "3",
						"ooe" => "4",
						"s" => "5",
						"st" => "6",
						"t" => "7",
						"v" => "8",
						"w" => "9");
//Bestätigungsmail
$emailaddress = "h.pitzer@buchzeit.at";

//Ja oder nein Array
$yesorno = array ("0" => "Nein",
				  "1" => "Ja");
			
$globaltableorder = array ( "0" => "schulkennzahl",
							"1" => "schultitel",
							"2" => "postleitzahl",
							"3" => "ort",
							"4" => "strasse_hausnummer",
							"5" => "schulart",
							"6" => "klassenanzahl",
							"7" => "budget DESC",
							"8" => "sid");
$oeffentlichkeitsarbeit = array ("1" => "eigene Website der Schulbibliothek",
							   "2" => "Soziale Medien (z.B. Facebook, Twitter etc.)",
							   "3" => "im Rahmen der Schulwebsite",
							   "4" => "Plakate, Folder, Flugblätter",
							   "5" => "Beitrag im Jahresbericht der Schule",
							   "6" => "Andere Varianten");
							   
//Datenbankzugangsdaten					 
$host="localhost";
$user="root";
$pass='1qayxsw"';
$db="usrdb_bibliabh_sb";

//Verbindung zur Datenbank herstellen (Kontaktid holen)
//$verbindung = mysql_connect($host,$user,$pass) or die ("keine Verbindung möglich. Benutzername oder Passwort sind falsch");
//$verbindung = mysqli_connect($host,$user,$pass,$db) or die ("keine Verbindung möglich. Benutzername oder Passwort sind falsch");
$mysqli = new mysqli($host,$user,$pass,$db) or die ("keine Verbindung möglich. Benutzername oder Passwort sind falsch");
//Auswählen der Datenbank
//mysql_select_db($db) or die ("Die Datenbank existiert nicht.");
if (mysqli_connect_errno()) {
      printf("Connect failed: %s\n", mysqli_connect_error());
      exit();
  }
 if (!$mysqli->set_charset("utf8")) {
      //printf("Error loading character set utf8: %s\n", $mysqli->error);
  } else {
      //printf("Current character set: %s\n", $mysqli->character_set_name());
  }
?>