<?php
/**
* Buchzeit APS-Verwaltung
*
* @author Manuel Postlbauer
* @version 0.9
*/

/**
* formular
*
* Das Formular f�r die APS-Verwaltung ist in mehrere Steps aufgeteilt. Als erstes werden die Daten zum aktuellen Benutzer aus der Datenbank ausgelesen
* und in das Formular gef�llt. Danach folgen die einzelnen Steps mit hilfe des "get-Parameters". z.B. formular.php?step=2
* Beim Step 7 wird eine Kontrollansicht angezeigt, wo alle bereits eingegebenen Daten nochmals gelistet werden. Klickt man dann auf speichern, wird
* Step 8 aufgerufen. Step 8 ist zust�ndig f�r das Speichern der Daten in die Datenbank. Nach der Durchf�hrung von Step 8 wird auf Step 9 weitergeleitet,
* welche den Benutzer die Meldung pr�sentiert, dass seine Daten nun gespeichert wurden.
*
*/
session_start();
error_reporting(E_ERROR | E_PARSE);
if(isset($_SESSION['LOGGEDIN'])){
	$displayloginregister=1;
}
if(!isset($_SESSION['LOGGEDIN'])){
	header("location: index.php");
	exit();
}
//Wenn in dieser Sitzung noch nicht aus der Datenbank gelesen wurde.
//$schultypen[$resultarray['schulart']]
if($_SESSION['ISREADFROMDB']!=1)
    {
	//Datenbankverbindung
	include("localconf.php");
	
	//Lest alle Daten f�r den eingeloggten Benutzer aus mit der zuvor in login.php gespeicherten USERID (Primary Key der schuladressen Tabelle)
	$qry="SELECT * FROM schuladressen WHERE id='".$_SESSION['USERID']."'";
	$result=$mysqli->query($qry);
		
	if($result) {
		if(mysqli_num_rows($result) == 1) {
			$resultarray = $result->fetch_assoc();
			//Step 1 des Formulars
			$_SESSION['school_id']=$resultarray['schulkennzahl'];
			$_SESSION['school_name']=$resultarray['schultitel'];
			$_SESSION['address_plz']=$resultarray['postleitzahl'];
			$_SESSION['address_school_loc']=$resultarray['ort'];
			$_SESSION['address_street']=$resultarray['strasse_hausnummer'];
			$_SESSION['schooltype']=$resultarray['schulart'];
			$_SESSION['gemeinde']=$resultarray['schulerhalter_gemeinde'];
			$_SESSION['schulerhalter']=$resultarray['schulerhalter_privat'];
                        $_SESSION['address_bundesland']=$resultarray['bundesland'];
			//Step 2 des Formulars
			$_SESSION['classroomcount']=$resultarray['klassenanzahl'];
			//SESSION Variablen f�r die Radiobuttons von Step 2 des Formulars, ist ein 1er in der Sessionvariable f�r einen Radiobutton ist er gesetzt.
			if($resultarray['eigene_bibliothek']==1)
				$_SESSION['librarykind_0']=1;
			if($resultarray['gemeinsame_bibliothek']==1)
				$_SESSION['librarykind_1']=1;
			if($resultarray['oeff_gemeinsame_bibliothek']==1)
				$_SESSION['librarykind_2']=1;
                        if(!empty($resultarray['andere_bibliothek']))
				$_SESSION['librarykind_3']=$resultarray['andere_bibliothek'];                        
			//Gibts es eine Bibliothek in der Schule, wenn ja dann wird der Fremdschl�ssel f�r den Eintrag in der Bibliothekstabelle in einer Sessionvariable speichern, 
			//wichtig f�r Step 8 des Formulars und den weiteren Auslesevorgang
                        
			if($resultarray['fk_bibliothek_id']!=0)
				$_SESSION['GLOBAL_BIBLIOTHEK_ID']=$resultarray['fk_bibliothek_id'];
		}	
	}
	else {
		echo $mysqli->error;
		die("Query failed");
	}
	//Gibt es eine Bibliothek dann die Daten der Bibliothek auslesen und ins Formular schreiben
	if(isset($_SESSION['GLOBAL_BIBLIOTHEK_ID'])){
	
		$qry="SELECT * FROM bibliothek WHERE id='".$_SESSION['GLOBAL_BIBLIOTHEK_ID']."'";
                
		$result=$mysqli->query($qry);
		
	if($result) {
		if(mysqli_num_rows($result) == 1) {
			$resultarray = $result->fetch_assoc();                        
				//Speichert die einzelnen Fremdschl�ssel in der Bibliothekstabelle in Sessionvariablen, wichtig f�r Step 8 des Formulars und den sp�teren Auslesevorgang.
				//Die Fremdschl�ssel sind f�r die Tabelle der Bibliothekssoftware, des Medienbestands, der Raum und Einrichtung, und die Funktion und Nutzung der Bibliothek
				if($resultarray['fk_bibliothek_verw_prog_id']!=0)
					$_SESSION['GLOBAL_VERW_PROG_ID']=$resultarray['fk_bibliothek_verw_prog_id'];
				if($resultarray['fk_bibliothek_medienbest_id']!=0)
					$_SESSION['GLOBAL_MEDIENBESTAND_ID']=$resultarray['fk_bibliothek_medienbest_id'];
				if($resultarray['fk_raum_einrichtung_id']!=0)
					$_SESSION['GLOBAL_RAUM_EINRICHTUNG_ID']=$resultarray['fk_raum_einrichtung_id'];
				if($resultarray['fk_bibliothek_funkt_nutzung_id']!=0)
					$_SESSION['GLOBAL_FUNKT_NUTZUNG_ID']=$resultarray['fk_bibliothek_funkt_nutzung_id'];
				//Step 5 des Formulars	
				$_SESSION['oeffnungstunden']=$resultarray['oeffnungsstunden'];
				$_SESSION['webopac']=$resultarray['webopacurl'];
				$_SESSION['homepage']=$resultarray['homepageurl'];
				$_SESSION['mail']=$resultarray['email'];
				$_SESSION['phone']=$resultarray['telefon'];	
				//Step 6 des Formulars
				$_SESSION['comments_pos']=$resultarray['anregung_gut'];
				$_SESSION['comments_neg']=$resultarray['anregung_schlecht'];	
				//Step 5 des Formulars
				$_SESSION['bibliothekaranzahl']=$resultarray['anz_schulbibliothekar'];
				$_SESSION['abgwochenstunden']=$resultarray['abgeltung_wochenstunden'];
				//Gleiches Prinzip wie bei den Radiobuttons. Ein 1er in der Sessionvariable setzt die Checkbox
				if($resultarray['mitarbeit_schueler']==1)
					$_SESSION['mitarbeitschueler']=1;
				if($resultarray['mitarbeit_eltern']==1)
					$_SESSION['mitarbeiteltern']=1;
				//Step 5 des Formulars
				$_SESSION['ankaufsbudget']=$resultarray['budget'];
			}	
		}
		else {
			echo $mysqli->error();
			die("Query failed");
		}
		//Ist ein Fremdschl�ssel f�r die Bibliothekssoftware vorhanden, lese die Daten f�r die Bibliothekssoftware aus und speichere sie ins Formular
		if(isset($_SESSION['GLOBAL_VERW_PROG_ID'])){
			//Der Fremdschl�ssel der Bibliothekssoftware in der Bibliothekstabelle entspricht den Primary Key eines Eintrages in der Tabelle f�r die Bibliothekssoftware
			$qry="SELECT * FROM bibliothek_verw_prog WHERE id='".$_SESSION['GLOBAL_VERW_PROG_ID']."'";
			$result=$mysqli->query($qry);		
	if($result) {
		if(mysqli_num_rows($result) == 1) {
			$resultarray = $result->fetch_assoc();
					//Hier wird der Name der Bibliothekssoftware abgefragt es wird der jeweilige Name in der Sessionvariable $_SESSION['name'] gespeichert. Es gibt nur eine Variable
					//f�r den Namen, da nur eine Software verwendet wird.
					
					//Step 3 des Formulars
					if($resultarray['name']=="LITTERA"){
						$_SESSION['islittera']=1;
						$_SESSION['litteraver']=$resultarray['version'];
					}
					if($resultarray['name']=="EXLIBRIS"){
						$_SESSION['isexlibris']=1;
						$_SESSION['exlibrisver']=$resultarray['version'];
					}
					if($resultarray['name']=="BOND"){
						$_SESSION['isbond']=1;
						$_SESSION['bondver']=$resultarray['version'];
					}
					if($resultarray['name']=="BIBLIOWEB"){
						$_SESSION['isbiblioweb']=1;
						$_SESSION['bibliowebver']=$resultarray['version'];
					}
					//Wurde kein Name der obigen Software gefunden, dann wird eine eigene Software verwendet. Der Name und die Version dieser wird in eigenen Sessionvariablen gespeichert
					if(($resultarray['name']!="LITTERA")&&($resultarray['name']!="EXLIBRIS")&&($resultarray['name']!="BOND")&&($resultarray['name']!="BIBLIOWEB")){
						$_SESSION['isanderes']=1;
						$_SESSION['anderesname']=$resultarray['name'];
						$_SESSION['anderesver']=$resultarray['version'];	
					}
				}	
			}
			else {
				echo $mysqli->error();
				die("Query failed");
			}
		}
		//Ist der Fremdschl�ssel f�r den Medienbestand in der Bibliothekstabelle vorhanden, dann werden die Daten �ber den Bestand ausgelesen und ins Formular gespeichert.
		if(isset($_SESSION['GLOBAL_MEDIENBESTAND_ID'])){
			$qry="SELECT * FROM bibliothek_medienbest WHERE id='".$_SESSION['GLOBAL_MEDIENBESTAND_ID']."'";
			$result=$mysqli->query($qry);
		
	if($result) {
		if(mysqli_num_rows($result) == 1) {
			$resultarray = $result->fetch_assoc();	
					//Step 4 des Formulars
					$_SESSION['belletristik']=$resultarray['printmedien_belletristik'];
					$_SESSION['zeitschriften']=$resultarray['printmedien_zeitschriften'];
					$_SESSION['tageszeitungen']=$resultarray['printmedien_zeitungen'];
					$_SESSION['cddvd']=$resultarray['digitale_medien'];
					$_SESSION['downloadang']=$resultarray['downloadangebote'];
					$_SESSION['videos']=$resultarray['av_medien'];
					$_SESSION['spiele']=$resultarray['spiele'];
					$_SESSION['mehrfachtitel']=$resultarray['anz_titel_klassenst'];
					$_SESSION['exemplareprotitel']=$resultarray['anzahl_titel'];
                                        $_SESSION['exemplareprotitel']=$resultarray['anzahl_titel'];
                                                                                
                                        
                                        $_SESSION['medienzugang']=$resultarray['medienzugang'];
                                        $_SESSION['medienabgang']=$resultarray['medienabgang'];
                                        $_SESSION['anz_schueler']=$resultarray['anz_schueler'];
                                        $_SESSION['anz_erw_in_schulbetrieb']=$resultarray['anz_erw_in_schulbetrieb'];
                                        $_SESSION['ext_leser']=$resultarray['ext_leser'];
                                        $_SESSION['kom_bibliotheken']=$resultarray['kom_bibliotheken'];
                                        $_SESSION['anz_lesungen']=$resultarray['anz_lesungen'];
                                        $_SESSION['anz_wokshops']=$resultarray['anz_wokshops'];
                                        $_SESSION['anz_bibliotheks_schulungen']=$resultarray['anz_bibliotheks_schulungen'];
                                        $_SESSION['anz_sonstige_va']=$resultarray['anz_sonstige_va'];
                                        $_SESSION['anz_ma_mit_abg_ausbildung']=$resultarray['anz_ma_mit_abg_ausbildung'];
                                        $_SESSION['anz_ma_ohne_abg_ausbildung']=$resultarray['anz_ma_ohne_abg_ausbildung'];
                                        $_SESSION['art_oeffentlichkeitsarbeit']=json_decode($resultarray['art_oeffentlichkeitsarbeit']);
                                        $_SESSION['andere_oeffentlichkeitsarbeit']=$resultarray['andere_oeffentlichkeitsarbeit'];
                                        
                                        
				}	
			}
			else {
				echo $mysqli->error();
				die("Query failed");
			}
		}
		//Ist der Fremdschl�ssel f�r die Raum und Einrichtungstabelle in der Bibliothekstabelle vorhanden, dann werden die Daten �ber den Raum und die Einrichung ausgelesen und ins Formular gespeichert.
		if(isset($_SESSION['GLOBAL_RAUM_EINRICHTUNG_ID'])){
			
			$qry="SELECT * FROM raum_einrichtung WHERE id='".$_SESSION['GLOBAL_RAUM_EINRICHTUNG_ID']."'";
			$result=$mysqli->query($qry);
		
	if($result) {
		if(mysqli_num_rows($result) == 1) {
			$resultarray = $result->fetch_assoc();
					//Step 3 des Formulars
					$_SESSION['roomsize']=$resultarray['raumgroesse'];
					if($resultarray['lage_zentral']==1)
						$_SESSION['zentral']=1;
					if($resultarray['lage_nahe_edv']==1)
						$_SESSION['lage_nahe_edv']=1;
					if($resultarray['lage_schwer_zu_erreichen']==1)
						$_SESSION['schwerzuerr']=1;
			
					$_SESSION['arbeitsplatzanzahl']=$resultarray['anzahl_arbeitsp'];
					$_SESSION['internetarbeits']=$resultarray['anzahl_arbeitsp_i'];
					
					if($resultarray['tvgeraet']==1)
						$_SESSION['tvgeraet']=1;
					if($resultarray['beamer']==1)
						$_SESSION['beamer']=1;
					if($resultarray['drucker']==1)
						$_SESSION['printer']=1;
					if($resultarray['scanner']==1)
						$_SESSION['scanner']=1;
					if($resultarray['hoerbuchstation']==1)
						$_SESSION['hoerbuchstation']=1;
					if($resultarray['kopiergeraet']==1)
						$_SESSION['kopierger']=1;
				}	
			}
			else {
				echo $mysqli->error();
				die("Query failed");
			}
		
		}
		//Ist der Fremdschl�ssel f�r die Funktion und Nutzungstabelle in der Bibliothekstabelle vorhanden, dann werden die Daten �ber die Funktion und Nutzung ausgelesen und ins Formular gespeichert.
		if(isset($_SESSION['GLOBAL_FUNKT_NUTZUNG_ID'])){
		
			$qry="SELECT * FROM bibliothek_funkt_nutzung WHERE id='".$_SESSION['GLOBAL_FUNKT_NUTZUNG_ID']."'";
			$result=$mysqli->query($qry);
		
	if($result) {
		if(mysqli_num_rows($result) == 1) {
			$resultarray = $result->fetch_assoc();	
					//Step 6 des Formulars
					if($resultarray['lesefoerderung']==1)
						$_SESSION['usage_reading']=1;
					if($resultarray['individuallernen']==1)
						$_SESSION['usage_learning']=1;
					if($resultarray['erwerbmedienkomp']==1)
						$_SESSION['usage_media']=1;

					$_SESSION['usage_other']=$resultarray['anderes'];
					
					if($resultarray['unterrichttaeg']==1)
						$_SESSION['dailyclass']=1;
					if($resultarray['unterrichtwoech']==1)
						$_SESSION['weeklyclass']=1;
					if($resultarray['unterrichtmonat']==1)	
						$_SESSION['monthlyclass']=1;
					if($resultarray['unterrichtsporad']==1)
						$_SESSION['sporadicclass']=1;
					if($resultarray['unterrichtnicht']==1)
						$_SESSION['noclass']=1;
					if($resultarray['nutzungveranstaltungen']==1)
						$_SESSION['outsideclass']=1;
				}	
			}
			else {
				echo $mysqli->error;
				die("Query failed");
			}
		}
		
		//Filtert die Kontaktpersonen aus der Tabelle bei denen der Fremdschl�ssel f�r die Bibliothek mit dem der Bibliothek f�r den eingeloggten User �bereinstimmt. (schuladressentabelle fk_bibliothek_id == bibliothek_kontaktperson fk_bibliothek_id)
		$qry="SELECT * FROM bibliothek_kontaktperson WHERE fk_bibliothek_id='".$_SESSION['GLOBAL_BIBLIOTHEK_ID']."'";
		$result=$mysqli->query($qry);		
	if($result) {		
				while($resultarray = $result->fetch_assoc()){
					//Ist im Feld Stellvertreter 0 eingetragen, dann ist der Kontakt die Hauptkontaktperson
					if($resultarray['stellvertreter']==0){
						$_SESSION['contact_name']=$resultarray['name'];
						$_SESSION['contact_mail']=$resultarray['email'];
						$_SESSION['contact_phone']=$resultarray['tel'];
						$_SESSION['contact_fax']=$resultarray['fax'];
						$_SESSION['contact_person_anz']+=1;
					}
					//sonst ist es der Stellvertreter
					else{
						$_SESSION['contact_name2']=$resultarray['name'];
						$_SESSION['contact_mail2']=$resultarray['email'];
						$_SESSION['contact_phone2']=$resultarray['tel'];
						$_SESSION['contact_fax2']=$resultarray['fax'];
						$_SESSION['contact_person_anz']+=1;
					}
				}
		} 
		else {                   
			echo $mysqli->error();
			die("Query failed");
		}
	}
	//Wird diese Sessionvariable auf 1 gesetzt wird angegeben, dass in dieser Sitzung schon einmal die Daten aus der Datenbank gelesen wurden, damit dieser Vorgang nicht wiederholt wird.
	//Diese Variable wird bei Step 8 des Formulars(Schreiben der Daten in die Datenbank) zur�ckgesetzt.
	$_SESSION['ISREADFROMDB'] = 1;
}
//Je nach Stepparameter z.B formular.php?step=1 wird die phpDatei f�r den jeweiligen Step includiert
$step=$_GET['step'];
switch ($step) {
    case 1:
		include("step1.php");
        break;
    case 2:
        //header("location: writedb.php");
		include("step7.php");
        break;
    case 3:
		include("step3.php");
        break;
	case 4:
		include("step4.php");
		break;
	case 5:
		include("step5.php");
		break;
	case 6:
		include("step6.php");
        break;
	case 7:
		include("step7.php");
        break;
	case 8:
		header("location: writedb.php");
        break;
}
?>