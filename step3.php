<?php
/**
* Buchzeit APS-Verwaltung
*
* @author Manuel Postlbauer
* @version 0.9
*/

/**
* step 3 (Teil von formular.php)
*/
if(!isset($_SESSION['LOGGEDIN'])){
	header("location: index.php");
	exit();
}
if($_SESSION['stepcounter']==3){
	if((!isset($_POST['librarykind_0']))&&(!isset($_POST['librarykind_1']))&&(!isset($_POST['librarykind_2']))) {
		//Falls der Benutzer vorher sagt er hat Bibliothek, dann zurückgeht anhakelt das er keine hat und wieder vorwärts
		//geht hat er trotzdem eine. Die 3 folgenden unset verhindern das.
		unset($_SESSION['librarykind_0']);
		unset($_SESSION['librarykind_1']);
		unset($_SESSION['librarykind_2']);
		//Diese Variable wird benötig für die Kontrolle der Daten bei Step 7
		//Ist nämlich keine Bibliothek vorhanden, brauchen die restlichen Formularfelder bei der Gesamtansicht der eingegebenen Formulardaten
		//nicht angezeigt werden
		$_SESSION['leermeldung']=1;
		$_SESSION['classroomcount']=$_POST['classroomcount'];
		header("Location: formular.php?step=7");
		exit(); 
	}
	$_SESSION['leermeldung']=0;
	$_SESSION['classroomcount']=$_POST['classroomcount'];
	$_SESSION['librarykind_0']=$_POST['librarykind_0'];
	$_SESSION['librarykind_1']=$_POST['librarykind_1'];
	$_SESSION['librarykind_2']=$_POST['librarykind_2'];
	
	$_SESSION['stepcounter']+=1;
}
else{
	$_SESSION['stepcounter']-=1;
}
include("header.php");
?>
<form action="formular.php?step=4" method="post" onsubmit="return checkFields(3)">
	<h1>Daten editieren</h1>
	<img src="images/step3.png" alt=""/>
	<br/>
	<br/>
	<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="481px">
		<tr>
			<th colspan="2">Raum, Einrichtung, EDV</th>
		</tr>
		<tr class="listschooltablealtbg">
			<td width="200px">Raumgröße (in m²):&nbsp;<span class="starcolor">*</span></td>
			<td><input type="text" name="roomsize" id="roomsize" value="<?php if(isset($_SESSION['roomsize'])) echo $_SESSION['roomsize']; ?>" /></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Lage: zentral</td>
			<td><input type="checkbox" name="zentral" value="1" <?php if(!empty($_SESSION['zentral'])) echo "checked"; ?>/></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Lage: in der Nähe von EDV-Sälen</td>
			<td><input type="checkbox" name="edvsaele" value="1" <?php if(!empty($_SESSION['lage_nahe_edv'])) echo "checked"; ?>/></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Lage: eher schwer zu erreichen</td>
			<td><input type="checkbox" name="schwerzuerr" value="1" <?php if(!empty($_SESSION['schwerzuerr'])) echo "checked"; ?> /></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Anzahl der Schüler-Arbeitsplätze insgesamt:</td>
			<td><input type="text" name="arbeitsplatzanzahl" id="arbeitsplatzanzahl" value="<?php if(isset($_SESSION['arbeitsplatzanzahl'])) echo $_SESSION['arbeitsplatzanzahl']; else echo"0"; ?>" /></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Anzahl der Schüler-Internet-Arbeitsplätze:</td>
			<td><input type="text" name="internetarbeits" id="internetarbeits" value="<?php if(isset($_SESSION['internetarbeits'])) echo $_SESSION['internetarbeits']; else echo"0"; ?>" /></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Fernseher:</td>
			<td><input type="checkbox" name="tvgeraet" value="1" <?php if(!empty($_SESSION['tvgeraet'])) echo "checked"; ?>/></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Beamer:</td>
			<td><input type="checkbox" name="beamer" value="1" <?php if(!empty($_SESSION['beamer'])) echo "checked"; ?>/></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Drucker:</td>
			<td><input type="checkbox" name="printer" value="1" <?php if(!empty($_SESSION['printer'])) echo "checked"; ?>/></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Handscanner:</td>
			<td><input type="checkbox" name="scanner" value="1" <?php if(!empty($_SESSION['scanner'])) echo "checked"; ?>/></td>
		</tr>
		<tr class="listschooltablealtbg">
			<td>Hörbuchstation:</td>
			<td><input type="checkbox" name="hoerbuchstation" value="1" <?php if(!empty($_SESSION['hoerbuchstation'])) echo "checked"; ?>/></td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td>Eigenes Kopiergerät:</td>
			<td><input type="checkbox" name="kopierger" value="1" <?php if(!empty($_SESSION['kopierger'])) echo "checked"; ?>/></td>
		</tr>
	</table>
	
	<table class="listschooltable" border="0" cellpadding="5" cellspacing="1" width="481px">
		<tr>
			<th colspan="2">Bibliotheksverwaltungsprogramm</th>
		</tr>
		<tr class="listschooltablealtbg">
		<!-- Klickt man einen Radiobutton für eine Software im Formular an wird eine Javascriptfunktion aufgerufen welche die Eingabefelder für die zugehörige Software anzeigt und die anderen ausblendet-->
			<td colspan="2">
				<input type="radio" name="programm" value="littera" onClick="bibliotheksverwaltungsprogramm(1);" <?php if(!empty($_SESSION['islittera'])) echo "checked";?>/> LITTERA
				<input type="radio" name="programm" value="exlibris" onClick="bibliotheksverwaltungsprogramm(2);"<?php if(!empty($_SESSION['isexlibris'])) echo "checked";?>/> EXLIBRIS
				<input type="radio" name="programm" value="bond" onClick="bibliotheksverwaltungsprogramm(3);"<?php if(!empty($_SESSION['isbond'])) echo "checked";?>/> BOND
				<input type="radio" name="programm" value="biblioweb" onClick="bibliotheksverwaltungsprogramm(4);"<?php if(!empty($_SESSION['isbiblioweb'])) echo "checked";?>/> BIBLIOWEB
				<input type="radio" name="programm" value="anderes" onClick="bibliotheksverwaltungsprogramm(5);"<?php if(!empty($_SESSION['isanderes'])) echo "checked";?>/> ANDERE
			</td>
		</tr>
		<tr class="listschooltablealtbg2">
			<td width="200px">
				<div id="verw1" style="display:none;">LITTERA (Version):</div>
				<div id="verw2" style="display:none;">EXLIBRIS (Version):</div>
				<div id="verw3" style="display:none;">BOND (Version):</div>
				<div id="verw4" style="display:none;">BIBLIOWEB (Version):</div>
				<div id="verw5" style="display:none;">Anderes (Name,Version):</div>
			</td>
			<td>
				<div id="verw1_1" style="display:none;"><input type="text" name="litteraver" id="litteraver" value="<?php if(isset($_SESSION['litteraver'])) echo $_SESSION['litteraver']; ?>"/></div>
				<div id="verw2_1" style="display:none;"><input type="text" name="exlibrisver" id="exlibrisver" value="<?php if(isset($_SESSION['exlibrisver'])) echo $_SESSION['exlibrisver']; ?>"/></div>
				<div id="verw3_1" style="display:none;"><input type="text" name="bondver" id="bondver" value="<?php if(isset($_SESSION['bondver'])) echo $_SESSION['bondver']; ?>"/></div>
				<div id="verw4_1" style="display:none;"><input type="text" name="bibliowebver" id="bibliowebver" value="<?php if(isset($_SESSION['bibliowebver'])) echo $_SESSION['bibliowebver']; ?>"/></div>
				<div id="verw5_1" style="display:none;"><input type="text" name="anderesname" id="anderesname" value="<?php if(isset($_SESSION['anderesname'])) echo $_SESSION['anderesname']; ?>"/><input type="text" name="anderesver" id="anderesver" value="<?php if(isset($_SESSION['anderesver'])) echo $_SESSION['anderesver']; ?>"/></div>
			</td>
		</tr>
		<?php
		//Wenn unter formular.php als Name in der Tabelle für die Bibliothekssoftware Littera, Exlibris, Bond, oder der Name einer eigenen Software ausgelesen wird,
		//wird die zum Namen gehörige Version in der jeweilgen Sessionvariable gespeichert.. Das heißt diese ist nicht leer. Z.B. Name ist Exlibris dann wird die Version im Feld version in der Datenbank in die
		//Sessionvariable exlibrisver gespeichert, d.h diese ist gesetzt. Ist es eine andere Software, wird eine andere Sessionvariable für die Version gesetzt. Danach wird die Javascriptfunktion aufgerufen
		//welche die Felder für die zugehörige Software auf der Webseite anzeigt und die anderen ausblendet.
			if(!empty($_SESSION['islittera']))
				echo"<SCRIPT LANGUAGE='javascript'>bibliotheksverwaltungsprogramm(1);</SCRIPT>";
			if(!empty($_SESSION['isexlibris']))
				echo"<SCRIPT LANGUAGE='javascript'>bibliotheksverwaltungsprogramm(2);</SCRIPT>";
			if(!empty($_SESSION['isbond']))
				echo"<SCRIPT LANGUAGE='javascript'>bibliotheksverwaltungsprogramm(3);</SCRIPT>";
			if(!empty($_SESSION['isbiblioweb']))
				echo"<SCRIPT LANGUAGE='javascript'>bibliotheksverwaltungsprogramm(4);</SCRIPT>";
			if(!empty($_SESSION['isanderes']))
				echo"<SCRIPT LANGUAGE='javascript'>bibliotheksverwaltungsprogramm(5);</SCRIPT>";
		?>
	</table>
	<input type="button" value="" onclick="document.location.href='formular.php?step=2';" class="zurueckbutton"/>
	<input type="submit" value="" class="weiterbutton"/>
</form>
<?php
include("footer.php");
?>